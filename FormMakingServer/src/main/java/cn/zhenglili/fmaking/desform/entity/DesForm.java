package cn.zhenglili.fmaking.desform.entity;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName：DesForm
 * @Description：TODO
 * @Author：zhenglili
 * @Date：2020/7/29 15:46
 **/
@Data
public class DesForm {
    /**
     * 主键
     */
    private String id;
    /**
     * 姓名
     */
    // 标识该字段可以模糊查询
    private String name;
    /**
     * 内容
     */
    private String content;
    /**
     * 编码
     */
    private String encoding;
    /**
     * 图标
     */
    private String icon;
    /**
     * userId
     */
    // 标识对应表字段,查询时必填
    private String userId;


    private Date createTime;

    private Date updateTime;

    private int deleteFlag;

    public DesForm() {
    }
    public DesForm(String name, String encoding, String icon, String userId) {
        this.name = name;
        this.encoding = encoding;
        this.icon = icon;
        this.userId = userId;
    }
}
