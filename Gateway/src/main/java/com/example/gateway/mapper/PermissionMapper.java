package com.example.gateway.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Mapper
@Component
public interface PermissionMapper {

    List<Map<String,Object>> queryByUser(Map<String,Object> map) throws SQLException;

}
