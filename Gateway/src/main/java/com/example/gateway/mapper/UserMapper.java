package com.example.gateway.mapper;


import com.example.gateway.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Mapper
@Component
public interface UserMapper {
    SysUser getUserByName(@Param("username")  String username) throws SQLException;
}
