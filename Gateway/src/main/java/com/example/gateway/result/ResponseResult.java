package com.example.gateway.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description: TODO
 * @author: 周天翼
 * @date: 2020年07月27日 16:03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseResult {

    private String success;
    private String message;
    private String code;
    private long timestamp;

}
