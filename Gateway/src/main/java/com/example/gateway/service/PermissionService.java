package com.example.gateway.service;


import com.example.gateway.mapper.PermissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @Description: TODO
 * @author: 周天翼
 * @date: 2020年07月26日 15:13
 */
@Service
public class PermissionService {

    @Autowired
    PermissionMapper permissionMapper;

    public List<Map<String,Object>> queryByUser(Map<String,Object> map) throws SQLException {
        return permissionMapper.queryByUser(map);
    }

}
