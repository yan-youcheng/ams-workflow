package com.example.gateway.service;


import com.example.gateway.entity.SysUser;
import com.example.gateway.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

/**
 * @Description: TODO
 * @author: 周天翼
 * @date: 2020年07月26日 13:33
 */
@Service
public class UserService {

    @Autowired
    UserMapper userMapper;

    public SysUser getUserByName(String username) throws SQLException {
/*        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username",username);
        return userMapper.selectOne(queryWrapper);*/
        return userMapper.getUserByName(username);
    }

}
