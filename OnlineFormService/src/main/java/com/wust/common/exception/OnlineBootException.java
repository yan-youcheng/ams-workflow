package com.wust.common.exception;

/**
 * @author wanheng
 */
public class OnlineBootException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public OnlineBootException(String message) {
        super(message);
    }

    public OnlineBootException(Throwable cause) {
        super(cause);
    }

    public OnlineBootException(String message, Throwable cause) {
        super(message, cause);
    }
}
