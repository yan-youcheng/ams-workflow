package com.wust.common.system.api;

import com.wust.common.system.vo.DictModel;

import java.sql.SQLException;
import java.util.List;


/**
 * @author wanheng
 */
public interface SysBaseApi {

    /**
     * 获取当前数据库类型
     *
     * @return
     * @throws SQLException
     */
    String getDatabaseType() throws SQLException;

    /**
     * 获取数据字典
     *
     * @param code
     * @return
     */
    List<DictModel> queryDictItemsByCode(String code);


    /**
     * 获取表数据字典
     *
     * @param table
     * @param text
     * @param code
     * @return
     */
    List<DictModel> queryTableDictItemsByCode(String table, String text, String code);

    /**
     * 查询表字典 支持过滤数据
     *
     * @param table
     * @param text
     * @param code
     * @param filterSql
     * @return
     */
    List<DictModel> queryFilterTableDictInfo(String table, String text, String code, String filterSql);


}
