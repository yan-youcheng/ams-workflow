package com.wust.common.system.query;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wanheng
 */
@Data
public class QueryCondition implements Serializable {

    private static final long serialVersionUID = 4740166316629191651L;

    private String field;
    private String type;
    private String rule;
    private String val;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (field == null || "".equals(field)) {
            return "";
        }
        stringBuilder.append(this.field).append(" ").append(this.rule).append(" ").append(this.type).append(" ").append(this.val);
        return stringBuilder.toString();
    }
}
