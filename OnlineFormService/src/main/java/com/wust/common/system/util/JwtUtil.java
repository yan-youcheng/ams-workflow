package com.wust.common.system.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.common.base.Joiner;
import com.wust.common.constant.DataBaseConstant;
import com.wust.common.system.vo.SysUserCacheInfo;
import com.wust.common.util.LoginUserUtil;
import com.wust.common.util.SpringContextUtils;
import com.wust.common.util.oConvertUtils;
import com.wust.modules.system.entity.SysUser;
import com.wust.common.exception.OnlineBootException;
import com.wust.modules.system.service.ISysService;
import com.wust.modules.system.service.impl.SysServiceImpl;
import org.springframework.context.ApplicationContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author wanehng
 * @Desc JWT工具类
 **/
public class JwtUtil {

    /**
     * 获得token中的信息无需secret解密也能获得
     *
     * @return token中包含的用户名
     */
    public static String getUsername(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("username").asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }


    /**
     * 根据request中的token获取用户账号
     *
     * @param request
     * @return
     * @throws OnlineBootException
     */
    public static String getUserNameByToken(HttpServletRequest request) throws OnlineBootException {
        String accessToken = request.getHeader("X-Access-Token");
        String username = getUsername(accessToken);
        if (oConvertUtils.isEmpty(username)) {
            throw new OnlineBootException("未获取到用户");
        }
        return username;
    }

    /**
     * 从session中获取变量
     *
     * @param key
     * @return
     */
    public static String getSessionData(String key) {
        //${myVar}%
        //得到${} 后面的值
        String moshi = "";
        if (key.contains("}")) {
            moshi = key.substring(key.indexOf("}") + 1);
        }
        String returnValue = null;
        if (key.contains("#{")) {
            key = key.substring(2, key.indexOf("}"));
        }
        if (oConvertUtils.isNotEmpty(key)) {
            HttpSession session = SpringContextUtils.getHttpServletRequest().getSession();
            returnValue = (String) session.getAttribute(key);
        }
        //结果加上${} 后面的值
        if (returnValue != null) {
            returnValue = returnValue + moshi;
        }
        return returnValue;
    }

    /**
     * 从当前用户中获取变量
     *
     * @param key
     * @param user
     * @return
     */
    public static String getUserSystemData(String key, SysUserCacheInfo user) {
        if (user == null) {
            user = DataAutorUtils.loadUserInfo();
        }
        // 获取登录用户信息
        SysUser sysUser = LoginUserUtil.getLoginUser();
        String moshi = "";
        if (key.contains("}")) {
            moshi = key.substring(key.indexOf("}") + 1);
        }
        String returnValue = null;
        //针对特殊标示处理#{sysOrgCode}，判断替换
        if (key.contains("#{")) {
            key = key.substring(2, key.indexOf("}"));
        }
        //替换为系统登录用户帐号
        if (key.equals(DataBaseConstant.SYS_USER_CODE) || key.toLowerCase().equals(DataBaseConstant.SYS_USER_CODE_TABLE)) {
            if (user == null) {
                returnValue = sysUser.getUsername();
            } else {
                returnValue = user.getSysUserCode();
            }
        }
        //替换为系统登录用户真实名字
        else if (key.equals(DataBaseConstant.SYS_USER_NAME) || key.toLowerCase().equals(DataBaseConstant.SYS_USER_NAME_TABLE)) {
            if (user == null) {

            } else {
                returnValue = user.getSysUserName();
            }
        }

        //替换为系统用户登录所使用的机构编码
        else if (key.equals(DataBaseConstant.SYS_ORG_CODE) || key.toLowerCase().equals(DataBaseConstant.SYS_ORG_CODE_TABLE)) {
            if (user == null) {
                returnValue = sysUser.getOrgCode();
            } else {
                returnValue = user.getSysOrgCode();
            }
        }
        //替换为系统用户所拥有的所有机构编码
        else if (key.equals(DataBaseConstant.SYS_MULTI_ORG_CODE) || key.toLowerCase().equals(DataBaseConstant.SYS_MULTI_ORG_CODE_TABLE)) {
            if (user.isOneDepart()) {
                returnValue = user.getSysMultiOrgCode().get(0);
            } else {
                returnValue = Joiner.on(",").join(user.getSysMultiOrgCode());
            }
        }
        // 替换为当前系统时间(年月日)
        else if (key.equals(DataBaseConstant.SYS_DATE) || key.toLowerCase().equals(DataBaseConstant.SYS_DATE_TABLE)) {
            // returnValue = user.getSysDate();
        }
        // 替换为当前系统时间（年月日时分秒）
        else if (key.equals(DataBaseConstant.SYS_TIME) || key.toLowerCase().equals(DataBaseConstant.SYS_TIME_TABLE)) {
            // returnValue = user.getSysTime();
        }
        // 流程状态默认值（默认未发起）
        else if (key.equals(DataBaseConstant.BPM_STATUS) || key.toLowerCase().equals(DataBaseConstant.BPM_STATUS_TABLE)) {
            returnValue = "1";
        }
        if (returnValue != null) {
            returnValue = returnValue + moshi;
        }
        return returnValue;
    }
}
