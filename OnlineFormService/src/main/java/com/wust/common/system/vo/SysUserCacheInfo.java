package com.wust.common.system.vo;

import lombok.Data;

import java.util.List;

/**
 * @author wanheng
 */
@Data
public class SysUserCacheInfo {

    private String sysUserCode;

    private String sysUserName;

    private String sysOrgCode;

    private List<String> sysMultiOrgCode;

    private boolean oneDepart;

}
