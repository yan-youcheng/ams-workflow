package com.wust.common.util;

/**
 * @Author wanheng
 */
public enum BrowserType {
    /**
     * 浏览器类型
     */
    IE11, IE10, IE9, IE8, IE7, IE6, Firefox, Safari, Chrome, Opera, Camino, Gecko
}
