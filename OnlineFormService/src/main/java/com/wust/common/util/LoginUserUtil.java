package com.wust.common.util;


import com.wust.modules.system.entity.SysUser;
import com.wust.modules.system.service.ISysService;
import com.wust.modules.system.service.impl.SysServiceImpl;
import lombok.extern.slf4j.Slf4j;


/**
 * @author wanheng
 */
@Slf4j
public class LoginUserUtil {

    public static SysUser getLoginUser(){
        log.warn("登录");
        ISysService sysService = new SysServiceImpl();
        log.warn(sysService.toString());
        return sysService.getLoginUser();
    }
}
