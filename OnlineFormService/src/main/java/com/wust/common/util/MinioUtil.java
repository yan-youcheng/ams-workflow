package com.wust.common.util;

import io.minio.MinioClient;
import io.minio.errors.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * minio文件上传工具类
 *
 * @author wanheng
 */
@Slf4j
@Data
public class MinioUtil {

    private static String minioUrl;

    private static String minioName;

    private static String minioPass;

    private static String bucketName;

    private static MinioClient minioClient = null;

    /**
     * 上传文件
     *
     * @param file
     * @return
     */
    public static String upload(MultipartFile file, String bizPath, String customBucket) {
        String fileUrl = "";
        String newBucket = bucketName;
        if (oConvertUtils.isNotEmpty(customBucket)) {
            newBucket = customBucket;
        }
        try {
            initMinio(minioUrl, minioName, minioPass);
            // 检查存储桶是否已经存在
            if (minioClient.bucketExists(newBucket)) {
                log.info("Bucket already exists.");
            } else {
                // 创建一个名为ota的存储桶
                minioClient.makeBucket(newBucket);
                log.info("create a new bucket.");
            }
            InputStream stream = file.getInputStream();
            // 获取文件名
            String orgName = file.getOriginalFilename();
            orgName = CommonUtils.getFileName(orgName);
            String objectName = bizPath + "/" + orgName.substring(0, orgName.lastIndexOf(".")) + "_" + System.currentTimeMillis() + orgName.substring(orgName.indexOf("."));

            // 使用putObject上传一个本地文件到存储桶中。
            minioClient.putObject(newBucket, objectName, stream, stream.available(), "application/octet-stream");
            stream.close();
            fileUrl = minioUrl + newBucket + "/" + objectName;
        } catch (IOException | InvalidKeyException | NoSuchAlgorithmException | NoResponseException | XmlPullParserException |
                InvalidArgumentException | RegionConflictException | InvalidBucketNameException | ErrorResponseException |
                InternalException | InsufficientDataException e) {
            log.error(e.getMessage(), e);
        }
        return fileUrl;
    }

    /**
     * 文件上传
     *
     * @param file
     * @param bizPath
     * @return
     */
    public static String upload(MultipartFile file, String bizPath) {
        return upload(file, bizPath, null);
    }


    /**
     * 初始化客户端
     *
     * @param minioUrl
     * @param minioName
     * @param minioPass
     * @return
     */
    private static MinioClient initMinio(String minioUrl, String minioName, String minioPass) {
        if (minioClient == null) {
            try {
                minioClient = new MinioClient(minioUrl, minioName, minioPass);
            } catch (InvalidEndpointException | InvalidPortException e) {
                e.printStackTrace();
            }
        }
        return minioClient;
    }

    /**
     * 上传文件到minio
     *
     * @param stream
     * @param relativePath
     * @return
     */
    public static String upload(InputStream stream, String relativePath) throws IOException, InvalidKeyException, NoSuchAlgorithmException, InsufficientDataException, InternalException, NoResponseException, InvalidBucketNameException, XmlPullParserException, ErrorResponseException, RegionConflictException, InvalidArgumentException {
        initMinio(minioUrl, minioName, minioPass);
        if (minioClient.bucketExists(bucketName)) {
            log.info("Bucket already exists.");
        } else {
            // 创建一个名为ota的存储桶
            minioClient.makeBucket(bucketName);
            log.info("create a new bucket.");
        }
        minioClient.putObject(bucketName, relativePath, stream, stream.available(), "application/octet-stream");
        stream.close();
        return minioUrl + bucketName + "/" + relativePath;
    }

}
