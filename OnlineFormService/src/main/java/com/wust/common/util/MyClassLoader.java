package com.wust.common.util;

/**
 * @Author wanheng
 */
public class MyClassLoader extends ClassLoader {

    public static Class getClassByScn(String className) {
        Class myclass = null;
        try {
            myclass = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(className + " not found!");
        }
        return myclass;
    }


}
