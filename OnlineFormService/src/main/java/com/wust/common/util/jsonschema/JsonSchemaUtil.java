package com.wust.common.util.jsonschema;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

/**
 * @author wanheng
 */
@Slf4j
public class JsonSchemaUtil {

    /**
     * 生成JsonSchema
     *
     * @param descrip
     * @param propertyList
     * @return
     */
    public static JSONObject getJsonSchema(JsonSchemaDescrip descrip, List<AbstractCommonProperty> propertyList) {
        JSONObject obj = new JSONObject();
        obj.put("$schema", descrip.getSchema());
        obj.put("type", descrip.getType());
        obj.put("title", descrip.getTitle());

        List<String> requiredArr = descrip.getRequired();
        obj.put("required", requiredArr);

        JSONObject properties = new JSONObject();
        for (AbstractCommonProperty commonProperty : propertyList) {
            Map<String, Object> map = commonProperty.getPropertyJson();
            properties.put(map.get("key").toString(), map.get("prop"));
        }
        obj.put("properties", properties);
        log.info("---JSONSchema--->"+obj.toJSONString());
        return obj;
    }
}
