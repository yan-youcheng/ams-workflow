package com.wust.common.util.jsonschema.validate;

import com.alibaba.fastjson.JSONObject;
import com.wust.common.util.jsonschema.AbstractCommonProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 字典属性
 *
 * @author wanheng
 */
@Data
public class DictProperty extends AbstractCommonProperty {

    private static final long serialVersionUID = 3786503639885610767L;

    private String dictCode;

    private String dictTable;

    private String dictText;

    /**
     * 构造器
     */
    public DictProperty(String key, String title, String dictTable, String dictCode, String dictText) {
        this.type = "string";
        this.view = "sel_search";
        this.key = key;
        this.title = title;
        this.dictCode = dictCode;
        this.dictTable = dictTable;
        this.dictText = dictText;
    }

    @Override
    public Map<String, Object> getPropertyJson() {
        Map<String, Object> map = new HashMap<>(8);
        map.put("key", getKey());
        JSONObject prop = getCommonJson();
        if (dictCode != null) {
            prop.put("dictCode", dictCode);
        }
        if (dictTable != null) {
            prop.put("dictTable", dictTable);
        }
        if (dictText != null) {
            prop.put("dictText", dictText);
        }
        map.put("prop", prop);
        return map;
    }

}
