package com.wust.common.util.jsonschema.validate;

import com.alibaba.fastjson.JSONObject;
import com.wust.common.util.jsonschema.BaseColumn;
import com.wust.common.util.jsonschema.AbstractCommonProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 级联下拉
 * @author wanheng
 */
@Data
public class LinkDownProperty extends AbstractCommonProperty {

    /**
     * 配置信息
     */
    String dictTable;

    /**
     * 级联下拉组件 的其他级联列
     */
    List<BaseColumn> otherColumns;

    /**
     * 构造器
     */
    public LinkDownProperty(String key, String title, String dictTable) {
        this.type = "string";
        this.view = "link_down";
        this.key = key;
        this.title = title;
        this.dictTable = dictTable;
    }

    @Override
    public Map<String, Object> getPropertyJson() {
        Map<String, Object> map = new HashMap<>(8);
        map.put("key", getKey());
        JSONObject prop = getCommonJson();
        JSONObject temp = JSONObject.parseObject(this.dictTable);
        prop.put("config", temp);
        prop.put("others", otherColumns);
        map.put("prop", prop);
        return map;
    }
}
