package com.wust.common.util.jsonschema.validate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wust.common.util.jsonschema.AbstractCommonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * 开关 属性
 * @author wanheng
 */
public class SwitchProperty extends AbstractCommonProperty {

    /**
     * 扩展参数配置信息
     */
    private String extendStr;


    /**
     * 构造器
     */
    public SwitchProperty(String key, String title, String extendStr) {
        this.type = "string";
        this.view = "switch";
        this.key = key;
        this.title = title;
        this.extendStr = extendStr;
    }

    @Override
    public Map<String, Object> getPropertyJson() {
        Map<String, Object> map = new HashMap<>(8);
        map.put("key", getKey());
        JSONObject prop = getCommonJson();
        JSONArray array;
        if (extendStr != null) {
            array = JSONArray.parseArray(extendStr);
            prop.put("extendOption", array);
        }
        map.put("prop", prop);
        return map;
    }
}
