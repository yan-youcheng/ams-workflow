package com.wust.modules.codegenerate.database.util;

/**
 * @author wanheng
 */
public class FieldUtil {

    public static boolean isInFiledList(String fieldName, String[] fieldNames) {
        if (fieldNames != null && fieldNames.length != 0) {
            for (String fieldName1 : fieldNames) {
                if (fieldName1.equals(fieldName)) {
                    return false;
                }
            }

        }
        return true;
    }

}
