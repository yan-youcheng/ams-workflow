package com.wust.modules.codegenerate.generate;

import java.util.List;
import java.util.Map;

/**
 * @author wanheng
 */
public interface IGenerate {

    /**
     * getTableMap
     * @return
     * @throws Exception
     */
    Map<String, Object> getTableMap() throws Exception;

    /**
     * generateCodeFile
     * @param stylePath
     * @return
     * @throws Exception
     */
    List<String> generateCodeFile(String stylePath) throws Exception;

    /**
     * generateCodeFile
     * @param projectPath
     * @param templatePath
     * @param stylePath
     * @return
     * @throws Exception
     */
    List<String> generateCodeFile(String projectPath, String templatePath, String stylePath) throws Exception;
}
