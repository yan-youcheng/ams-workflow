package com.wust.modules.codegenerate.generate.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.wust.modules.codegenerate.util.CodeUtil;
import com.wust.modules.codegenerate.generate.prop.FileProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import com.wust.modules.codegenerate.database.DbReadTableUtil;
import com.wust.modules.codegenerate.generate.IGenerate;
import com.wust.modules.codegenerate.generate.impl.util.GenerateUtil;
import com.wust.modules.codegenerate.generate.pojo.ColumnVo;
import com.wust.modules.codegenerate.generate.pojo.TableVo;
import com.wust.modules.codegenerate.generate.util.NonceUtils;

/**
 * @author wanheng
 */
@Slf4j
@AllArgsConstructor
public class CodeGenerateOne extends GenerateUtil implements IGenerate {

    private final TableVo tableVo;
    private List<ColumnVo> columns;
    private List<ColumnVo> originalColumns;

    @Override
    public Map<String, Object> getTableMap() throws Exception {
        HashMap hashMap = new HashMap(16);
        hashMap.put("bussiPackage", CodeUtil.bussiPackage);
        hashMap.put("entityPackage", this.tableVo.getEntityPackage());
        hashMap.put("entityName", this.tableVo.getEntityName());
        hashMap.put("tableName", this.tableVo.getTableName());
        hashMap.put("primaryKeyField", CodeUtil.dbTableId);
        if (this.tableVo.getFieldRequiredNum() == null) {
            this.tableVo.setFieldRequiredNum(StringUtils.isNotEmpty(CodeUtil.n) ? Integer.parseInt(CodeUtil.n) : -1);
        }

        if (this.tableVo.getSearchFieldNum() == null) {
            this.tableVo.setSearchFieldNum(StringUtils.isNotEmpty(CodeUtil.pageSearchFiledNum) ? Integer.parseInt(CodeUtil.pageSearchFiledNum) : -1);
        }

        if (this.tableVo.getFieldRowNum() == null) {
            this.tableVo.setFieldRowNum(Integer.parseInt(CodeUtil.q));
        }

        hashMap.put("tableVo", this.tableVo);

        try {
            if (this.columns == null || this.columns.size() == 0) {
                this.columns = DbReadTableUtil.getColumns(this.tableVo.getTableName());
            }

            hashMap.put("columns", this.columns);
            if (this.originalColumns == null || this.originalColumns.size() == 0) {
                this.originalColumns = DbReadTableUtil.getColumnList(this.tableVo.getTableName());
            }

            hashMap.put("originalColumns", this.originalColumns);

            for (ColumnVo columnVo : this.originalColumns) {
                if (columnVo.getFieldName().toLowerCase().equals(CodeUtil.dbTableId.toLowerCase())) {
                    hashMap.put("primaryKeyPolicy", columnVo.getFieldType());
                }
            }
        } catch (Exception e) {
            throw e;
        }

        long currentTimeMillis = NonceUtils.getSecureRandomLong() + NonceUtils.getCurrentTimeMillis();
        hashMap.put("serialVersionUID", String.valueOf(currentTimeMillis));
        log.info("load template data: " + hashMap.toString());
        return hashMap;
    }

    @Override
    public List<String> generateCodeFile(String stylePath) throws Exception {
        log.debug("----online---Code----Generation----[单表模型:" + this.tableVo.getTableName() + "]------- 生成中。。。");
        String filePath = CodeUtil.filePath;
        Map map = this.getTableMap();
        String templatePath = CodeUtil.templatePath;
        if ("code/code-template".equals(substring(templatePath, "/"))) {
            templatePath = "/" + substring(templatePath, "/") + "/one";
            CodeUtil.setTemplatePath(templatePath);
        }

        FileProperties fileProperties = new FileProperties(templatePath);
        fileProperties.setStylePath(stylePath);
        this.generate(fileProperties, filePath, map);
        log.info(" ----- code ---- generate  code  success =======> 表名：" + this.tableVo.getTableName() + " ");
        return this.arrayList;
    }

    @Override
    public List<String> generateCodeFile(String projectPath, String templatePath, String stylePath) throws Exception {
        if (projectPath != null && !"".equals(projectPath)) {
            CodeUtil.setFilePath(projectPath);
        }

        if (templatePath != null && !"".equals(templatePath)) {
            CodeUtil.setTemplatePath(templatePath);
        }

        this.generateCodeFile(stylePath);
        return this.arrayList;
    }
}
