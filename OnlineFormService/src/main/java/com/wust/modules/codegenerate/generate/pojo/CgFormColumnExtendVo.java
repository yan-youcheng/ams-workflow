package com.wust.modules.codegenerate.generate.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author wanheng
 */
@NoArgsConstructor
@Data
@ToString
public class CgFormColumnExtendVo {

    protected Integer fieldLength;
    protected String fieldHref;
    protected String fieldValidType;
    protected String fieldDefault;
    protected String fieldShowType;
    protected Integer fieldOrderNum;
    protected String isKey;
    protected String isShow;
    protected String isShowList;
    protected String isQuery;
    protected String queryMode;
    protected String dictField;
    protected String dictTable;
    protected String dictText;

}
