package com.wust.modules.codegenerate.generate.pojo;

import lombok.*;

/**
 * @author wanheng
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ColumnVo extends CgFormColumnExtendVo {

    private String fieldDbName;
    private String fieldName;
    private String filedComment = "";
    private String fieldType = "";
    private String fieldDbType = "";
    private String charMaxLength = "";
    private String precision;
    private String scale;
    private String nullable;
    private String classType = "";
    private String classTypeRow = "";
    private String optionType = "";

}
