package com.wust.modules.codegenerate.generate.pojo.onetomany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @author wanheng
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MainTableVo {

    private String entityPackage;
    private String tableName;
    private String entityName;
    private String ftlDescription;
    private String primaryKeyPolicy;
    private String sequenceCode;
    private String ftlMode = "A";
    List<SubTableVo> subTables;
    public Integer fieldRowNum;
    public Integer searchFieldNum;
    public Integer fieldRequiredNum;

}
