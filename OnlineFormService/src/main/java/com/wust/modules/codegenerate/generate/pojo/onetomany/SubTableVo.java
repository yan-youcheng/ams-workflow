package com.wust.modules.codegenerate.generate.pojo.onetomany;

import com.wust.modules.codegenerate.generate.pojo.ColumnVo;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @author wanheng
 */
@Data
@NoArgsConstructor
@ToString
public class SubTableVo {

    private String entityPackage;
    private String tableName;
    private String entityName;
    private String primaryKeyPolicy;
    private String sequenceCode;
    private String ftlDescription;
    private String[] originalForeignKeys;
    private String[] foreignKeys;
    private String foreignRelationType;
    private List<ColumnVo> colums;
    private List<ColumnVo> originalColumns;

}
