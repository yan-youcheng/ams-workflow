package com.wust.modules.codegenerate.generate.prop;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wanheng
 */
@NoArgsConstructor
@Slf4j
@Data
@ToString
public class FileProperties {

    private String templatePath;
    private List<File> fileList = new ArrayList();
    private String stylePath;

    public FileProperties(String templatePath) {
        log.debug("----templatePath-----------------" + templatePath);
        log.debug("----stylePath-----------------" + this.stylePath);
        this.templatePath = templatePath;
    }


    private void setFiles(File... files) {
        this.fileList = Arrays.asList(files);
    }

    public List<File> getFiles() {
        String file = this.getClass().getResource(this.templatePath).getFile();
        file = file.replaceAll("%20", " ");
        log.debug("-------classpath-------" + file);
        if (file.indexOf("/BOOT-INF/classes!") != -1) {
            file = System.getProperty("user.dir") + File.separator + "config/code/code-template-online/".replace("/", File.separator);
            log.debug("---JAR--config--classpath-------" + file);
        }

        this.setFiles(new File(file));
        return this.fileList;
    }
}
