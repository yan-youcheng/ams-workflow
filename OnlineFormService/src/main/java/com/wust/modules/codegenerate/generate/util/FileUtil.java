package com.wust.modules.codegenerate.generate.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author wanheng
 */
@Slf4j
public class FileUtil {

    public static List<String> arrayList = new ArrayList();
    public static List<String> arrayList1 = new ArrayList();

    public static List<File> getFileList(File file) {
        ArrayList arrayList2 = new ArrayList();
        addDirectory(file, (List) arrayList2);
        Collections.sort(arrayList2, (Comparator<File>) (file1, file2) -> file1.getAbsolutePath().compareTo(file2.getAbsolutePath()));
        return arrayList2;
    }

    public static void addDirectory(File file, List<File> fileList) {
        log.debug("---------dir------------path: " + file.getPath() + " -- isHidden --: " + file.isHidden() + " -- isDirectory --: " + file.isDirectory());
        if (!file.isHidden() && file.isDirectory() && !isType(file)) {
            File[] files = file.listFiles();

            for (File value : files) {
                addDirectory(value, fileList);
            }
        } else if (!isType1(file) && !isType(file)) {
            fileList.add(file);
        }

    }

    public static String getTemplateFile(File file, File file1) {
        if (file.equals(file1)) {
            return "";
        } else {
            return file.getParentFile() == null ? file1.getAbsolutePath().substring(file.getAbsolutePath().length()) : file1.getAbsolutePath().substring(file.getAbsolutePath().length() + 1);
        }
    }

    public static File mkFile(String path) {
        if (path == null) {
            throw new IllegalArgumentException("file must be not null");
        } else {
            File file = new File(path);
            mkdir(file);
            return file;
        }
    }

    public static void mkdir(File file) {
        if (file.getParentFile() != null) {
            file.getParentFile().mkdirs();
        }

    }

    private static boolean isType(File file) {
        for (String s : arrayList) {
            if (file.getName().equals(s)) {
                return true;
            }
        }

        return false;
    }

    private static boolean isType1(File file) {
        for (String s : arrayList1) {
            if (file.getName().endsWith(s)) {
                return true;
            }
        }

        return false;
    }

    static {
        arrayList.add(".svn");
        arrayList.add("CVS");
        arrayList.add(".cvsignore");
        arrayList.add(".copyarea.db");
        arrayList.add("SCCS");
        arrayList.add("vssver.scc");
        arrayList.add(".DS_Store");
        arrayList.add(".git");
        arrayList.add(".gitignore");
        arrayList1.add(".ftl");
    }
}
