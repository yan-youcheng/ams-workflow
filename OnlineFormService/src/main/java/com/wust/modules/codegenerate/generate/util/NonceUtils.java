package com.wust.modules.codegenerate.generate.util;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wanheng
 */
public class NonceUtils {

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private static final String[] ZERO = new String[]{"0", "00", "0000", "00000000"};
    private static Date date;
    private static final int i = 0;


    public static long getSecureRandomLong() {
        return (new SecureRandom()).nextLong();
    }

    public static long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }
}
