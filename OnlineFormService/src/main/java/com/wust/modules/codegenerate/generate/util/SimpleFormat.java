package com.wust.modules.codegenerate.generate.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * @author wanheng
 */
public class SimpleFormat {

    public String number(Object obj) {
        obj = obj != null && obj.toString().length() != 0 ? obj : 0;
        return "NaN".equalsIgnoreCase(obj.toString()) ? "NaN" : (new DecimalFormat("0.00")).format(Double.parseDouble(obj.toString()));
    }

    public String number(Object obj, String pattern) {
        obj = obj != null && obj.toString().length() != 0 ? obj : 0;
        return "NaN".equalsIgnoreCase(obj.toString()) ? "NaN" : (new DecimalFormat(pattern)).format(Double.parseDouble(obj.toString()));
    }

    public String round(Object obj) {
        obj = obj != null && obj.toString().length() != 0 ? obj : 0;
        return "NaN".equalsIgnoreCase(obj.toString()) ? "NaN" : (new DecimalFormat("0")).format(Double.parseDouble(obj.toString()));
    }

    public String currency(Object obj) {
        obj = obj != null && obj.toString().length() != 0 ? obj : 0;
        return NumberFormat.getCurrencyInstance(Locale.CHINA).format(obj);
    }

    public String percent(Object obj) {
        obj = obj != null && obj.toString().length() != 0 ? obj : 0;
        return "NaN".equalsIgnoreCase(obj.toString()) ? "" : NumberFormat.getPercentInstance(Locale.CHINA).format(obj);
    }

    public String date(Object obj, String pattern) {
        return obj == null ? "" : (new SimpleDateFormat(pattern)).format(obj);
    }

    public String date(Object obj) {
        return obj == null ? "" : DateFormat.getDateInstance(1, Locale.CHINA).format(obj);
    }

    public String time(Object obj) {
        return obj == null ? "" : DateFormat.getTimeInstance(3, Locale.CHINA).format(obj);
    }

    public String datetime(Object obj) {
        return obj == null ? "" : DateFormat.getDateTimeInstance(1, 3, Locale.CHINA).format(obj);
    }
}
