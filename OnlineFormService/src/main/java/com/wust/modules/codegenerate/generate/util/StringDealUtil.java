package com.wust.modules.codegenerate.generate.util;

import org.apache.commons.lang.StringUtils;

/**
 * @author 0
 */
public class StringDealUtil {

    public static String getYN(String s) {
        if (!"YES".equals(s) && !"yes".equals(s) && !"y".equals(s) && !"Y".equals(s) && !"f".equals(s)) {
            return !"NO".equals(s) && !"N".equals(s) && !"no".equals(s) && !"n".equals(s) && !"t".equals(s) ? null : "N";
        } else {
            return "Y";
        }
    }

    public static String blankDeal(String s) {
        return StringUtils.isBlank(s) ? "" : s;
    }

    public static String addQuotes(String s) {
        return "'" + s + "'";
    }
}
