package com.wust.modules.codegenerate.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import lombok.extern.slf4j.Slf4j;

/**
 * @author wanheng
 */
@Slf4j
public class CodeUtil {

    private static ResourceBundle resourceBundle = getResourceBundle("code/code_database");
    private static ResourceBundle resourceBundle1;
    public static String diverName;
    public static String url;
    public static String username;
    public static String password;
    public static String databaseName;
    public static String databaseName1;
    public static String filePath;
    public static String bussiPackage;
    public static String sourceRootPackage;
    public static String webRootPackage;
    public static String templatePath;
    public static boolean flag;
    public static String dbTableId;
    public static String n;
    public static String pageSearchFiledNum;
    public static String pageFilterFields;
    public static String q;

    private static ResourceBundle getResourceBundle(String path) {
        PropertyResourceBundle propertyResourceBundle = null;
        BufferedInputStream bufferedInputStream = null;
        String prop = System.getProperty("user.dir") + File.separator + "config" + File.separator + path + ".properties";

        try {
            bufferedInputStream = new BufferedInputStream(new FileInputStream(prop));
            propertyResourceBundle = new PropertyResourceBundle(bufferedInputStream);
            bufferedInputStream.close();
            if (propertyResourceBundle != null) {
                log.debug(" JAR方式部署，通过config目录读取配置：" + prop);
            }
        } catch (IOException e) {
        } finally {
            if (bufferedInputStream != null) {
                try {
                    bufferedInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        return propertyResourceBundle;
    }

    public static final String getDiverName() {
        return resourceBundle.getString("diver_name");
    }

    public static final String getUrl() {
        return resourceBundle.getString("url");
    }

    public static final String getUsername() {
        return resourceBundle.getString("username");
    }

    public static final String getPassword() {
        return resourceBundle.getString("password");
    }

    public static final String getDatabaseName() {
        return resourceBundle.getString("database_name");
    }


    private static String getBussiPackage() {
        return resourceBundle1.getString("bussi_package");
    }

    private static String getTemplatePath() {
        return resourceBundle1.getString("templatepath");
    }

    public static final String getSourceRootPackage() {
        return resourceBundle1.getString("source_root_package");
    }

    public static final String getWebRootPackage() {
        return resourceBundle1.getString("webroot_package");
    }

    public static final String getDbTableId() {
        return resourceBundle1.getString("db_table_id");
    }

    public static final String getPageFilterFields() {
        return resourceBundle1.getString("page_filter_fields");
    }

    public static final String getPageSearchFiledNum() {
        return resourceBundle1.getString("page_search_filed_num");
    }

    public static final String getPageFieldRequiredNum() {
        return resourceBundle1.getString("page_field_required_num");
    }

    public static String getProjectPath() {
        String projectPath = resourceBundle1.getString("project_path");
        if (projectPath != null && !"".equals(projectPath)) {
            filePath = projectPath;
        }

        return filePath;
    }

    public static void setFilePath(String filePath1) {
        filePath = filePath1;
    }

    public static void setTemplatePath(String path) {
        templatePath = path;
    }

    static {
        if (resourceBundle == null) {
            log.debug("通过class目录加载配置文件 code/code_database");
            resourceBundle = ResourceBundle.getBundle("code/code_database");
        }

        resourceBundle1 = getResourceBundle("code/code_config");
        if (resourceBundle1 == null) {
            log.debug("通过class目录加载配置文件 code/code_config");
            resourceBundle1 = ResourceBundle.getBundle("code/code_config");
        }

        diverName = "mysql";
        url = "com.mysql.jdbc.Driver";
        username = "jdbc:mysql://localhost:3306/code?useUnicode=true&characterEncoding=UTF-8";
        password = "root";
        databaseName = "root";
        databaseName1 = "code";
        filePath = "c:/workspace/code";
        bussiPackage = "com.wust";
        sourceRootPackage = "src";
        webRootPackage = "WebRoot";
        templatePath = "/code/code-template/";
        flag = true;
        n = "4";
        pageSearchFiledNum = "3";
        q = "1";
        url = getDiverName();
        username = getUrl();
        password = getUsername();
        databaseName = getPassword();
        databaseName1 = getDatabaseName();
        sourceRootPackage = getSourceRootPackage();
        webRootPackage = getWebRootPackage();
        bussiPackage = getBussiPackage();
        templatePath = getTemplatePath();
        filePath = getProjectPath();
        dbTableId = getDbTableId();
        flag = true;
        pageFilterFields = getPageFilterFields();
        pageSearchFiledNum = getPageSearchFiledNum();
        if (username.indexOf("mysql") < 0 && username.indexOf("MYSQL") < 0) {
            if (username.indexOf("oracle") < 0 && username.indexOf("ORACLE") < 0) {
                if (username.indexOf("postgresql") < 0 && username.indexOf("POSTGRESQL") < 0) {
                    if (username.indexOf("sqlserver") >= 0 || username.indexOf("sqlserver") >= 0) {
                        diverName = "sqlserver";
                    }
                } else {
                    diverName = "postgresql";
                }
            } else {
                diverName = "oracle";
            }
        } else {
            diverName = "mysql";
        }

        sourceRootPackage = sourceRootPackage.replace(".", "/");
        webRootPackage = webRootPackage.replace(".", "/");
    }
}
