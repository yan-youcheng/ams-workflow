package com.wust.modules.core.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author wanheng
 */
public class ApplicationContextUtil implements ApplicationContextAware {

    private static ApplicationContext context;

    public ApplicationContextUtil() {
    }

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        ApplicationContextUtil.context = context;
    }

    public static ApplicationContext getContext() {
        return context;
    }
}
