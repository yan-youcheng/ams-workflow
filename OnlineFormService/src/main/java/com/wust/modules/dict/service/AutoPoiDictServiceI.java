package com.wust.modules.dict.service;

/**
 * @author wanheng
 */
public interface AutoPoiDictServiceI {

    /**
     * queryDict
     * @param dictTable
     * @param dicCode
     * @param dicText
     * @return
     */
    String[] queryDict(String dictTable, String dicCode, String dicText);
}
