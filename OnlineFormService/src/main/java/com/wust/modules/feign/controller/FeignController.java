package com.wust.modules.feign.controller;

import com.wust.common.api.vo.Result;
import com.wust.common.util.LoginUserUtil;
import com.wust.common.util.SpringContextUtils;
import com.wust.modules.feign.service.UserFeignService;
import com.wust.modules.system.entity.SysUser;
import com.wust.modules.system.service.ISysService;
import com.wust.modules.system.service.impl.SysServiceImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wanheng
 * @date 2020/8/10 11:23
 */
@RestController
public class FeignController {

    @Resource
    private UserFeignService userFeignService;

    @Resource
    private ISysService sysService;

    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(@RequestParam(name = "id", required = true) String id) {
        SysUser sysUser = sysService.getSysUserById(id);
        Result.ok(sysUser);
        return Result.ok(sysUser);
    }

    @RequestMapping("/test")
    public String test(){
        SysUser loginUser = LoginUserUtil.getLoginUser();
        return loginUser.toString();
    }
}
