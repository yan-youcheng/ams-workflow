package com.wust.modules.feign.service;

import com.wust.common.api.vo.Result;
import com.wust.modules.system.entity.SysUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author wanheng
 * @date 2020/8/10 10:11
 */

@Service("userFeignService")
@FeignClient(value = "USER-SYSTEM")
public interface UserFeignService {

    @RequestMapping(value = "/sys/user/queryById", method = RequestMethod.GET)
    Result<SysUser> queryById(@RequestParam(name = "id", required = true) String id);

}
