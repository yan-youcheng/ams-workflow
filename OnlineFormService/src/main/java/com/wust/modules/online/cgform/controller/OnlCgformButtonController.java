package com.wust.modules.online.cgform.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.Arrays;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import com.wust.common.api.vo.Result;
import com.wust.common.system.query.QueryGenerator;
import com.wust.modules.online.cgform.entity.OnlCgformButton;
import com.wust.modules.online.cgform.service.IOnlCgformButtonService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wanheng
 */
@Slf4j
@RestController("onlCgformButtonController")
@RequestMapping({"/admin/cgform/button"})
@Api
public class OnlCgformButtonController {

    @Resource
    private IOnlCgformButtonService onlCgformButtonService;


    @GetMapping({"/list/{code}"})
    public Result<IPage<OnlCgformButton>> list(OnlCgformButton onlCgformButton, @RequestParam(name = "pageNo",defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize, HttpServletRequest request, @PathVariable("code") String code) {
        Result result = new Result();
        onlCgformButton.setCgformHeadId(code);
        QueryWrapper queryWrapper = QueryGenerator.initQueryWrapper(onlCgformButton, request.getParameterMap());
        Page page = new Page((long)pageNo, (long)pageSize);
        IPage iPage = this.onlCgformButtonService.page(page, queryWrapper);
        result.setSuccess(true);
        result.setResult(iPage);
        return result;
    }

    @PostMapping({"/add"})
    public Result<OnlCgformButton> add(@RequestBody OnlCgformButton onlCgformButton) {
        Result result = new Result();

        try {
            this.onlCgformButtonService.save(onlCgformButton);
            result.success("添加成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.error500("操作失败");
        }

        return result;
    }


    @PutMapping({"/edit"})
    public Result<OnlCgformButton> edit(@RequestBody OnlCgformButton onlCgformButton) {
        Result result = new Result();
        OnlCgformButton onlCgformButton1 = (OnlCgformButton)this.onlCgformButtonService.getById(onlCgformButton.getId());
        if (onlCgformButton1 == null) {
            result.error500("未找到对应实体");
        } else {
            boolean b = this.onlCgformButtonService.updateById(onlCgformButton);
            if (b) {
                result.success("修改成功!");
            }
        }

        return result;
    }

    @DeleteMapping({"/delete"})
    public Result<OnlCgformButton> delete(@RequestParam(name = "id",required = true) String id) {
        Result result = new Result();
        OnlCgformButton onlCgformButton = (OnlCgformButton)this.onlCgformButtonService.getById(id);
        if (onlCgformButton == null) {
            result.error500("未找到对应实体");
        } else {
            boolean b = this.onlCgformButtonService.removeById(id);
            if (b) {
                result.success("删除成功!");
            }
        }

        return result;
    }


    @DeleteMapping({"/deleteBatch"})
    public Result<OnlCgformButton> deleteBatch(@RequestParam(name = "ids",required = true) String ids) {
        Result result = new Result();
        if (ids != null && !"".equals(ids.trim())) {
            this.onlCgformButtonService.removeByIds(Arrays.asList(ids.split(",")));
            result.success("删除成功!");
        } else {
            result.error500("参数不识别！");
        }

        return result;
    }


    @GetMapping({"/queryById"})
    public Result<OnlCgformButton> queryById(@RequestParam(name = "id",required = true) String id) {
        Result result = new Result();
        OnlCgformButton onlCgformButton = (OnlCgformButton)this.onlCgformButtonService.getById(id);
        if (onlCgformButton == null) {
            result.error500("未找到对应实体");
        } else {
            result.setResult(onlCgformButton);
            result.setSuccess(true);
        }

        return result;
    }
}
