package com.wust.modules.online.cgform.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import com.wust.common.api.vo.Result;
import com.wust.common.system.query.QueryGenerator;
import com.wust.modules.online.cgform.entity.OnlCgformField;
import com.wust.modules.online.cgform.entity.OnlCgformHead;
import com.wust.modules.online.cgform.service.IOnlCgformFieldService;
import com.wust.modules.online.cgform.service.IOnlCgformHeadService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wanheng
 */
@Slf4j
@RestController("onlCgformFieldController")
@RequestMapping({"/admin/cgform/field"})
@Api
public class OnlCgformFieldController {

    @Resource
    private IOnlCgformHeadService onlCgformHeadService;

    @Resource
    private IOnlCgformFieldService onlCgformFieldService;


    @GetMapping({"/listByHeadCode"})
    public Result<?> listByHeadCode(@RequestParam("headCode") String headCode) {
        LambdaQueryWrapper<OnlCgformHead> onlCgformHeadLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformHeadLambdaQueryWrapper.eq(OnlCgformHead::getTableName, headCode);
        OnlCgformHead onlCgformHead = this.onlCgformHeadService.getOne(onlCgformHeadLambdaQueryWrapper);
        return onlCgformHead == null ? Result.error("不存在的code") : this.listByHeadId(onlCgformHead.getId());
    }

    @GetMapping({"/listByHeadId"})
    public Result<?> listByHeadId(@RequestParam("headId") String headId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("cgform_head_id", headId);
        queryWrapper.orderByAsc("order_num");
        List onlCgformFields = this.onlCgformFieldService.list(queryWrapper);
        if (onlCgformFields.size() == 0) {
            return Result.error("表不存在");
        }
        return Result.ok(onlCgformFields);
    }

    @GetMapping({"/list"})
    public Result<IPage<OnlCgformField>> list(OnlCgformField onlCgformField, @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest request) {
        Result result = new Result();
        QueryWrapper queryWrapper = QueryGenerator.initQueryWrapper(onlCgformField, request.getParameterMap());
        Page page = new Page((long) pageNo, (long) pageSize);
        IPage iPage = this.onlCgformFieldService.page(page, queryWrapper);
        result.setSuccess(true);
        result.setResult(iPage);
        return result;
    }

    @PostMapping({"/add"})
    public Result<OnlCgformField> add(@RequestBody OnlCgformField onlCgformField) {
        Result result = new Result();

        try {
            this.onlCgformFieldService.save(onlCgformField);
            result.success("添加成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.error500("操作失败");
        }

        return result;
    }

    @PutMapping({"/edit"})
    public Result<OnlCgformField> edit(@RequestBody OnlCgformField onlCgformField) {
        Result result = new Result();
        OnlCgformField onlCgformField1 = (OnlCgformField) this.onlCgformFieldService.getById(onlCgformField.getId());
        if (onlCgformField1 == null) {
            result.error500("未找到对应实体");
        } else {
            boolean b = this.onlCgformFieldService.updateById(onlCgformField);
            if (b) {
                result.success("修改成功!");
            }
        }

        return result;
    }

    @DeleteMapping({"/delete"})
    public Result<OnlCgformField> delete(@RequestParam(name = "id", required = true) String id) {
        Result result = new Result();
        OnlCgformField onlCgformField = (OnlCgformField) this.onlCgformFieldService.getById(id);
        if (onlCgformField == null) {
            result.error500("未找到对应实体");
        } else {
            boolean b = this.onlCgformFieldService.removeById(id);
            if (b) {
                result.success("删除成功!");
            }
        }

        return result;
    }

    @DeleteMapping({"/deleteBatch"})
    public Result<OnlCgformField> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        Result result = new Result();
        if (ids != null && !"".equals(ids.trim())) {
            this.onlCgformFieldService.removeByIds(Arrays.asList(ids.split(",")));
            result.success("删除成功!");
        } else {
            result.error500("参数不识别！");
        }

        return result;
    }

    @GetMapping({"/queryById"})
    public Result<OnlCgformField> queryById(@RequestParam(name = "id", required = true) String id) {
        Result result = new Result();
        OnlCgformField onlCgformField = (OnlCgformField) this.onlCgformFieldService.getById(id);
        if (onlCgformField == null) {
            result.error500("未找到对应实体");
        } else {
            result.setResult(onlCgformField);
            result.setSuccess(true);
        }

        return result;
    }
}
