package com.wust.modules.online.cgform.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import com.wust.common.api.vo.Result;
import com.wust.common.constant.CommonConstant;
import com.wust.common.system.query.QueryGenerator;
import com.wust.modules.online.cgform.entity.OnlCgformIndex;
import com.wust.modules.online.cgform.service.IOnlCgformIndexService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wanheng
 */
@Slf4j
@Api
@RestController("onlCgformIndexController")
@RequestMapping({"/admin/cgform/index"})
public class OnlCgformIndexController {

    @Resource
    private IOnlCgformIndexService onlCgformIndexService;

    @GetMapping({"/listByHeadId"})
    public Result<?> listByHeadId(@RequestParam("headId") String headId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("cgform_head_id", headId);
        queryWrapper.eq("del_flag", CommonConstant.DEL_FLAG_0);
        queryWrapper.orderByDesc("create_time");
        List onlCgformIndexs = this.onlCgformIndexService.list(queryWrapper);
        return Result.ok(onlCgformIndexs);
    }

    @GetMapping({"/list"})
    public Result<IPage<OnlCgformIndex>> list(OnlCgformIndex onlCgformIndex, @RequestParam(name = "pageNo",defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize, HttpServletRequest request) {
        Result result = new Result();
        QueryWrapper onlCgformIndexQueryWrapper = QueryGenerator.initQueryWrapper(onlCgformIndex, request.getParameterMap());
        Page page = new Page((long) pageNo, (long) pageSize);
        IPage iPage = this.onlCgformIndexService.page(page, onlCgformIndexQueryWrapper);
        result.setSuccess(true);
        result.setResult(iPage);
        return result;
    }

    @PostMapping({"/add"})
    public Result<OnlCgformIndex> add(@RequestBody OnlCgformIndex onlCgformIndex) {
        Result result = new Result();

        try {
            this.onlCgformIndexService.save(onlCgformIndex);
            result.success("添加成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.error500("操作失败");
        }

        return result;
    }

    @PutMapping({"/edit"})
    public Result<OnlCgformIndex> edit(@RequestBody OnlCgformIndex onlCgformIndex) {
        Result result = new Result();
        OnlCgformIndex onlCgformIndex1 = (OnlCgformIndex)this.onlCgformIndexService.getById(onlCgformIndex.getId());
        if (onlCgformIndex1 == null) {
            result.error500("未找到对应实体");
        } else {
            boolean b = this.onlCgformIndexService.updateById(onlCgformIndex);
            if (b) {
                result.success("修改成功!");
            }
        }

        return result;
    }

    @DeleteMapping({"/delete"})
    public Result<OnlCgformIndex> delete(@RequestParam(name = "id",required = true) String id) {
        Result result = new Result();
        OnlCgformIndex onlCgformIndex = (OnlCgformIndex)this.onlCgformIndexService.getById(id);
        if (onlCgformIndex == null) {
            result.error500("未找到对应实体");
        } else {
            boolean b = this.onlCgformIndexService.removeById(id);
            if (b) {
                result.success("删除成功!");
            }
        }

        return result;
    }

    @DeleteMapping({"/deleteBatch"})
    public Result<OnlCgformIndex> deleteBatch(@RequestParam(name = "ids",required = true) String ids) {
        Result result = new Result();
        if (ids != null && !"".equals(ids.trim())) {
            this.onlCgformIndexService.removeByIds(Arrays.asList(ids.split(",")));
            result.success("删除成功!");
        } else {
            result.error500("参数不识别！");
        }

        return result;
    }

    @GetMapping({"/queryById"})
    public Result<OnlCgformIndex> queryById(@RequestParam(name = "id",required = true) String id) {
        Result result = new Result();
        OnlCgformIndex onlCgformIndex = (OnlCgformIndex)this.onlCgformIndexService.getById(id);
        if (onlCgformIndex == null) {
            result.error500("未找到对应实体");
        } else {
            result.setResult(onlCgformIndex);
            result.setSuccess(true);
        }

        return result;
    }
}
