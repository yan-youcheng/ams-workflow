package com.wust.modules.online.cgform.converter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.wust.common.util.MyClassLoader;
import com.wust.common.util.SpringContextUtils;
import com.wust.common.util.oConvertUtils;
import com.wust.modules.online.cgform.converter.impl.*;
import com.wust.modules.online.cgform.entity.OnlCgformField;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wanheng
 */
@Slf4j
@NoArgsConstructor
public class ControlTypeUtil {


    public static FieldCommentConverter getFieldCommentConverter(OnlCgformField onlCgformField) {
        String fieldShowType = onlCgformField.getFieldShowType();
        FieldCommentConverter object = null;
        byte bt = -1;
        switch(fieldShowType.hashCode()) {
            case -1624761913:
                if ("link_down".equals(fieldShowType)) {
                    bt = 7;
                }
                break;
            case 110798:
                if ("pca".equals(fieldShowType)) {
                    bt = 10;
                }
                break;
            case 3322014:
                if ("list".equals(fieldShowType)) {
                    bt = 0;
                }
                break;
            case 45359719:
                if ("cat_tree".equals(fieldShowType)) {
                    bt = 6;
                }
                break;
            case 108270587:
                if ("radio".equals(fieldShowType)) {
                    bt = 1;
                }
                break;
            case 702184024:
                if ("list_multi".equals(fieldShowType)) {
                    bt = 2;
                }
                break;
            case 1186535523:
                if ("sel_tree".equals(fieldShowType)) {
                    bt = 5;
                }
                break;
            case 1186566288:
                if ("sel_user".equals(fieldShowType)) {
                    bt = 9;
                }
                break;
            case 1536891843:
                if ("checkbox".equals(fieldShowType)) {
                    bt = 3;
                }
                break;
            case 1624559481:
                if ("sel_depart".equals(fieldShowType)) {
                    bt = 8;
                }
                break;
            case 2053565741:
                if ("sel_search".equals(fieldShowType)) {
                    bt = 4;
                }
            default:
        }

        switch(bt) {
            case 0:
            case 1:
                object = new FieldCommentConverterC(onlCgformField);
                break;
            case 2:
            case 3:
                object = new FieldCommentConverterF(onlCgformField);
                break;
            case 4:
                object = new FieldCommentConverterD(onlCgformField);
                break;
            case 5:
                object = new FieldCommentConverterH(onlCgformField);
                break;
            case 6:
                object = new FieldCommentConverterA(onlCgformField);
                break;
            case 7:
                object = new FieldCommentConverterE(onlCgformField);
                break;
            case 8:
                object = new FieldCommentConverterB(onlCgformField);
                break;
            case 9:
                object = new FieldCommentConverterI(onlCgformField);
                break;
            case 10:
                object = new FieldCommentConverterG(onlCgformField);
                break;
            default:
        }

        return object;
    }

    public static Map<String, FieldCommentConverter> getFieldCommentConverters(List<OnlCgformField> onlCgformFields) {
        HashMap hashMap = new HashMap(16);

        for (OnlCgformField onlCgformField : onlCgformFields) {
            FieldCommentConverter fieldCommentConverter;
            if (oConvertUtils.isNotEmpty(onlCgformField.getConverter())) {
                fieldCommentConverter = getFieldCommentConverter(onlCgformField.getConverter().trim());
            } else {
                fieldCommentConverter = getFieldCommentConverter(onlCgformField);
            }

            if (fieldCommentConverter != null) {
                hashMap.put(onlCgformField.getDbFieldName(), fieldCommentConverter);
            }
        }

        return hashMap;
    }

    private static FieldCommentConverter getFieldCommentConverter(String s) {
        Object object = null;
        if (s.indexOf(".") > 0) {
            try {
                object = MyClassLoader.getClassByScn(s).newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                log.error(e.getMessage(), e);
            }
        } else {
            object = SpringContextUtils.getBean(s);
        }

        if (object instanceof FieldCommentConverter) {
            return (FieldCommentConverter) object;
        } else {
            return null;
        }
    }
}
