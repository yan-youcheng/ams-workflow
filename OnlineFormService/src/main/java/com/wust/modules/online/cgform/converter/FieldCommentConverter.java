package com.wust.modules.online.cgform.converter;

import java.util.Map;

/**
 * @author wanheng
 */
public interface FieldCommentConverter {

  /**
   * val转txt
   * @param txt
   * @return
   */
  String converterToVal(String txt);

  /**
   * txt转val
   * @param val
   * @return
   */
  String converterToTxt(String val);

  /**
   * 获取属性
   * @return
   */
  Map<String, String> getConfig();
}
