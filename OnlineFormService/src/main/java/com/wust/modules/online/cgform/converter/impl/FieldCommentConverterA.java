package com.wust.modules.online.cgform.converter.impl;

import java.util.HashMap;
import java.util.Map;

import com.wust.common.util.oConvertUtils;
import com.wust.modules.online.cgform.entity.OnlCgformField;
import lombok.Data;

/**
 * @author wanheng
 */
@Data
public class FieldCommentConverterA extends TableFieldCommentConverter {
    private String treeText;


    public FieldCommentConverterA(OnlCgformField onlCgformField) {
        super("SYS_CATEGORY", "ID", "NAME");
        this.treeText = onlCgformField.getDictText();
        this.field = onlCgformField.getDbFieldName();
    }

    @Override
    public Map<String, String> getConfig() {
        if (oConvertUtils.isEmpty(this.treeText)) {
            return null;
        } else {
            HashMap hashMap = new HashMap(4);
            hashMap.put("treeText", this.treeText);
            hashMap.put("field", this.field);
            return hashMap;
        }
    }
}
