package com.wust.modules.online.cgform.converter.impl;

import java.util.ArrayList;
import java.util.List;

import com.wust.common.system.api.SysBaseApi;
import com.wust.common.util.SpringContextUtils;
import com.wust.common.util.oConvertUtils;
import com.wust.modules.online.cgform.entity.OnlCgformField;

/**
 * @author wanheng
 */
public class FieldCommentConverterB extends FieldFieldCommentConverter {

    public FieldCommentConverterB(OnlCgformField onlCgformField) {
        SysBaseApi sysBaseApi = SpringContextUtils.getBean(SysBaseApi.class);
        String sysDepart = "SYS_DEPART";
        String departName = "DEPART_NAME";
        String id = "ID";
        this.dictList = sysBaseApi.queryTableDictItemsByCode(sysDepart, departName, id);
        this.filed = onlCgformField.getDbFieldName();
    }

    @Override
    public String converterToVal(String txt) {
        if (oConvertUtils.isEmpty(txt)) {
            return null;
        } else {
            ArrayList arrayList = new ArrayList();
            String[] strings = txt.split(",");
            for (String s : strings) {
                String s1 = super.converterToVal(s);
                if (s1 != null) {
                    arrayList.add(s1);
                }
            }

            return String.join(",", arrayList);
        }
    }

    @Override
    public String converterToTxt(String val) {
        if (oConvertUtils.isEmpty(val)) {
            return null;
        } else {
            ArrayList arrayList = new ArrayList();
            String[] strings = val.split(",");

            for (String s : strings) {
                String s1 = super.converterToTxt(s);
                if (s1 != null) {
                    arrayList.add(s1);
                }
            }

            return String.join(",", arrayList);
        }
    }
}
