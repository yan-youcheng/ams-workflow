package com.wust.modules.online.cgform.converter.impl;

import java.util.ArrayList;
import java.util.List;

import com.wust.common.system.api.SysBaseApi;
import com.wust.common.util.SpringContextUtils;
import com.wust.common.util.oConvertUtils;
import com.wust.modules.online.cgform.entity.OnlCgformField;

/**
 * @author wanheng
 */
public class FieldCommentConverterC extends FieldFieldCommentConverter {

    public FieldCommentConverterC(OnlCgformField onlCgformField) {
        SysBaseApi sysBaseApi = SpringContextUtils.getBean(SysBaseApi.class);
        String dictTable = onlCgformField.getDictTable();
        String dictText = onlCgformField.getDictText();
        String dictField = onlCgformField.getDictField();
        Object arrayList = new ArrayList();
        if (oConvertUtils.isNotEmpty(dictTable)) {
            arrayList = sysBaseApi.queryTableDictItemsByCode(dictTable, dictText, dictField);
        } else if (oConvertUtils.isNotEmpty(dictField)) {
            arrayList = sysBaseApi.queryDictItemsByCode(dictField);
        }

        this.dictList = (List) arrayList;
        this.filed = onlCgformField.getDbFieldName();
    }
}
