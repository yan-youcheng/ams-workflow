package com.wust.modules.online.cgform.converter.impl;

import com.wust.modules.online.cgform.entity.OnlCgformField;

/**
 * @author wanheng
 */
public class FieldCommentConverterD extends TableFieldCommentConverter {

    public FieldCommentConverterD(OnlCgformField onlCgformField) {
        super(onlCgformField.getDictTable(), onlCgformField.getDictField(), onlCgformField.getDictText());
    }
}
