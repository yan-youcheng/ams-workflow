package com.wust.modules.online.cgform.converter.impl;

import com.alibaba.fastjson.JSONObject;
import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import com.wust.modules.online.cgform.entity.OnlCgformField;
import com.wust.modules.online.cgform.model.LinkDown;

/**
 * @author wanheng
 */
@Data
public class FieldCommentConverterE extends TableFieldCommentConverter {

    private String linkField;

    public FieldCommentConverterE(OnlCgformField onlCgformField) {
        String dictTable = onlCgformField.getDictTable();
        LinkDown linkDown = JSONObject.parseObject(dictTable, LinkDown.class);
        this.setTable(linkDown.getTable());
        this.setCode(linkDown.getKey());
        this.setText(linkDown.getTxt());
        this.linkField = linkDown.getLinkField();
    }

    @Override
    public Map<String, String> getConfig() {
        HashMap hashMap = new HashMap(4);
        hashMap.put("linkField", this.linkField);
        return hashMap;
    }
}