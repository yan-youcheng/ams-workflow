package com.wust.modules.online.cgform.converter.impl;

import java.util.ArrayList;
import java.util.List;

import com.wust.common.system.api.SysBaseApi;
import com.wust.common.util.SpringContextUtils;
import com.wust.common.util.oConvertUtils;
import com.wust.modules.online.cgform.entity.OnlCgformField;

/**
 * @author wanheng
 */
public class FieldCommentConverterF extends FieldFieldCommentConverter {

    public FieldCommentConverterF(OnlCgformField onlCgformField) {
        SysBaseApi sysBaseAPI = SpringContextUtils.getBean(SysBaseApi.class);
        String dictTable = onlCgformField.getDictTable();
        String dictText = onlCgformField.getDictText();
        String dictField = onlCgformField.getDictField();
        Object arrayList = new ArrayList();
        if (oConvertUtils.isNotEmpty(dictTable)) {
            arrayList = sysBaseAPI.queryTableDictItemsByCode(dictTable, dictText, dictField);
        } else if (oConvertUtils.isNotEmpty(dictField)) {
            arrayList = sysBaseAPI.queryDictItemsByCode(dictField);
        }

        this.dictList = (List) arrayList;
        this.filed = onlCgformField.getDbFieldName();
    }

    @Override
    public String converterToVal(String txt) {
        if (oConvertUtils.isEmpty(txt)) {
            return null;
        } else {
            ArrayList arrayList = new ArrayList();
            String[] strings = txt.split(",");

            for (String s : strings) {
                String s1 = super.converterToVal(s);
                if (s1 != null) {
                    arrayList.add(s1);
                }
            }

            return String.join(",", arrayList);
        }
    }

    @Override
    public String converterToTxt(String val) {
        if (oConvertUtils.isEmpty(val)) {
            return null;
        } else {
            ArrayList arrayList = new ArrayList();
            String[] strings = val.split(",");
            int length = strings.length;

            for(int i = 0; i < length; ++i) {
                String s = strings[i];
                String s1 = super.converterToTxt(s);
                if (s1 != null) {
                    arrayList.add(s1);
                }
            }

            return String.join(",", arrayList);
        }
    }
}
