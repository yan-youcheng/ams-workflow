package com.wust.modules.online.cgform.converter.impl;

import com.wust.common.constant.ProvinceCityArea;
import com.wust.common.util.SpringContextUtils;
import com.wust.common.util.oConvertUtils;
import com.wust.modules.online.cgform.entity.OnlCgformField;

/**
 * @author wanheng
 */
public class FieldCommentConverterG extends FieldFieldCommentConverter {

    ProvinceCityArea provinceCityArea;

    public FieldCommentConverterG(OnlCgformField onlCgformField) {
        this.filed = onlCgformField.getDbFieldName();
        this.provinceCityArea = SpringContextUtils.getBean(ProvinceCityArea.class);
    }

    @Override
    public String converterToVal(String txt) {
        return oConvertUtils.isEmpty(txt) ? null : this.provinceCityArea.getCode(txt);
    }

    @Override
    public String converterToTxt(String val) {
        return oConvertUtils.isEmpty(val) ? null : this.provinceCityArea.getText(val);
    }
}
