package com.wust.modules.online.cgform.converter.impl;

import com.wust.modules.online.cgform.entity.OnlCgformField;

/**
 * @author wanheng
 */
public class FieldCommentConverterH extends TableFieldCommentConverter {
    public FieldCommentConverterH(OnlCgformField onlCgformField) {

        String dictText = onlCgformField.getDictText();
        String[] strings = dictText.split(",");
        this.setTable(onlCgformField.getDictTable());
        this.setCode(strings[0]);
        this.setText(strings[2]);
    }
}
