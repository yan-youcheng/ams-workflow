package com.wust.modules.online.cgform.converter.impl;

import java.util.ArrayList;
import java.util.List;

import com.wust.common.system.api.SysBaseApi;
import com.wust.common.util.SpringContextUtils;
import com.wust.common.util.oConvertUtils;
import com.wust.modules.online.cgform.entity.OnlCgformField;

/**
 * @author wanheng
 */
public class FieldCommentConverterI extends FieldFieldCommentConverter {

    public FieldCommentConverterI(OnlCgformField onlCgformField) {
        SysBaseApi sysBaseApi = SpringContextUtils.getBean(SysBaseApi.class);
        String sysUser = "SYS_USER";
        String realname = "REALNAME";
        String username = "USERNAME";
        this.dictList = sysBaseApi.queryTableDictItemsByCode(sysUser, realname, username);
        this.filed = onlCgformField.getDbFieldName();
    }

    @Override
    public String converterToVal(String txt) {
        if (oConvertUtils.isEmpty(txt)) {
            return null;
        } else {
            ArrayList arrayList = new ArrayList();
            String[] strings = txt.split(",");

            for (String s : strings) {
                String s1 = super.converterToVal(s);
                if (s1 != null) {
                    arrayList.add(s1);
                }
            }

            return String.join(",", arrayList);
        }
    }

    @Override
    public String converterToTxt(String val) {
        if (oConvertUtils.isEmpty(val)) {
            return null;
        } else {
            ArrayList arrayList = new ArrayList();
            String[] strings = val.split(",");

            for (String s : strings) {
                String s1 = super.converterToTxt(s);
                if (s1 != null) {
                    arrayList.add(s1);
                }
            }

            return String.join(",", arrayList);
        }
    }
}
