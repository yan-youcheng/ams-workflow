package com.wust.modules.online.cgform.converter.impl;

import com.wust.common.system.vo.DictModel;
import com.wust.common.util.oConvertUtils;
import com.wust.modules.online.cgform.converter.FieldCommentConverter;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author wanheng
 */
@Data
@NoArgsConstructor
public class FieldFieldCommentConverter implements FieldCommentConverter {
    protected String filed;
    protected List<DictModel> dictList;

    @Override
    public String converterToVal(String txt) {
        if (oConvertUtils.isNotEmpty(txt)) {

            for (DictModel dictModel : this.dictList) {
                if (dictModel.getText().equals(txt)) {
                    return dictModel.getValue();
                }
            }
        }

        return null;
    }

    @Override
    public String converterToTxt(String val) {
        if (oConvertUtils.isNotEmpty(val)) {

            for (DictModel dictModel : this.dictList) {
                if (dictModel.getValue().equals(val)) {
                    return dictModel.getText();
                }
            }
        }

        return null;
    }

    @Override
    public Map<String, String> getConfig() {
        return null;
    }
}
