package com.wust.modules.online.cgform.converter.impl;

import java.util.List;
import java.util.Map;

import com.wust.common.system.api.SysBaseApi;
import com.wust.common.system.vo.DictModel;
import com.wust.common.util.SpringContextUtils;
import com.wust.common.util.oConvertUtils;
import lombok.Data;
import com.wust.modules.online.cgform.converter.FieldCommentConverter;

/**
 * @author wanheng
 */
@Data
public class TableFieldCommentConverter implements FieldCommentConverter {

    protected SysBaseApi sysBaseApi;
    protected String field;
    protected String table;
    protected String code;
    protected String text;

    public TableFieldCommentConverter() {
        this.sysBaseApi = SpringContextUtils.getBean(SysBaseApi.class);
    }

    public TableFieldCommentConverter(String table, String code, String text) {
        this();
        this.table = table;
        this.code = code;
        this.text = text;
    }


    @Override
    public String converterToVal(String txt) {
        if (oConvertUtils.isNotEmpty(txt)) {
            String s = this.text + "= '" + txt + "'";
            String s1;
            int where = this.table.indexOf("where");
            if (where > 0) {
                s1 = this.table.substring(0, where).trim();
                s = s + " and " + this.table.substring(where + 5);
            } else {
                s1 = this.table;
            }

            List filterTableDictInfo = this.sysBaseApi.queryFilterTableDictInfo(s1, this.text, this.code, s);
            if (filterTableDictInfo != null && filterTableDictInfo.size() > 0) {
                return ((DictModel) filterTableDictInfo.get(0)).getValue();
            }
        }

        return null;
    }

    @Override
    public String converterToTxt(String val) {
        if (oConvertUtils.isNotEmpty(val)) {
            String s = this.code + "= '" + val + "'";
            String s1;
            int where = this.table.indexOf("where");
            if (where > 0) {
                s1 = this.table.substring(0, where).trim();
                s = s + " and " + this.table.substring(where + 5);
            } else {
                s1 = this.table;
            }

            List queryFilterTableDictInfo = this.sysBaseApi.queryFilterTableDictInfo(s1, this.text, this.code, s);
            if (queryFilterTableDictInfo != null && queryFilterTableDictInfo.size() > 0) {
                return ((DictModel) queryFilterTableDictInfo.get(0)).getText();
            }
        }

        return null;
    }

    @Override
    public Map<String, String> getConfig() {
        return null;
    }
}
