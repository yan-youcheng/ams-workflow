package com.wust.modules.online.cgform.enhance;

import com.alibaba.fastjson.JSONObject;
import java.util.Map;

import com.wust.modules.online.config.exception.BusinessException;

/**
 * @author wanheng
 */
public interface CgformEnhanceJavaInter {

  /**
   * 执行Java
   * @param s
   * @param jsonObject
   * @return
   * @throws BusinessException
   */
  int execute(String s, JSONObject jsonObject) throws BusinessException;
}
