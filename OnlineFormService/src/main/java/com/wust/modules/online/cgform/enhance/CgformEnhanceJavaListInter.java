package com.wust.modules.online.cgform.enhance;

import java.util.List;
import java.util.Map;

import com.wust.modules.online.config.exception.BusinessException;

/**
 * @author wanheng
 */
public interface CgformEnhanceJavaListInter {

  /**
   * 执行JavaList
   * @param s
   * @param mapList
   * @throws BusinessException
   */
  void execute(String s, List<Map<String, Object>> mapList) throws BusinessException;
}
