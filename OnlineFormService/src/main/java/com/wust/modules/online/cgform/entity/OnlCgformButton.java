package com.wust.modules.online.cgform.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

/**
 * @author wanheng
 */
@NoArgsConstructor
@Data
@EqualsAndHashCode
@ToString
@TableName("onl_cgform_button")
@ApiModel
public class OnlCgformButton implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(
            type = IdType.UUID
    )
    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("表单ID")
    private String cgformHeadId;

    @ApiModelProperty("按钮编码")
    private String buttonCode;

    @ApiModelProperty("按钮名称")
    private String buttonName;

    @ApiModelProperty("按钮样式")
    private String buttonStyle;

    @ApiModelProperty("按钮类型")
    private String optType;

    @ApiModelProperty("表达式")
    private String exp;

    @ApiModelProperty("按钮状态")
    private String buttonStatus;

    @ApiModelProperty("排序")
    private Integer orderNum;

    @ApiModelProperty("按钮图标")
    private String buttonIcon;

    @ApiModelProperty("按钮位置1侧面 2底部")
    private String optPosition;

}
