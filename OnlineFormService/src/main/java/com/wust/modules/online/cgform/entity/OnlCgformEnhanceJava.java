package com.wust.modules.online.cgform.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author wanheng
 */
@NoArgsConstructor
@Data
@EqualsAndHashCode
@ToString
@TableName("onl_cgform_enhance_java")
public class OnlCgformEnhanceJava implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(
            type = IdType.UUID
    )
    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("表单ID")
    private String cgformHeadId;

    @ApiModelProperty("按钮编码")
    private String buttonCode;

    @ApiModelProperty("类型")
    private String cgJavaType;

    @ApiModelProperty("数值")
    private String cgJavaValue;

    @ApiModelProperty("生效状态")
    private String activeStatus;

    @ApiModelProperty("事件状态(end:结束，start:开始)")
    private String event;

}
