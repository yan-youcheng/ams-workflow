package com.wust.modules.online.cgform.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author wanheng
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@TableName("onl_cgform_enhance_js")
public class OnlCgformEnhanceJs implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(
            type = IdType.UUID
    )
    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("表单ID")
    private String cgformHeadId;

    @ApiModelProperty("类型")
    private String cgJsType;

    @ApiModelProperty("JS增强内容")
    private String cgJs;

    @ApiModelProperty("备注")
    private String content;


}
