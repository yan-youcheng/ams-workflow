package com.wust.modules.online.cgform.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author wanheng
 */
@Data
@EqualsAndHashCode
@NoArgsConstructor
@ToString
@TableName("onl_cgform_field")
public class OnlCgformField implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(
            type = IdType.UUID
    )
    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("表ID")
    private String cgformHeadId;

    @ApiModelProperty("字段名字")
    private String dbFieldName;

    @ApiModelProperty("字段备注")
    private String dbFieldTxt;

    @ApiModelProperty("原字段名")
    private String dbFieldNameOld;

    @ApiModelProperty("是否主键 0否 1是")
    private Integer dbIsKey;

    @ApiModelProperty("是否允许为空0否 1是")
    private Integer dbIsNull;

    @ApiModelProperty("数据库字段类型")
    private String dbType;

    @ApiModelProperty("数据库字段长度")
    private Integer dbLength;

    @ApiModelProperty("小数点")
    private Integer dbPointLength;

    @ApiModelProperty("表字段默认值")
    private String dbDefaultVal;

    @ApiModelProperty("字典code")
    private String dictField;

    @ApiModelProperty("字典表")
    private String dictTable;

    @ApiModelProperty("字典Text")
    private String dictText;

    @ApiModelProperty("表单控件类型")
    private String fieldShowType;

    @ApiModelProperty("跳转URL")
    private String fieldHref;

    @ApiModelProperty("表单控件长度")
    private Integer fieldLength;

    @ApiModelProperty("表单字段校验规则")
    private String fieldValidType;

    @ApiModelProperty("字段是否必填")
    private String fieldMustInput;

    @ApiModelProperty("扩展参数JSON")
    private String fieldExtendJson;

    @ApiModelProperty("控件默认值，不同的表达式展示不同的结果。\n" +
            "1. 纯字符串直接赋给默认值；\n" +
            "2. #{普通变量}；\n" +
            "3. {{ 动态JS表达式 }}；\n" +
            "4. ${填值规则编码}；\n" +
            "填值规则表达式只允许存在一个，且不能和其他规则混用。")
    private String fieldDefaultValue;

    @ApiModelProperty("是否查询条件0否 1是")
    private Integer isQuery;

    @ApiModelProperty("表单是否显示0否 1是")
    private Integer isShowForm;

    @ApiModelProperty("列表是否显示0否 1是")
    private Integer isShowList;

    @ApiModelProperty("是否是只读（1是 0否）")
    private Integer isReadOnly;

    @ApiModelProperty("查询模式")
    private String queryMode;

    @ApiModelProperty("外键主表名")
    private String mainTable;

    @ApiModelProperty("外键主键字段")
    private String mainField;

    @ApiModelProperty("排序")
    private Integer orderNum;

    @ApiModelProperty("修改人")
    private String updateBy;

    @JsonFormat(
            timezone = "GMT+8",
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @ApiModelProperty("修改时间")
    private Date updateTime;

    @JsonFormat(
            timezone = "GMT+8",
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("创建人")
    private String createBy;

    @ApiModelProperty("自定义值转换器")
    private String converter;

    @ApiModelProperty("是否启用查询配置1是0否")
    private String queryConfigFlag;

    @ApiModelProperty("查询默认值")
    private String queryDefVal;

    @ApiModelProperty("查询配置字典text")
    private String queryDictText;

    @ApiModelProperty("查询配置字典code")
    private String queryDictField;

    @ApiModelProperty("查询配置字典table")
    private String queryDictTable;

    @ApiModelProperty("查询显示控件")
    private String queryShowType;

    @ApiModelProperty("查询字段校验类型")
    private String queryValidType;

    @ApiModelProperty("查询字段是否必填1是0否")
    private String queryMustInput;

    @ApiModelProperty("是否支持排序1是0否")
    private String sortFlag;

}
