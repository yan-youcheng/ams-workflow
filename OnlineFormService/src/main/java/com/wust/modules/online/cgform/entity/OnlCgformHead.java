package com.wust.modules.online.cgform.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author wanheng
 */
@EqualsAndHashCode
@Data
@NoArgsConstructor
@ToString
@TableName("onl_cgform_head")
public class OnlCgformHead implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(
            type = IdType.UUID
    )
    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("表名")
    private String tableName;

    @ApiModelProperty("表类型: 0单表、1主表、2附表")
    private Integer tableType;

    @ApiModelProperty("表版本")
    private Integer tableVersion;

    @ApiModelProperty("表描述")
    private String tableTxt;

    @ApiModelProperty("是否带checkbox")
    private String isCheckbox;

    @ApiModelProperty("同步数据库状态")
    private String isDbSynch;

    @ApiModelProperty("是否分页")
    private String isPage;

    @ApiModelProperty("是否是树")
    private String isTree;

    @ApiModelProperty("主键生成序列")
    private String idSequence;

    @ApiModelProperty("主键类型")
    private String idType;

    @ApiModelProperty("查询模式")
    private String queryMode;

    @ApiModelProperty("映射关系 0一对多  1一对一")
    private Integer relationType;

    @ApiModelProperty("子表")
    private String subTableStr;

    @ApiModelProperty("附表排序序号")
    private Integer tabOrderNum;

    @ApiModelProperty("树形表单父id")
    private String treeParentIdField;

    @ApiModelProperty("树表主键字段")
    private String treeIdField;

    @ApiModelProperty("树开表单列字段")
    private String treeFieldname;

    @ApiModelProperty("表单分类")
    private String formCategory;

    @ApiModelProperty("PC表单模板")
    private String formTemplate;

    @ApiModelProperty("主题模板")
    private String themeTemplate;

    @ApiModelProperty("表单模板样式(移动端)")
    private String formTemplateMobile;

    @ApiModelProperty("修改人")
    private String updateBy;

    @JsonFormat(
            timezone = "GMT+8",
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @ApiModelProperty("修改时间")
    private Date updateTime;

    @ApiModelProperty("创建人")
    private String createBy;

    @JsonFormat(
            timezone = "GMT+8",
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("复制表类型1为复制表 0为原始表")
    private Integer copyType;

    @ApiModelProperty("复制版本号")
    private Integer copyVersion;

    @ApiModelProperty("原始表ID")
    private String physicId;

    private transient Integer hascopy;

    private Integer scroll;

    private transient String taskId;

}
