package com.wust.modules.online.cgform.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author wanheng
 */
@NoArgsConstructor
@Data
@EqualsAndHashCode
@ToString
@TableName("onl_cgform_index")
public class OnlCgformIndex implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(
            type = IdType.UUID
    )
    @ApiModelProperty("主键")
    private String id;

    @ApiModelProperty("主表id")
    private String cgformHeadId;

    @ApiModelProperty("索引名称")
    private String indexName;

    @ApiModelProperty("索引栏位")
    private String indexField;

    @ApiModelProperty("是否同步数据库 N未同步 Y已同步")
    private String isDbSynch;

    @ApiModelProperty("是否删除 0未删除 1删除")
    private Integer delFlag;

    @ApiModelProperty("索引类型")
    private String indexType;

    @ApiModelProperty("创建人")
    private String createBy;

    @JsonFormat(
            timezone = "GMT+8",
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @ApiModelProperty("创建日期")
    private Date createTime;

    @ApiModelProperty("更新人")
    private String updateBy;

    @JsonFormat(
            timezone = "GMT+8",
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @ApiModelProperty("更新日期")
    private Date updateTime;

}
