package com.wust.modules.online.cgform.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wanheng
 */

public enum CgformEnum {

    /**
     * 经典风格
     * 经典风格
     * ERP风格
     * 内嵌子表风格
     * 树形列表
     */
    ONE(1, "one", "/code/code-template-online", "default.one", "经典风格"),
    MANY(2, "many", "/code/code-template-online", "default.onetomany", "经典风格"),
    ERP(2, "erp", "/code/code-template-online", "erp.onetomany", "ERP风格"),
    INNER_TABLE(2, "innerTable", "/code/code-template-online", "inner-table.onetomany", "内嵌子表风格"),
    TREE(3, "tree", "/code/code-template-online", "default.tree", "树形列表");

    int type;
    String code;
    String templatePath;
    String stylePath;
    String note;

    CgformEnum(int type, String code, String templatePath, String stylePath, String note) {
        this.type = type;
        this.code = code;
        this.templatePath = templatePath;
        this.stylePath = stylePath;
        this.note = note;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTemplatePath() {
        return this.templatePath;
    }


    public String getStylePath() {
        return this.stylePath;
    }

    public static CgformEnum getCgformEnumByConfig(String code) {
        CgformEnum[] values = values();
        for (CgformEnum cgformEnum : values) {
            if (cgformEnum.code.equals(code)) {
                return cgformEnum;
            }
        }

        return null;
    }

    public static List<Map<String, Object>> getJspModelList(int type) {
        ArrayList arrayList = new ArrayList();
        CgformEnum[] values = values();
        for (CgformEnum cgformEnum : values) {
            if (cgformEnum.type == type) {
                HashMap hashMap = new HashMap();
                hashMap.put("code", cgformEnum.code);
                hashMap.put("note", cgformEnum.note);
                arrayList.add(hashMap);
            }
        }

        return arrayList;
    }
}
