package com.wust.modules.online.cgform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wust.modules.online.cgform.entity.OnlCgformButton;

/**
 * @author wanheng
 */
public interface OnlCgformButtonMapper extends BaseMapper<OnlCgformButton> {
}
