package com.wust.modules.online.cgform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wust.modules.online.cgform.entity.OnlCgformEnhanceJava;

/**
 * @author wanheng
 */
public interface OnlCgformEnhanceJavaMapper extends BaseMapper<OnlCgformEnhanceJava> {
}
