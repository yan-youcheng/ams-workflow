package com.wust.modules.online.cgform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wust.modules.online.cgform.entity.OnlCgformEnhanceJs;

/**
 * @author wanheng
 */
public interface OnlCgformEnhanceJsMapper extends BaseMapper<OnlCgformEnhanceJs> {
}
