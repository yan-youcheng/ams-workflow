package com.wust.modules.online.cgform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wust.modules.online.cgform.entity.OnlCgformEnhanceSql;

/**
 * @author wanheng
 */
public interface OnlCgformEnhanceSqlMapper extends BaseMapper<OnlCgformEnhanceSql> {
}
