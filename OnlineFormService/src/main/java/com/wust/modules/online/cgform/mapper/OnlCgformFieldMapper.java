package com.wust.modules.online.cgform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;
import java.util.Map;

import com.wust.modules.online.cgform.entity.OnlCgformField;
import org.apache.ibatis.annotations.Param;
import com.wust.modules.online.cgform.model.TreeModel;
import com.wust.modules.online.cgform.model.LinkDown;

/**
 * @author wanheng
 */
public interface OnlCgformFieldMapper extends BaseMapper<OnlCgformField> {

  /**
   * 查询
   * @param sqlStr
   * @return
   */
  List<Map<String, Object>> queryListBySql(@Param("sqlStr") String sqlStr);

  /**
   * 查询数量
   * @param sqlStr
   * @return
   */
  Integer queryCountBySql(@Param("sqlStr") String sqlStr);

  /**
   * 批量删除
   * @param sqlStr
   */
  void deleteAutoList(@Param("sqlStr") String sqlStr);

  /**
   * 保存表单数据
   * @param sqlStr
   */
  void saveFormData(@Param("sqlStr") String sqlStr);

  /**
   * 编辑表单数据
   * @param sqlStr
   */
  void editFormData(@Param("sqlStr") String sqlStr);

  /**
   * 查询表单数据
   * @param sqlStr
   * @return
   */
  Map<String, Object> queryFormData(@Param("sqlStr") String sqlStr);

  /**
   * 查询数据
   * @param sqlStr
   * @return
   */
  List<Map<String, Object>> queryListData(@Param("sqlStr") String sqlStr);

  /**
   * 分页查询
   * @param mapPage
   * @param sqlStr
   * @return
   */
  IPage<Map<String, Object>> selectPageBySql(Page<Map<String, Object>> mapPage, @Param("sqlStr") String sqlStr);

  /**
   * 插入数据
   * @param map
   */
  void executeInsertSql(Map<String, Object> map);

  /**
   * 更新数据
   * @param map
   */
  void executeUpdateSql(Map<String, Object> map);

  /**
   *
   * 通过linkDown查询数据
   * @param query
   * @return
   */
  List<TreeModel> queryDataListByLinkDown(@Param("query") LinkDown query);
}
