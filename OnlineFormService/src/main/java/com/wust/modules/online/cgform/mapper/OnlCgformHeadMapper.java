package com.wust.modules.online.cgform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;

import com.wust.modules.online.cgform.entity.OnlCgformHead;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author wanheng
 */
public interface OnlCgformHeadMapper extends BaseMapper<OnlCgformHead> {

  /**
   * 执行DD语句
   * @param sqlStr
   */
  void executeDDL(@Param("sqlStr") String sqlStr);

  /**
   * 查询列表
   * @param sqlStr
   * @return
   */
  List<Map<String, Object>> queryList(@Param("sqlStr") String sqlStr);

  /**
   * 查询所有表单
   * @return
   */
  List<String> queryOnlineTables();

  /**
   * 通过tableName和id查询
   * @param tbname
   * @param dataId
   * @return
   */
  Map<String, Object> queryOneByTableNameAndId(@Param("tbname") String tbname, @Param("dataId") String dataId);

  /**
   * 删除
   * @param sqlStr
   */
  void deleteOne(@Param("sqlStr") String sqlStr);

  /**
   * 查询最大copyVersion
   * @param physicId
   * @return
   */
  @Select({"select max(copy_version) from onl_cgform_head where physic_id = #{physicId}"})
  Integer getMaxCopyVersion(@Param("physicId") String physicId);

  /**
   * 查询physicId
   * @return
   */
  @Select({"select physic_id from onl_cgform_head GROUP BY physic_id"})
  List<String> queryCopyPhysicId();

  /**
   * 根据code查询id
   * @param code
   * @return
   */
  String queryCategoryIdByCode(@Param("code") String code);

  /**
   * 查询子节点数量
   * @param tableName
   * @param pidField
   * @param pidValue
   * @return
   */
  @Select({"select count(*) from ${tableName} where ${pidField} = #{pidValue}"})
  Integer queryChildNode(@Param("tableName") String tableName, @Param("pidField") String pidField, @Param("pidValue") String pidValue);
}
