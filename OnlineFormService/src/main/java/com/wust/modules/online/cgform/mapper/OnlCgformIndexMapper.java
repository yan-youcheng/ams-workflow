package com.wust.modules.online.cgform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wust.modules.online.cgform.entity.OnlCgformIndex;
import org.apache.ibatis.annotations.Param;

/**
 * @author wanheng
 */
public interface OnlCgformIndexMapper extends BaseMapper<OnlCgformIndex> {

  /**
   * 获取索引数量
   * @param sqlStr
   * @return
   */
  int queryIndexCount(@Param("sqlStr") String sqlStr);
}
