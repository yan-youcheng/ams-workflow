package com.wust.modules.online.cgform.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author wanheng
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor
public class ColumnModel {

    private String title;

    private String dataIndex;

    private String align;

    private String customRender;

    private CustomRenderModel scopedSlots;

    private String hrefSlotName;

    private boolean sorter = false;

    public ColumnModel(String title, String dataIndex) {
        this.align = "center";
        this.title = title;
        this.dataIndex = dataIndex;
    }
}
