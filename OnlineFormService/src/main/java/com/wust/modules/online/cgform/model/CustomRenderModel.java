package com.wust.modules.online.cgform.model;

import lombok.*;

/**
 * @author wanheng
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode
@ToString
public class CustomRenderModel {

    private String customRender;

}
