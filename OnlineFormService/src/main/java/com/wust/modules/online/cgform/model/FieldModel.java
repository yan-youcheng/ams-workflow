package com.wust.modules.online.cgform.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wanheng
 */
@NoArgsConstructor
@Data
public class FieldModel {

    private String fieldName;
    private String tableName;
    private String codeField;
    private String textField;
    private String pidField;
    private String pidValue;
    private String hsaChildField;

}
