package com.wust.modules.online.cgform.model;

import lombok.*;

/**
 * @author wanheng
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class HrefSlots {

    private String slotName;

    private String href;

}