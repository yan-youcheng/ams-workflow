package com.wust.modules.online.cgform.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author wanheng
 */
@NoArgsConstructor
@Data
@EqualsAndHashCode
@ToString
public class KeyModel {

    private String field;
    private String table;
    private String key;


    public KeyModel(String field, String key) {
        this.field = field;
        this.key = key;
    }

}
