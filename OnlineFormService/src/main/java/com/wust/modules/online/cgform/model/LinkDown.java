package com.wust.modules.online.cgform.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author wanheng
 */
@NoArgsConstructor
@Data
@EqualsAndHashCode
@ToString
public class LinkDown {

    private String table;
    private String txt;
    private String key;
    private String linkField;
    private String idField;
    private String pidField;
    private String pidValue;
    private String condition;
}
