package com.wust.modules.online.cgform.model;

import java.util.List;

import com.wust.modules.online.cgform.entity.OnlCgformField;
import com.wust.modules.online.cgform.entity.OnlCgformHead;
import com.wust.modules.online.cgform.entity.OnlCgformIndex;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author wanheng
 */
@NoArgsConstructor
@Data
@ToString
public class OnlCgformHeadModel {

    private OnlCgformHead head;
    private List<OnlCgformField> fields;
    private List<String> deleteFieldIds;
    private List<OnlCgformIndex> indexs;
    private List<String> deleteIndexIds;

}
