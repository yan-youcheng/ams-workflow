package com.wust.modules.online.cgform.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wanheng
 */
@NoArgsConstructor
@Data
public class OnlGenerateModel implements Serializable {

    private static final long b = 684098897071177558L;
    private String code;
    private String projectPath;
    private String packageStyle;
    private String ftlDescription;
    private String jformType;
    private String tableName;
    private String entityPackage;
    private String entityName;
    private String jspMode;
    List<OnlGenerateModel> subList = new ArrayList();

}
