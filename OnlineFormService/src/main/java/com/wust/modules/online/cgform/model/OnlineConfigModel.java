package com.wust.modules.online.cgform.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wust.common.system.vo.DictModel;
import com.wust.modules.online.cgform.entity.OnlCgformButton;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author wanheng
 */
@NoArgsConstructor
@Data
@EqualsAndHashCode
@ToString
public class OnlineConfigModel {

    private String code;
    private String formTemplate;
    private String description;
    private String currentTableName;
    private Integer tableType;
    private String paginationFlag;
    private String checkboxFlag;
    private Integer scrollFlag;
    private List<ColumnModel> columns;
    private List<String> hideColumns;
    private Map<String, List<DictModel>> dictOptions = new HashMap();
    private List<OnlCgformButton> cgButtonList;
    List<HrefSlots> fieldHrefSlots;
    private String enhanceJs;
    private List<KeyModel> foreignKeys;
    private String pidField;
    private String hasChildrenField;
    private String textField;

}
