package com.wust.modules.online.cgform.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author wanheng
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class TreeModel {

    private String label;
    private String store;
    private String id;
    private String pid;

}
