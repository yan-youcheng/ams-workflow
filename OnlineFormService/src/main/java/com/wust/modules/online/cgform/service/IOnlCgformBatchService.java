package com.wust.modules.online.cgform.service;

import java.util.List;
import java.util.Map;

import com.wust.modules.online.cgform.entity.OnlCgformField;
import com.wust.modules.online.cgform.entity.OnlCgformHead;

/**
 * @author wanheng
 */
public interface IOnlCgformBatchService {

  /**
   * 批量保存数据
   * @param head
   * @param fieldList
   * @param dataList
   */
  void saveBatchOnlineTable(OnlCgformHead head, List<OnlCgformField> fieldList, List<Map<String, Object>> dataList);

}
