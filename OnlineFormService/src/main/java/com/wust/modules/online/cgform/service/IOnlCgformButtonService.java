package com.wust.modules.online.cgform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wust.modules.online.cgform.entity.OnlCgformButton;

/**
 * @author wanheng
 */
public interface IOnlCgformButtonService extends IService<OnlCgformButton> {
}
