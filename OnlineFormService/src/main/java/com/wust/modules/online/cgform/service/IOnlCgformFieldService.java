package com.wust.modules.online.cgform.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wust.modules.online.cgform.entity.OnlCgformField;
import com.wust.modules.online.cgform.entity.OnlCgformHead;
import com.wust.modules.online.cgform.model.LinkDown;
import com.wust.modules.online.cgform.model.TreeModel;

import java.util.List;
import java.util.Map;

/**
 * @author wanheng
 */
public interface IOnlCgformFieldService extends IService<OnlCgformField> {
    /**
     * 查询表单的数据
     * @param tableName
     * @param headId
     * @param params
     * @param needList
     * @return
     */
    Map<String, Object> queryAutolistPage(String tableName, String headId, Map<String, Object> params, List<String> needList);

    /**
     * 删除表单
     * @param head
     * @param ids
     */
    void deleteAutoListMainAndSub(OnlCgformHead head, String ids);

    /**
     * 删除表单
     * @param tableName
     * @param ids
     */
    void deleteAutoListById(String tableName, String ids);

    /**
     * 删除表单
     * @param tableName
     * @param linkField
     * @param linkValue
     */
    void deleteAutoList(String tableName, String linkField, String linkValue);

    /**
     * 保存表单的数据
     * @param code
     * @param tableName
     * @param json
     * @param isCrazy
     */
    void saveFormData(String code, String tableName, JSONObject json, boolean isCrazy);

    /**
     * 保存树形
     * @param code
     * @param tableName
     * @param json
     * @param hasChildField
     * @param pidField
     */
    void saveTreeFormData(String code, String tableName, JSONObject json, String hasChildField, String pidField);

    /**
     * 保存表单的数据
     * @param fieldList
     * @param tableName
     * @param json
     */
    void saveFormData(List<OnlCgformField> fieldList, String tableName, JSONObject json);

    /**
     * 获取表单的字段
     * @param code
     * @param isForm
     * @return
     */
    List<OnlCgformField> queryFormFields(String code, boolean isForm);

    /**
     * 通过表名获取字段
     * @param tableName
     * @return
     */
    List<OnlCgformField> queryFormFieldsByTableName(String tableName);

    /**
     * 编辑树形
     * @param code
     * @param tableName
     * @param json
     * @param hasChildField
     * @param pidField
     */
    void editTreeFormData(String code, String tableName, JSONObject json, String hasChildField, String pidField);

    /**
     * 编辑表单的数据
     * @param code
     * @param tableName
     * @param json
     * @param isCrazy
     */
    void editFormData(String code, String tableName, JSONObject json, boolean isCrazy);

    /**
     * 获取表单的数据
     * @param fieldList
     * @param tableName
     * @param id
     * @return
     */
    Map<String, Object> queryFormData(List<OnlCgformField> fieldList, String tableName, String id);

    /**
     * 获取表单的数据
     * @param fieldList
     * @param tableName
     * @param linkField
     * @param value
     * @return
     */
    List<Map<String, Object>> querySubFormData(List<OnlCgformField> fieldList, String tableName, String linkField, String value);

    /**
     * 获取查询信息
     * @param code
     * @return
     */
    List<Map<String, String>> getAutoListQueryInfo(String code);

    /**
     * 查询隐藏字段
     * @param tableName
     * @return
     */
    List<String> selectOnlineHideColumns(String tableName);

    /**
     * 查询可用字段
     * @param cgFormId
     * @param tableName
     * @param taskId
     * @param isList
     * @return
     */
    List<OnlCgformField> queryAvailableFields(String cgFormId, String tableName, String taskId, boolean isList);

    /**
     * 查询被禁用字段
     * @param tableName
     * @return
     */
    List<String> queryDisabledFields(String tableName);

    /**
     * 查询可用字段
     * @param tableName
     * @param isList
     * @param onlCgformFieldList
     * @param needList
     * @return
     */
    List<OnlCgformField> queryAvailableFields(String tableName, boolean isList, List<OnlCgformField> onlCgformFieldList, List<String> needList);

    /**
     * 执行插入语句
     * @param params
     */
    void executeInsertSql(Map<String, Object> params);

    /**
     * 通过linkDown获取树形数据
     * @param linkDown
     * @return
     */
    List<TreeModel> queryDataListByLinkDown(LinkDown linkDown);

    /**
     * 更新无孩子节点
     * @param tableName
     * @param filed
     * @param id
     */
    void updateTreeNodeNoChild(String tableName, String filed, String id);

}
