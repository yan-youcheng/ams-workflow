package com.wust.modules.online.cgform.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wust.common.api.vo.Result;
import com.wust.modules.online.cgform.model.OnlCgformHeadModel;
import com.wust.modules.online.cgform.model.OnlGenerateModel;
import com.wust.modules.online.config.exception.BusinessException;
import com.wust.modules.online.config.exception.DBException;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import com.wust.modules.online.cgform.entity.OnlCgformButton;
import com.wust.modules.online.cgform.entity.OnlCgformEnhanceJava;
import com.wust.modules.online.cgform.entity.OnlCgformEnhanceJs;
import com.wust.modules.online.cgform.entity.OnlCgformEnhanceSql;
import com.wust.modules.online.cgform.entity.OnlCgformHead;

/**
 * @author wanheng
 */
public interface IOnlCgformHeadService extends IService<OnlCgformHead> {

  /**
   * 添加表单
   * @param model
   * @return
   */
  Result<?> addAll(OnlCgformHeadModel model);

  /**
   * 编辑表单
   * @param model
   * @return
   */
  Result<?> editAll(OnlCgformHeadModel model);

  /**
   * 同步数据库
   * @param code
   * @param synMethod
   * @throws HibernateException
   * @throws IOException
   * @throws TemplateException
   * @throws SQLException
   * @throws DBException
   */
  void doDbSynch(String code, String synMethod) throws HibernateException, IOException, TemplateException, SQLException, DBException;

  /**
   * 删除表单
   * @param id
   * @throws DBException
   * @throws SQLException
   */
  void deleteRecordAndTable(String id) throws DBException, SQLException;

  /**
   * 删除记录
   * @param id
   * @throws DBException
   * @throws SQLException
   */
  void deleteRecord(String id) throws DBException, SQLException;

  /**
   * 获取数据
   * @param sql
   * @return
   */
  List<Map<String, Object>> queryListData(String sql);

  /**
   * 查询Js增强
   * @param code
   * @param type
   * @return
   */
  OnlCgformEnhanceJs queryEnhance(String code, String type);

  /**
   * 保存Js增强
   * @param onlCgformEnhanceJs
   */
  void saveEnhance(OnlCgformEnhanceJs onlCgformEnhanceJs);

  /**
   * 编辑Js增强
   * @param onlCgformEnhanceJs
   */
  void editEnhance(OnlCgformEnhanceJs onlCgformEnhanceJs);

  /**
   * 查询Sql增强
   * @param formId
   * @param buttonCode
   * @return
   */
  OnlCgformEnhanceSql queryEnhanceSql(String formId, String buttonCode);

  /**
   * 保存Sql增强
   * @param onlCgformEnhanceSql
   */
  void saveEnhance(OnlCgformEnhanceSql onlCgformEnhanceSql);

  /**
   * 编辑Sql增强
   * @param onlCgformEnhanceSql
   */
  void editEnhance(OnlCgformEnhanceSql onlCgformEnhanceSql);

  /**
   * 获取Java增强
   * @param onlCgformEnhanceJava
   * @return
   */
  OnlCgformEnhanceJava queryEnhanceJava(OnlCgformEnhanceJava onlCgformEnhanceJava);

  /**
   * 校验唯一性
   * @param onlCgformEnhanceJava
   * @return
   */
  boolean checkOnlyEnhance(OnlCgformEnhanceJava onlCgformEnhanceJava);

  /**
   * 保存Java增强
   * @param onlCgformEnhanceJava
   */
  void saveEnhance(OnlCgformEnhanceJava onlCgformEnhanceJava);

  /**
   * 编辑Java增强
   * @param onlCgformEnhanceJava
   */
  void editEnhance(OnlCgformEnhanceJava onlCgformEnhanceJava);

  /**
   * 获取按钮
   * @param code
   * @param isListButton
   * @return
   */
  List<OnlCgformButton> queryButtonList(String code, boolean isListButton);

  /**
   * 获取按钮
   * @param code
   * @return
   */
  List<OnlCgformButton> queryButtonList(String code);

  /**
   * 获取Online表单
   * @return
   */
  List<String> queryOnlinetables();

  /**
   * 保存Online表单
   * @param tableName
   */
  void saveDbTable2Online(String tableName);

  /**
   * 查询表单项
   * @param onlCgformHead
   * @return
   */
  JSONObject queryFormItem(OnlCgformHead onlCgformHead);

  /**
   * 多数据存储
   * @param code
   * @param json
   * @param xAccessToken
   * @throws DBException
   * @throws BusinessException
   */
  void saveManyFormData(String code, JSONObject json, String xAccessToken) throws DBException, BusinessException;

  /**
   * 查询多数据
   * @param code
   * @param id
   * @return
   * @throws DBException
   */
  Map<String, Object> queryManyFormData(String code, String id) throws DBException;

  /**
   * 查询子表数据
   * @param table
   * @param mainId
   * @return
   * @throws DBException
   */
  List<Map<String, Object>> queryManySubFormData(String table, String mainId) throws DBException;

  /**
   * 查询子表数据
   * @param table
   * @param mainId
   * @return
   * @throws DBException
   */
  Map<String, Object> querySubFormData(String table, String mainId) throws DBException;

  /**
   * 编辑多数据
   * @param code
   * @param json
   * @throws DBException
   * @throws BusinessException
   */
  void editManyFormData(String code, JSONObject json) throws DBException, BusinessException;

  /**
   * 执行Java增强
   * @param buttonCode
   * @param eventType
   * @param head
   * @param json
   * @return
   * @throws BusinessException
   */
  int executeEnhanceJava(String buttonCode, String eventType, OnlCgformHead head, JSONObject json) throws BusinessException;

  /**
   * 导出增强
   * @param head
   * @param dataList
   * @throws BusinessException
   */
  void executeEnhanceExport(OnlCgformHead head, List<Map<String, Object>> dataList) throws BusinessException;

  /**
   * 多增强
   * @param head
   * @param buttonCode
   * @param dataList
   * @throws BusinessException
   */
  void executeEnhanceList(OnlCgformHead head, String buttonCode, List<Map<String, Object>> dataList) throws BusinessException;

  /**
   * 执行Sql增强
   * @param buttonCode
   * @param formId
   * @param json
   */
  void executeEnhanceSql(String buttonCode, String formId, JSONObject json);

  /**
   * 执行自定义按钮
   * @param buttonCode
   * @param formId
   * @param dataId
   * @throws BusinessException
   */
  void executeCustomerButton(String buttonCode, String formId, String dataId) throws BusinessException;

  /**
   * 执行Js增强
   * @param formId
   * @param cgJsType
   * @return
   */
  OnlCgformEnhanceJs queryEnhanceJs(String formId, String cgJsType);

  /**
   * 查询表单信息
   * @param formId
   * @param dataId
   * @throws BusinessException
   */
  void deleteOneTableInfo(String formId, String dataId) throws BusinessException;

  /**
   * 代码生成
   * @param model
   * @return
   * @throws Exception
   */
  List<String> generateCode(OnlGenerateModel model) throws Exception;

  /**
   * 一对多代码生成
   * @param model
   * @return
   * @throws Exception
   */
  List<String> generateOneToMany(OnlGenerateModel model) throws Exception;

  /**
   * 懒加载添加
   * @param tableName
   * @param json
   * @throws DBException
   * @throws UnsupportedEncodingException
   */
  void addCrazyFormData(String tableName, JSONObject json) throws DBException, UnsupportedEncodingException;

  /**
   * 懒加载编辑
   * @param tableName
   * @param json
   * @throws DBException
   * @throws UnsupportedEncodingException
   */
  void editCrazyFormData(String tableName, JSONObject json) throws DBException, UnsupportedEncodingException;

  /**
   * 获取最大CopyVersion
   * @param physicId
   * @return
   */
  Integer getMaxCopyVersion(String physicId);

  /**
   * 复制表单配置
   * @param physicTable
   * @throws Exception
   */
  void copyOnlineTableConfig(OnlCgformHead physicTable) throws Exception;

  /**
   * 初始化Copy状态
   * @param headList
   */
  void initCopyState(List<OnlCgformHead> headList);

  /**
   * 批量删除
   * @param ids
   * @param flag
   */
  void deleteBatch(String ids, String flag);

  /**
   * 更新父节点
   * @param onlCgformHead
   * @param dataId
   */
  void updateParentNode(OnlCgformHead onlCgformHead, String dataId);
}
