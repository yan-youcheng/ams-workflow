package com.wust.modules.online.cgform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

import com.wust.modules.online.cgform.entity.OnlCgformIndex;

/**
 * @author wanheng
 */
public interface IOnlCgformIndexService extends IService<OnlCgformIndex> {

  /**
   * 创建索引
   * @param code
   * @param databaseType
   * @param tableName
   */
  void createIndex(String code, String databaseType, String tableName);

  /**
   * 是否存在所有
   * @param sql
   * @return
   */
  boolean isExistIndex(String sql);

  /**
   * 通过表单id删除索引
   * @param cgformId
   * @return
   */
  List<OnlCgformIndex> getCgformIndexsByCgformId(String cgformId);
}
