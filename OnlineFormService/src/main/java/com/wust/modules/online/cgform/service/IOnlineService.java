
package com.wust.modules.online.cgform.service;

import com.alibaba.fastjson.JSONObject;
import com.wust.modules.online.cgform.entity.OnlCgformEnhanceJs;
import com.wust.modules.online.cgform.entity.OnlCgformHead;
import com.wust.modules.online.cgform.model.OnlineConfigModel;

/**
 * @author wanheng
 */
public interface IOnlineService {

    /**
     * 查询表单配置
     * @param onlCgformHead
     * @return
     */
    OnlineConfigModel queryOnlineConfig(OnlCgformHead onlCgformHead);

    /**
     * 查询表单对象
     * @param onlCgformHead
     * @param onlCgformEnhanceJs
     * @return
     */
    JSONObject queryOnlineFormObj(OnlCgformHead onlCgformHead, OnlCgformEnhanceJs onlCgformEnhanceJs);

    /**
     * 查询表单对象
     * @param onlCgformHead
     * @return
     */
    JSONObject queryOnlineFormObj(OnlCgformHead onlCgformHead);
}
