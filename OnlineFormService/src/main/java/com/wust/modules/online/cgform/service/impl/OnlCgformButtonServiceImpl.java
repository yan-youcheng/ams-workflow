package com.wust.modules.online.cgform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wust.modules.online.cgform.entity.OnlCgformButton;
import com.wust.modules.online.cgform.mapper.OnlCgformButtonMapper;
import com.wust.modules.online.cgform.service.IOnlCgformButtonService;
import org.springframework.stereotype.Service;

/**
 * @author wanheng
 */
@Service
public class OnlCgformButtonServiceImpl extends ServiceImpl<OnlCgformButtonMapper, OnlCgformButton> implements IOnlCgformButtonService {

}
