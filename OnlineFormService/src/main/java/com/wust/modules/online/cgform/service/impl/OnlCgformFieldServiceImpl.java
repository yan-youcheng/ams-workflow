package com.wust.modules.online.cgform.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.wust.common.util.oConvertUtils;
import com.wust.modules.online.cgform.entity.OnlCgformField;
import com.wust.modules.online.cgform.entity.OnlCgformHead;
import com.wust.modules.online.cgform.mapper.OnlCgformFieldMapper;
import com.wust.modules.online.cgform.mapper.OnlCgformHeadMapper;
import com.wust.modules.online.cgform.model.LinkDown;
import com.wust.modules.online.cgform.model.TreeModel;
import com.wust.modules.online.cgform.service.IOnlCgformFieldService;
import com.wust.modules.online.cgform.util.SqlSymbolUtil;
import com.wust.modules.system.entity.SysUser;
import com.wust.modules.system.service.ISysService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author wanheng
 */
@Slf4j
@Service
public class OnlCgformFieldServiceImpl extends ServiceImpl<OnlCgformFieldMapper, OnlCgformField> implements IOnlCgformFieldService {

    @Resource
    private OnlCgformFieldMapper onlCgformFieldMapper;

    @Resource
    private OnlCgformHeadMapper cgformHeadMapper;

    @Resource
    private ISysService sysService;

    @Override
    public Map<String, Object> queryAutolistPage(String tbname, String headId, Map<String, Object> params, List<String> needList) {
        HashMap hashMap = new HashMap(16);
        LambdaQueryWrapper<OnlCgformField> onlCgformFieldLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, headId);
        onlCgformFieldLambdaQueryWrapper.orderByAsc(OnlCgformField::getOrderNum);
        List onlCgformFields = this.list(onlCgformFieldLambdaQueryWrapper);
        List onlCgformFields1 = this.queryAvailableFields(tbname, true, onlCgformFields, needList);
        StringBuffer stringBuffer = new StringBuffer();
        SqlSymbolUtil.getSelect(tbname, onlCgformFields1, stringBuffer);
        String sql = SqlSymbolUtil.getByDataType(onlCgformFields, params, needList);
        String sql1 = SqlSymbolUtil.getByParams(params);
        stringBuffer.append(" where 1=1  ").append(sql).append(sql1);
        Object column = params.get("column");
        if (column != null) {
            String columnStr = column.toString();
            String orderStr = params.get("order").toString();
            if (this.equalsField(columnStr, onlCgformFields)) {
                stringBuffer.append(" ORDER BY ").append(oConvertUtils.camelToUnderline(columnStr));
                if ("asc".equals(orderStr)) {
                    stringBuffer.append(" asc");
                } else {
                    stringBuffer.append(" desc");
                }
            }
        }

        int pageSzie = params.get("pageSize") == null ? 10 : Integer.parseInt(params.get("pageSize").toString());
        if (pageSzie == -521) {
            List onlCgformFieldList = this.onlCgformFieldMapper.queryListBySql(stringBuffer.toString());
            log.info("---Online查询sql 不分页 :>>" + stringBuffer.toString());
            if (onlCgformFieldList != null && onlCgformFieldList.size() != 0) {
                hashMap.put("total", onlCgformFieldList.size());
                hashMap.put("fieldList", onlCgformFields1);
                hashMap.put("records", SqlSymbolUtil.getMapList(onlCgformFieldList));
            } else {
                hashMap.put("total", 0);
                hashMap.put("fieldList", onlCgformFields1);
            }
        } else {
            int pageNo = params.get("pageNo") == null ? 1 : Integer.parseInt(params.get("pageNo").toString());
            Page page = new Page(pageNo, pageSzie);
            log.info("---Online查询sql:>>" + stringBuffer.toString());
            IPage iPage = this.onlCgformFieldMapper.selectPageBySql(page, stringBuffer.toString());
            hashMap.put("total", iPage.getTotal());
            hashMap.put("records", SqlSymbolUtil.getMapList(iPage.getRecords()));
        }

        return hashMap;
    }

    @Override
    public void saveFormData(String code, String tbname, JSONObject json, boolean isCrazy) {
        LambdaQueryWrapper<OnlCgformField> onlCgformFieldLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, code);
        List onlCgformFields = this.list(onlCgformFieldLambdaQueryWrapper);
        if (isCrazy) {
            this.baseMapper.executeInsertSql(SqlSymbolUtil.getFMakingInsertSql(tbname, onlCgformFields, json));
        } else {
            this.baseMapper.executeInsertSql(SqlSymbolUtil.getInsertSql(tbname, onlCgformFields, json));
        }

    }

    @Override
    public void saveTreeFormData(String code, String tbname, JSONObject json, String hasChildField, String pidField) {
        LambdaQueryWrapper<OnlCgformField> onlCgformFieldLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, code);
        List onlCgformFieldList = this.list(onlCgformFieldLambdaQueryWrapper);
        Iterator iterator = onlCgformFieldList.iterator();

        while (true) {
            while (iterator.hasNext()) {
                OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                if (hasChildField.equals(onlCgformField.getDbFieldName()) && onlCgformField.getIsShowForm() != 1) {
                    onlCgformField.setIsShowForm(1);
                    json.put(hasChildField, "0");
                } else if (pidField.equals(onlCgformField.getDbFieldName()) && oConvertUtils.isEmpty(json.get(pidField))) {
                    onlCgformField.setIsShowForm(1);
                    json.put(pidField, "0");
                }
            }

            Map map = SqlSymbolUtil.getInsertSql(tbname, onlCgformFieldList, json);
            this.baseMapper.executeInsertSql(map);
            if (!"0".equals(json.getString(pidField))) {
                this.baseMapper.editFormData("update " + tbname + " set " + hasChildField + " = '1' where id = '" + json.getString(pidField) + "'");
            }

            return;
        }
    }

    @Override
    public void saveFormData(List<OnlCgformField> fieldList, String tbname, JSONObject json) {
        Map map = SqlSymbolUtil.getInsertSql(tbname, fieldList, json);
        this.baseMapper.executeInsertSql(map);
    }

    @Override
    public void editFormData(String code, String tbname, JSONObject json, boolean isCrazy) {
        LambdaQueryWrapper<OnlCgformField> onlCgformFieldLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, code);
        List onlCgformFields = this.list(onlCgformFieldLambdaQueryWrapper);
        if (isCrazy) {
            this.baseMapper.executeUpdateSql(SqlSymbolUtil.formDesignerEditingSql(tbname, onlCgformFields, json));
        } else {
            this.baseMapper.executeUpdateSql(SqlSymbolUtil.getUpdateSql(tbname, onlCgformFields, json));
        }

    }

    @Override
    public void editTreeFormData(String code, String tbname, JSONObject json, String hasChildField, String pidField) {
        String tableName = SqlSymbolUtil.getSubstring(tbname);
        String sql = "select * from " + tableName + " where id = '" + json.getString("id") + "'";
        Map formData = ((OnlCgformFieldMapper) this.baseMapper).queryFormData(sql);
        Map valueType = SqlSymbolUtil.getValueType(formData);
        String field = valueType.get(pidField).toString();
        LambdaQueryWrapper<OnlCgformField> onlCgformFieldLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, code);
        List onlCgformFieldList = this.list(onlCgformFieldLambdaQueryWrapper);

        for (Object o : onlCgformFieldList) {
            OnlCgformField onlCgformField = (OnlCgformField) o;
            if (pidField.equals(onlCgformField.getDbFieldName()) && oConvertUtils.isEmpty(json.get(pidField))) {
                onlCgformField.setIsShowForm(1);
                json.put(pidField, "0");
            }
        }

        Map sqlMap = SqlSymbolUtil.getUpdateSql(tbname, onlCgformFieldList, json);
        this.baseMapper.executeUpdateSql(sqlMap);
        if (!field.equals(json.getString(pidField))) {
            if (!"0".equals(field)) {
                String sql1 = "select count(*) from " + tableName + " where " + pidField + " = '" + field + "'";
                Integer count = this.baseMapper.queryCountBySql(sql1);
                if (count == null || count == 0) {
                    this.baseMapper.editFormData("update " + tableName + " set " + hasChildField + " = '0' where id = '" + field + "'");
                }
            }

            if (!"0".equals(json.getString(pidField))) {
                this.baseMapper.editFormData("update " + tableName + " set " + hasChildField + " = '1' where id = '" + json.getString(pidField) + "'");
            }
        }

    }

    @Override
    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void deleteAutoListMainAndSub(OnlCgformHead head, String ids) {
        if (head.getTableType() == 2) {
            String headId = head.getId();
            String tableName = head.getTableName();
            String tableName1 = "tableName";
            String linkField = "linkField";
            String linkValueStr = "linkValueStr";
            String mainField = "mainField";
            ArrayList arrayList = new ArrayList();
            if (oConvertUtils.isNotEmpty(head.getSubTableStr())) {
                String[] subTableStrs = head.getSubTableStr().split(",");
                int length = subTableStrs.length;

                for (int i = 0; i < length; ++i) {
                    String subTableStr = subTableStrs[i];
                    OnlCgformHead onlCgformHead = (OnlCgformHead) this.cgformHeadMapper.selectOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, subTableStr));
                    if (onlCgformHead != null) {
                        LambdaQueryWrapper<OnlCgformField> lambdaQueryWrapper = (LambdaQueryWrapper) ((LambdaQueryWrapper<OnlCgformField>) (new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, onlCgformHead.getId())).eq(OnlCgformField::getMainTable, head.getTableName());
                        List onlCgformFields = this.list(lambdaQueryWrapper);
                        if (onlCgformFields != null && onlCgformFields.size() != 0) {
                            OnlCgformField onlCgformField = (OnlCgformField) onlCgformFields.get(0);
                            HashMap hashMap = new HashMap();
                            hashMap.put(linkField, onlCgformField.getDbFieldName());
                            hashMap.put(mainField, onlCgformField.getMainField());
                            hashMap.put(tableName1, subTableStr);
                            hashMap.put(linkValueStr, "");
                            arrayList.add(hashMap);
                        }
                    }
                }

                LambdaQueryWrapper<OnlCgformField> onlCgformFieldLambdaQueryWrapper = new LambdaQueryWrapper();
                onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, headId);
                List onlCgformFields = this.list(onlCgformFieldLambdaQueryWrapper);
                String[] idList = ids.split(",");
                int length1 = idList.length;
                int i = 0;

                label52:
                while (true) {
                    if (i >= length1) {
                        Iterator iterator = arrayList.iterator();

                        while (true) {
                            if (!iterator.hasNext()) {
                                break label52;
                            }

                            Map map = (Map) iterator.next();
                            this.deleteAutoList((String) map.get(tableName1), (String) map.get(linkField), (String) map.get(linkValueStr));
                        }
                    }

                    String id = idList[i];
                    String sql = SqlSymbolUtil.getSelectSql(tableName, onlCgformFields, id);
                    Map formData = this.onlCgformFieldMapper.queryFormData(sql);
                    new ArrayList();

                    for (Object o : arrayList) {
                        Map map = (Map) o;
                        Object object = formData.get(((String) map.get(mainField)).toLowerCase());
                        if (object == null) {
                            object = formData.get(((String) map.get(mainField)).toUpperCase());
                        }

                        if (object != null) {
                            String linkValue = map.get(linkValueStr) + String.valueOf(object) + ",";
                            map.put(linkValueStr, linkValue);
                        }
                    }

                    ++i;
                }
            }

            this.deleteAutoListById(head.getTableName(), ids);
        }

    }

    @Override
    public void deleteAutoListById(String tbname, String ids) {
        this.deleteAutoList(tbname, "id", ids);
    }

    @Override
    public void deleteAutoList(String tbname, String linkField, String linkValue) {
        if (linkValue != null && !"".equals(linkValue)) {
            String[] linkValues = linkValue.split(",");
            StringBuilder stringBuilder = new StringBuilder();
            String[] linkValues1 = linkValues;
            int length = linkValues.length;

            for (int i = 0; i < length; ++i) {
                String linkValue1 = linkValues1[i];
                if (linkValue1 != null && !"".equals(linkValue1)) {
                    stringBuilder.append("'" + linkValue1 + "',");
                }
            }

            String s = stringBuilder.toString();
            String sql = "DELETE FROM " + SqlSymbolUtil.getSubstring(tbname) + " where " + linkField + " in(" + s.substring(0, s.length() - 1) + ")";
            log.info("--删除sql-->" + sql);
            this.onlCgformFieldMapper.deleteAutoList(sql);
        }

    }

    @Override
    public List<Map<String, String>> getAutoListQueryInfo(String code) {
        LambdaQueryWrapper<OnlCgformField> onlCgformFieldLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, code);
        onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getIsQuery, 1);
        onlCgformFieldLambdaQueryWrapper.orderByAsc(OnlCgformField::getOrderNum);
        List list = this.list(onlCgformFieldLambdaQueryWrapper);
        ArrayList arrayList = new ArrayList();
        int i = 0;

        HashMap hashMap;
        for (Iterator iterator = list.iterator(); iterator.hasNext(); arrayList.add(hashMap)) {
            OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
            hashMap = new HashMap();
            hashMap.put("label", onlCgformField.getDbFieldTxt());
            hashMap.put("field", onlCgformField.getDbFieldName());
            hashMap.put("mode", onlCgformField.getQueryMode());
            String[] strings;
            String s;
            if ("1".equals(onlCgformField.getQueryConfigFlag())) {
                hashMap.put("config", "1");
                hashMap.put("view", onlCgformField.getQueryShowType());
                hashMap.put("defValue", onlCgformField.getQueryDefVal());
                if ("cat_tree".equals(onlCgformField.getFieldShowType())) {
                    hashMap.put("pcode", onlCgformField.getQueryDictField());
                } else if ("sel_tree".equals(onlCgformField.getFieldShowType())) {
                    strings = onlCgformField.getQueryDictText().split(",");
                    s = onlCgformField.getQueryDictTable() + "," + strings[2] + "," + strings[0];
                    hashMap.put("dict", s);
                    hashMap.put("pidField", strings[1]);
                    hashMap.put("hasChildField", strings[3]);
                    hashMap.put("pidValue", onlCgformField.getQueryDictField());
                } else {
                    hashMap.put("dictTable", onlCgformField.getQueryDictTable());
                    hashMap.put("dictCode", onlCgformField.getQueryDictField());
                    hashMap.put("dictText", onlCgformField.getQueryDictText());
                }
            } else {
                hashMap.put("view", onlCgformField.getFieldShowType());
                if ("cat_tree".equals(onlCgformField.getFieldShowType())) {
                    hashMap.put("pcode", onlCgformField.getDictField());
                } else if ("sel_tree".equals(onlCgformField.getFieldShowType())) {
                    strings = onlCgformField.getDictText().split(",");
                    s = onlCgformField.getDictTable() + "," + strings[2] + "," + strings[0];
                    hashMap.put("dict", s);
                    hashMap.put("pidField", strings[1]);
                    hashMap.put("hasChildField", strings[3]);
                    hashMap.put("pidValue", onlCgformField.getDictField());
                } else if ("popup".equals(onlCgformField.getFieldShowType())) {
                    hashMap.put("dictTable", onlCgformField.getDictTable());
                    hashMap.put("dictCode", onlCgformField.getDictField());
                    hashMap.put("dictText", onlCgformField.getDictText());
                }

                hashMap.put("mode", onlCgformField.getQueryMode());
            }

            ++i;
            if (i > 2) {
                hashMap.put("hidden", "1");
            }
        }

        return arrayList;
    }

    @Override
    public List<OnlCgformField> queryFormFields(String code, boolean isform) {
        LambdaQueryWrapper<OnlCgformField> onlCgformFieldLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, code);
        if (isform) {
            onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getIsShowForm, 1);
        }
        List<OnlCgformField> list = list(onlCgformFieldLambdaQueryWrapper);
        for (OnlCgformField onlCgformField : list) {
        }
        return this.list(onlCgformFieldLambdaQueryWrapper);
    }

    @Override
    public List<OnlCgformField> queryFormFieldsByTableName(String tableName) {
        OnlCgformHead onlCgformHead = this.cgformHeadMapper.selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, tableName));
        if (onlCgformHead != null) {
            LambdaQueryWrapper<OnlCgformField> onlCgformFieldLambdaQueryWrapper = new LambdaQueryWrapper();
            onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, onlCgformHead.getId());
            return this.list(onlCgformFieldLambdaQueryWrapper);
        } else {
            return null;
        }
    }

    @Override
    public Map<String, Object> queryFormData(List<OnlCgformField> fieldList, String tbname, String id) {
        String sql = SqlSymbolUtil.getSelectSql(tbname, fieldList, id);
        log.info("sql:" + sql);
        return this.onlCgformFieldMapper.queryFormData(sql);
    }

    @Override
    public List<Map<String, Object>> querySubFormData(List<OnlCgformField> fieldList, String tbname, String linkField, String value) {
        String sql = SqlSymbolUtil.getSelectSql(tbname, fieldList, linkField, value);
        return this.onlCgformFieldMapper.queryListData(sql);
    }

    @Override
    public List<String> selectOnlineHideColumns(String tableName) {
        String s = "online:" + tableName + "%";
        SysUser sysUser = sysService.getLoginUser();
        String userId = null;
        if (sysUser != null) {
            userId = sysUser.getId();
        }
        List list = sysService.selectOnlineHideColumns(userId, s);
        return this.removeEmpty(list);
    }

    @Override
    public List<OnlCgformField> queryAvailableFields(String cgFormId, String tbname, String taskId, boolean isList) {
        LambdaQueryWrapper<OnlCgformField> onlCgformFieldLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, cgFormId);
        if (isList) {
            onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getIsShowList, 1);
        } else {
            onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getIsShowForm, 1);
        }

        onlCgformFieldLambdaQueryWrapper.orderByAsc(OnlCgformField::getOrderNum);
        List onlCgformFields = this.list(onlCgformFieldLambdaQueryWrapper);
        String s = "online:" + tbname + "%";
        SysUser user = sysService.getLoginUser();
        String userId = null;
        if (user != null) {
            userId = user.getId();
        }
        ArrayList arrayList = new ArrayList();
        List list;
        if (oConvertUtils.isEmpty(taskId)) {
            list = sysService.selectOnlineHideColumns(userId, s);
            list = null;
            if (list != null && list.size() != 0 && list.get(0) != null) {
                arrayList.addAll(list);
            }
        }

        if (arrayList.size() == 0) {
            return onlCgformFields;
        } else {
            ArrayList arrayList1 = new ArrayList();

            for (int i = 0; i < onlCgformFields.size(); ++i) {
                OnlCgformField onlCgformField = (OnlCgformField) onlCgformFields.get(i);
                if (this.equals(onlCgformField.getDbFieldName(), arrayList)) {
                    arrayList1.add(onlCgformField);
                }
            }

            return arrayList1;
        }
    }

    @Override
    public List<String> queryDisabledFields(String tbname) {
        String s = "online:" + tbname + "%";
        SysUser sysUser = sysService.getLoginUser();
        String userId = null;
        if (sysUser != null) {
            userId = sysUser.getId();
        }
        List list = sysService.selectOnlineHideColumns(userId, s);
        return this.removeEmpty(list);
    }


    private List<String> removeEmpty(List<String> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null && list.size() != 0 && list.get(0) != null) {
            Iterator iterator = list.iterator();

            while (iterator.hasNext()) {
                String next = (String) iterator.next();
                if (!oConvertUtils.isEmpty(next)) {
                    String substring = next.substring(next.lastIndexOf(":") + 1);
                    if (!oConvertUtils.isEmpty(substring)) {
                        arrayList.add(substring);
                    }
                }
            }

            return arrayList;
        } else {
            return arrayList;
        }
    }

    @Override
    public List<OnlCgformField> queryAvailableFields(String tbname, boolean isList, List<OnlCgformField> onlCgformFieldList, List<String> needList) {
        ArrayList arrayList = new ArrayList();
        String s = "online:" + tbname + "%";
        SysUser user = sysService.getLoginUser();
        String userId = null;
        if (user != null) {
            userId = user.getId();
        }
        List list = sysService.selectOnlineHideColumns(userId, s);
        boolean b = true;
        if (list == null || list.size() == 0 || list.get(0) == null) {
            b = false;
        }

        Iterator iterator = onlCgformFieldList.iterator();

        while (true) {
            while (iterator.hasNext()) {
                OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                String dbFieldName = onlCgformField.getDbFieldName();
                if (needList != null && needList.contains(dbFieldName)) {
                    onlCgformField.setIsQuery(1);
                    arrayList.add(onlCgformField);
                } else {
                    if (isList) {
                        if (onlCgformField.getIsShowList() != 1) {
                            if (oConvertUtils.isNotEmpty(onlCgformField.getMainTable()) && oConvertUtils.isNotEmpty(onlCgformField.getMainField())) {
                                arrayList.add(onlCgformField);
                            }
                            continue;
                        }
                    } else if (onlCgformField.getIsShowForm() != 1) {
                        continue;
                    }

                    if (b) {
                        if (this.equals(dbFieldName, list)) {
                            arrayList.add(onlCgformField);
                        }
                    } else {
                        arrayList.add(onlCgformField);
                    }
                }
            }

            return arrayList;
        }
    }

    private boolean equals(String s, List<String> list) {
        boolean b = true;

        for (int i = 0; i < list.size(); ++i) {
            String s1 = (String) list.get(i);
            if (!oConvertUtils.isEmpty(s1)) {
                String substring = s1.substring(s1.lastIndexOf(":") + 1);
                if (!oConvertUtils.isEmpty(substring) && substring.equals(s)) {
                    b = false;
                }
            }
        }

        return b;
    }

    public boolean equalsField(String column, List<OnlCgformField> onlCgformFields) {
        boolean b = false;
        Iterator iterator = onlCgformFields.iterator();

        while (iterator.hasNext()) {
            OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
            if (oConvertUtils.camelToUnderline(column).equals(onlCgformField.getDbFieldName())) {
                b = true;
                break;
            }
        }

        return b;
    }

    @Override
    public void executeInsertSql(Map<String, Object> params) {
        this.baseMapper.executeInsertSql(params);
    }

    @Override
    public List<TreeModel> queryDataListByLinkDown(LinkDown linkDown) {
        return this.baseMapper.queryDataListByLinkDown(linkDown);
    }

    @Override
    public void updateTreeNodeNoChild(String tableName, String filed, String id) {
        Map map = SqlSymbolUtil.getUpdateSql(tableName, filed, id);
        this.baseMapper.executeUpdateSql(map);
    }

}

