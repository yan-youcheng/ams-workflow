package com.wust.modules.online.cgform.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wust.common.api.vo.Result;
import com.wust.common.constant.CommonConstant;
import com.wust.common.util.*;
import com.wust.modules.codegenerate.database.DbReadTableUtil;
import com.wust.modules.codegenerate.generate.impl.CodeGenerateOne;
import com.wust.modules.codegenerate.generate.impl.CodeGenerateOneToMany;
import com.wust.modules.codegenerate.generate.pojo.ColumnVo;
import com.wust.modules.codegenerate.generate.pojo.TableVo;
import com.wust.modules.codegenerate.generate.pojo.onetomany.MainTableVo;
import com.wust.modules.codegenerate.generate.pojo.onetomany.SubTableVo;
import com.wust.modules.online.cgform.enhance.CgformEnhanceJavaInter;
import com.wust.modules.online.cgform.enhance.CgformEnhanceJavaListInter;
import com.wust.modules.online.cgform.enums.CgformEnum;
import com.wust.modules.online.cgform.model.FieldModel;
import com.wust.modules.online.cgform.model.OnlCgformHeadModel;
import com.wust.modules.online.cgform.model.OnlGenerateModel;
import com.wust.modules.online.cgform.service.IOnlCgformFieldService;
import com.wust.modules.online.cgform.util.SqlSymbolUtil;
import com.wust.modules.online.config.config.DataBaseConfig;
import com.wust.modules.online.config.config.TableConfig;
import com.wust.modules.online.config.exception.BusinessException;
import com.wust.modules.online.config.exception.DBException;
import com.wust.modules.online.config.service.DbTableHandle;
import com.wust.modules.online.config.util.TableUtil;
import com.wust.modules.online.config.util.UpdateSqlUtil;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import com.wust.modules.online.cgform.entity.OnlCgformButton;
import com.wust.modules.online.cgform.entity.OnlCgformEnhanceJava;
import com.wust.modules.online.cgform.entity.OnlCgformEnhanceJs;
import com.wust.modules.online.cgform.entity.OnlCgformEnhanceSql;
import com.wust.modules.online.cgform.entity.OnlCgformField;
import com.wust.modules.online.cgform.entity.OnlCgformHead;
import com.wust.modules.online.cgform.entity.OnlCgformIndex;
import com.wust.modules.online.cgform.mapper.OnlCgformButtonMapper;
import com.wust.modules.online.cgform.mapper.OnlCgformEnhanceJavaMapper;
import com.wust.modules.online.cgform.mapper.OnlCgformEnhanceJsMapper;
import com.wust.modules.online.cgform.mapper.OnlCgformEnhanceSqlMapper;
import com.wust.modules.online.cgform.mapper.OnlCgformHeadMapper;
import com.wust.modules.online.cgform.service.IOnlCgformHeadService;
import com.wust.modules.online.cgform.service.IOnlCgformIndexService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author wanheng
 */
@Slf4j
@Service("onlCgformHeadServiceImpl")
public class OnlCgformHeadServiceImpl extends ServiceImpl<OnlCgformHeadMapper, OnlCgformHead> implements IOnlCgformHeadService {
    @Resource
    private IOnlCgformFieldService fieldService;
    @Resource
    private IOnlCgformIndexService indexService;
    @Resource
    private OnlCgformEnhanceJsMapper onlCgformEnhanceJsMapper;
    @Resource
    private OnlCgformButtonMapper onlCgformButtonMapper;
    @Resource
    private OnlCgformEnhanceJavaMapper onlCgformEnhanceJavaMapper;
    @Resource
    private OnlCgformEnhanceSqlMapper onlCgformEnhanceSqlMapper;
    @Resource
    private DataBaseConfig dataBaseConfig;


    @Override
    @Transactional(
            rollbackFor = {Exception.class}
    )
    public Result<?> addAll(OnlCgformHeadModel model) {
        String id = UUID.randomUUID().toString().replace("-", "");
        OnlCgformHead onlCgformHead = model.getHead();
        List fields = model.getFields();
        List indexs = model.getIndexs();
        onlCgformHead.setId(id);

        for (int i = 0; i < fields.size(); ++i) {
            OnlCgformField onlCgformField = (OnlCgformField) fields.get(i);
            onlCgformField.setId(null);
            onlCgformField.setCgformHeadId(id);
            if (onlCgformField.getOrderNum() == null) {
                onlCgformField.setOrderNum(i);
            }

            this.setFieldLength(onlCgformField);
        }

        for (Object index : indexs) {
            OnlCgformIndex onlCgformIndex = (OnlCgformIndex) index;
            onlCgformIndex.setId(null);
            onlCgformIndex.setCgformHeadId(id);
        }

        onlCgformHead.setIsDbSynch("N");
        onlCgformHead.setTableVersion(1);
        onlCgformHead.setCopyType(0);
        if (onlCgformHead.getTableType() == 3 && onlCgformHead.getTabOrderNum() == null) {
            onlCgformHead.setTabOrderNum(1);
        }

        super.save(onlCgformHead);
        this.fieldService.saveBatch(fields);
        this.indexService.saveBatch(indexs);
        this.saveOnlCgformHead(onlCgformHead, fields);
        return Result.ok("添加成功");
    }

    @Override
    @Transactional(
            rollbackFor = {Exception.class}
    )
    public Result<?> editAll(OnlCgformHeadModel model) {
        OnlCgformHead newOnlCgformHeadModel = model.getHead();
        OnlCgformHead onlCgformHead = super.getById(newOnlCgformHeadModel.getId());
        if (onlCgformHead == null) {
            return Result.error("未找到对应实体");
        } else {
            String isDbSynch = onlCgformHead.getIsDbSynch();
            if (SqlSymbolUtil.onlCgformHeadEquals(onlCgformHead, newOnlCgformHeadModel)) {
                isDbSynch = "N";
            }

            Integer tableVersion = onlCgformHead.getTableVersion();
            if (tableVersion == null) {
                tableVersion = 1;
            }

            newOnlCgformHeadModel.setTableVersion(tableVersion + 1);
            List fields = model.getFields();
            List<OnlCgformIndex> onlCgformIndices = model.getIndexs();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList1 = new ArrayList();

            for (Object field : fields) {
                OnlCgformField onlCgformField = (OnlCgformField) field;
                String onlCgformFieldId = String.valueOf(onlCgformField.getId());
                this.setFieldLength(onlCgformField);
                if (onlCgformFieldId.length() == 32) {
                    arrayList1.add(onlCgformField);
                } else {
                    if (!"_pk".equals(onlCgformFieldId)) {
                        onlCgformField.setId(null);
                        onlCgformField.setCgformHeadId(newOnlCgformHeadModel.getId());
                        arrayList.add(onlCgformField);
                    }
                }
            }

            if (arrayList.size() > 0) {
                isDbSynch = "N";
            }

            int orderNum = 0;

            Iterator iterator1;
            OnlCgformField onlCgformField;
            for (iterator1 = arrayList1.iterator(); iterator1.hasNext(); this.fieldService.updateById(onlCgformField)) {
                onlCgformField = (OnlCgformField) iterator1.next();
                OnlCgformField onlCgformField1 = (OnlCgformField) this.fieldService.getById(onlCgformField.getId());
                boolean fieldEquals = SqlSymbolUtil.fieldEquals(onlCgformField1, onlCgformField);
                if (fieldEquals) {
                    isDbSynch = "N";
                }

                if ((onlCgformField1.getOrderNum() == null ? 0 : onlCgformField1.getOrderNum()) > orderNum) {
                    orderNum = onlCgformField1.getOrderNum();
                }
            }

            for (iterator1 = arrayList.iterator(); iterator1.hasNext(); this.fieldService.save(onlCgformField)) {
                onlCgformField = (OnlCgformField) iterator1.next();
                if (onlCgformField.getOrderNum() == null) {
                    ++orderNum;
                    onlCgformField.setOrderNum(orderNum);
                }
            }

            List cgformIndices = this.indexService.getCgformIndexsByCgformId(newOnlCgformHeadModel.getId());
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            Iterator iterator2 = onlCgformIndices.iterator();

            OnlCgformIndex onlCgformIndex = new OnlCgformIndex();
            while (iterator2.hasNext()) {
                onlCgformIndex = (OnlCgformIndex) iterator2.next();
                String onlCgformIndexId = String.valueOf(onlCgformIndex.getId());
                if (onlCgformIndexId.length() == 32) {
                    arrayList3.add(onlCgformIndex);
                } else {
                    onlCgformIndex.setId((String) null);
                    onlCgformIndex.setCgformHeadId(newOnlCgformHeadModel.getId());
                    arrayList2.add(onlCgformIndex);
                }
            }

            iterator2 = cgformIndices.iterator();

            while (iterator2.hasNext()) {
                onlCgformIndex = (OnlCgformIndex) iterator2.next();
                String id = onlCgformIndex.getId();
                boolean anyMatch = onlCgformIndices.stream().anyMatch((onlCgformIndex1) -> {
                    return onlCgformIndex1.getId().equals(id);
                });
                if (!anyMatch) {
                    onlCgformIndex.setDelFlag(CommonConstant.DEL_FLAG_1);
                    arrayList3.add(onlCgformIndex);
                    isDbSynch = "N";
                }
            }

            if (arrayList2.size() > 0) {
                isDbSynch = "N";
                this.indexService.saveBatch(arrayList2);
            }

            for (iterator2 = arrayList3.iterator(); iterator2.hasNext(); this.indexService.updateById(onlCgformIndex)) {
                onlCgformIndex = (OnlCgformIndex) iterator2.next();
                OnlCgformIndex onlCgformIndex1 = (OnlCgformIndex) this.indexService.getById(onlCgformIndex.getId());
                boolean indexEquals = SqlSymbolUtil.indexEquals(onlCgformIndex1, onlCgformIndex);
                if (indexEquals) {
                    isDbSynch = "N";
                    onlCgformIndex.setIsDbSynch("N");
                }
            }

            if (model.getDeleteFieldIds().size() > 0) {
                isDbSynch = "N";
                this.fieldService.removeByIds(model.getDeleteFieldIds());
            }

            newOnlCgformHeadModel.setIsDbSynch(isDbSynch);
            super.updateById(newOnlCgformHeadModel);
            this.saveOnlCgformHead(newOnlCgformHeadModel, fields);
            this.saveField(newOnlCgformHeadModel, fields);
            return Result.ok("全部修改成功");
        }
    }

    @Override
    public void doDbSynch(String code, String synMethod) throws HibernateException, IOException, TemplateException, SQLException, DBException {
        OnlCgformHead onlCgformHead = (OnlCgformHead) this.getById(code);
        if (onlCgformHead == null) {
            throw new DBException("实体配置不存在");
        } else {
            String tableName = onlCgformHead.getTableName();
            LambdaQueryWrapper<OnlCgformField> lambdaQueryWrapper = new LambdaQueryWrapper();
            lambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, code);
            lambdaQueryWrapper.orderByAsc(OnlCgformField::getOrderNum);
            List fields = this.fieldService.list(lambdaQueryWrapper);
            TableConfig tableConfig = new TableConfig();
            tableConfig.setTableName(tableName);
            log.info("tableName--->" + tableName);
            tableConfig.setJFormPkType(onlCgformHead.getIdType());
            log.info("JformPkType--->" + onlCgformHead.getIdType());
            tableConfig.setJFormPkSequence(onlCgformHead.getIdSequence());
            log.info(onlCgformHead.getIdSequence());
            tableConfig.setContent(onlCgformHead.getTableTxt());
            log.info(onlCgformHead.getTableTxt());
            tableConfig.setColumns(fields);
            log.info(fields.toString());
            tableConfig.setDataBaseConfig(this.dataBaseConfig);
            log.info(dataBaseConfig.toString());
            if ("normal".equals(synMethod)) {
                long currentTimeMillis = System.currentTimeMillis();
                boolean exist = TableUtil.isTableExist(tableName);
                log.info("==判断表是否存在消耗时间" + (System.currentTimeMillis() - currentTimeMillis) + "毫秒");
                if (exist) {
                    UpdateSqlUtil updateSqlUtil = new UpdateSqlUtil();
                    List list = updateSqlUtil.getUpdateSql(tableConfig);
                    Iterator iterator = list.iterator();

                    while (iterator.hasNext()) {
                        String next = (String) iterator.next();
                        if (!oConvertUtils.isEmpty(next) && !oConvertUtils.isEmpty(next.trim())) {
                            ((OnlCgformHeadMapper) this.baseMapper).executeDDL(next);
                        }
                    }

                    List indexs = this.indexService.list((Wrapper) (new LambdaQueryWrapper<OnlCgformIndex>()).eq(OnlCgformIndex::getCgformHeadId, code));
                    Iterator iterator1 = indexs.iterator();

                    label54:
                    while (true) {
                        OnlCgformIndex onlCgformIndex;
                        do {
                            if (!iterator1.hasNext()) {
                                break label54;
                            }

                            onlCgformIndex = (OnlCgformIndex) iterator1.next();
                        } while (!"N".equals(onlCgformIndex.getIsDbSynch()) && !CommonConstant.DEL_FLAG_1.equals(onlCgformIndex.getDelFlag()));

                        String sql = updateSqlUtil.countIndex(onlCgformIndex.getIndexName(), tableName);
                        if (this.indexService.isExistIndex(sql)) {
                            String drop = updateSqlUtil.dropIndexs(onlCgformIndex.getIndexName(), tableName);

                            try {
                                log.info("删除索引 executeDDL:" + drop);
                                ((OnlCgformHeadMapper) this.baseMapper).executeDDL(drop);
                                if (CommonConstant.DEL_FLAG_1.equals(onlCgformIndex.getDelFlag())) {
                                    this.indexService.removeById(onlCgformIndex.getId());
                                }
                            } catch (Exception e) {
                                log.error("删除表【" + tableName + "】索引(" + onlCgformIndex.getIndexName() + ")失败!", e);
                            }
                        } else if (CommonConstant.DEL_FLAG_1.equals(onlCgformIndex.getDelFlag())) {
                            this.indexService.removeById(onlCgformIndex.getId());
                        }
                    }
                } else {
                    UpdateSqlUtil.update(tableConfig);
                }
            } else if ("force".equals(synMethod)) {
                DbTableHandle dbTableHandle = TableUtil.getTableHandle();
                String dropTableSQL = dbTableHandle.dropTableSql(tableName);
                ((OnlCgformHeadMapper) this.baseMapper).executeDDL(dropTableSQL);
                UpdateSqlUtil.update(tableConfig);
            }

            this.indexService.createIndex(code, TableUtil.getDatabaseType(), tableName);
            onlCgformHead.setIsDbSynch("Y");
            this.updateById(onlCgformHead);
        }
    }

    @Override
    public void deleteRecordAndTable(String id) throws DBException, SQLException {
        OnlCgformHead onlCgformHead = (OnlCgformHead) this.getById(id);
        if (onlCgformHead == null) {
            throw new DBException("实体配置不存在");
        } else {
            long currentTimeMillis = System.currentTimeMillis();
            boolean flag = TableUtil.isTableExist(onlCgformHead.getTableName());
            log.info("==判断表是否存在消耗时间 " + (System.currentTimeMillis() - currentTimeMillis) + "毫秒");
            if (flag) {
                String dropTableSQL = TableUtil.getTableHandle().dropTableSql(onlCgformHead.getTableName());
                log.info(" 删除表  executeDDL： " + dropTableSQL);
                ((OnlCgformHeadMapper) this.baseMapper).executeDDL(dropTableSQL);
            }

            ((OnlCgformHeadMapper) this.baseMapper).deleteById(id);
            this.updateMainTable(onlCgformHead);
        }
    }

    @Override
    public void deleteRecord(String id) throws DBException, SQLException {
        OnlCgformHead onlCgformHead = (OnlCgformHead) this.getById(id);
        if (onlCgformHead == null) {
            throw new DBException("实体配置不存在");
        } else {
            ((OnlCgformHeadMapper) this.baseMapper).deleteById(id);
            this.updateMainTable(onlCgformHead);
        }
    }

    private void updateMainTable(OnlCgformHead onlCgformHead) {
        if (onlCgformHead.getTableType() == 3) {
            LambdaQueryWrapper lambdaQueryWrapper = (LambdaQueryWrapper) (new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, onlCgformHead.getId());
            List list = this.fieldService.list(lambdaQueryWrapper);
            String mainTable = null;
            Iterator iterator = list.iterator();
            if (iterator.hasNext()) {
                OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                mainTable = onlCgformField.getMainTable();
            }

            if (oConvertUtils.isNotEmpty(mainTable)) {
                OnlCgformHead onlCgformHead1 = (OnlCgformHead) ((OnlCgformHeadMapper) this.baseMapper).selectOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, mainTable));
                if (onlCgformHead1 != null) {
                    String subTableStr = onlCgformHead1.getSubTableStr();
                    if (oConvertUtils.isNotEmpty(subTableStr)) {
                        List subTableStrs = (List) Arrays.asList(subTableStr.split(",")).stream().collect(Collectors.toList());
                        subTableStrs.remove(onlCgformHead.getTableName());
                        onlCgformHead1.setSubTableStr(String.join(",", subTableStrs));
                        ((OnlCgformHeadMapper) this.baseMapper).updateById(onlCgformHead1);
                    }
                }
            }
        }

    }

    @Override
    public List<Map<String, Object>> queryListData(String sql) {
        return ((OnlCgformHeadMapper) this.baseMapper).queryList(sql);
    }

    @Override
    public void saveEnhance(OnlCgformEnhanceJs onlCgformEnhanceJs) {
        this.onlCgformEnhanceJsMapper.insert(onlCgformEnhanceJs);
    }

    @Override
    public OnlCgformEnhanceJs queryEnhance(String code, String type) {
        return (OnlCgformEnhanceJs) this.onlCgformEnhanceJsMapper.selectOne((Wrapper) ((LambdaQueryWrapper<OnlCgformEnhanceJs>) (new LambdaQueryWrapper<OnlCgformEnhanceJs>()).eq(OnlCgformEnhanceJs::getCgJsType, type)).eq(OnlCgformEnhanceJs::getCgformHeadId, code));
    }

    @Override
    public void editEnhance(OnlCgformEnhanceJs onlCgformEnhanceJs) {
        this.onlCgformEnhanceJsMapper.updateById(onlCgformEnhanceJs);
    }

    @Override
    public OnlCgformEnhanceSql queryEnhanceSql(String formId, String buttonCode) {
        return (OnlCgformEnhanceSql) this.onlCgformEnhanceSqlMapper.selectOne((Wrapper) ((LambdaQueryWrapper<OnlCgformEnhanceSql>) (new LambdaQueryWrapper<OnlCgformEnhanceSql>()).eq(OnlCgformEnhanceSql::getCgformHeadId, formId)).eq(OnlCgformEnhanceSql::getButtonCode, buttonCode));
    }

    @Override
    public void saveEnhance(OnlCgformEnhanceSql onlCgformEnhanceSql) {
        this.onlCgformEnhanceSqlMapper.insert(onlCgformEnhanceSql);
    }

    @Override
    public void editEnhance(OnlCgformEnhanceSql onlCgformEnhanceSql) {
        this.onlCgformEnhanceSqlMapper.updateById(onlCgformEnhanceSql);
    }

    @Override
    public OnlCgformEnhanceJava queryEnhanceJava(OnlCgformEnhanceJava onlCgformEnhanceJava) {
        LambdaQueryWrapper<OnlCgformEnhanceJava> onlCgformEnhanceJavaLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformEnhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getButtonCode, onlCgformEnhanceJava.getButtonCode());
        onlCgformEnhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getCgformHeadId, onlCgformEnhanceJava.getCgformHeadId());
        onlCgformEnhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getCgJavaType, onlCgformEnhanceJava.getCgJavaType());
        onlCgformEnhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getEvent, onlCgformEnhanceJava.getEvent());
        return (OnlCgformEnhanceJava) this.onlCgformEnhanceJavaMapper.selectOne(onlCgformEnhanceJavaLambdaQueryWrapper);
    }

    @Override
    public void saveEnhance(OnlCgformEnhanceJava onlCgformEnhanceJava) {
        this.onlCgformEnhanceJavaMapper.insert(onlCgformEnhanceJava);
    }

    @Override
    public void editEnhance(OnlCgformEnhanceJava onlCgformEnhanceJava) {
        this.onlCgformEnhanceJavaMapper.updateById(onlCgformEnhanceJava);
    }

    @Override
    public List<OnlCgformButton> queryButtonList(String code, boolean isListButton) {
        LambdaQueryWrapper<OnlCgformButton> onlCgformButtonLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformButtonLambdaQueryWrapper.eq(OnlCgformButton::getButtonStatus, "1");
        onlCgformButtonLambdaQueryWrapper.eq(OnlCgformButton::getCgformHeadId, code);
        if (isListButton) {
            onlCgformButtonLambdaQueryWrapper.in(OnlCgformButton::getButtonStyle, new Object[]{"link", "button"});
        } else {
            onlCgformButtonLambdaQueryWrapper.eq(OnlCgformButton::getButtonStyle, "form");
        }

        onlCgformButtonLambdaQueryWrapper.orderByAsc(OnlCgformButton::getOrderNum);
        return this.onlCgformButtonMapper.selectList(onlCgformButtonLambdaQueryWrapper);
    }

    @Override
    public List<OnlCgformButton> queryButtonList(String code) {
        LambdaQueryWrapper<OnlCgformButton> lambdaQueryWrapper = new LambdaQueryWrapper();
        lambdaQueryWrapper.eq(OnlCgformButton::getButtonStatus, "1");
        lambdaQueryWrapper.eq(OnlCgformButton::getCgformHeadId, code);
        lambdaQueryWrapper.orderByAsc(OnlCgformButton::getOrderNum);
        return this.onlCgformButtonMapper.selectList(lambdaQueryWrapper);
    }

    @Override
    public boolean checkOnlyEnhance(OnlCgformEnhanceJava onlCgformEnhanceJava) {
        LambdaQueryWrapper<OnlCgformEnhanceJava> onlCgformEnhanceJavaLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformEnhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getButtonCode, onlCgformEnhanceJava.getButtonCode());
        onlCgformEnhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getCgformHeadId, onlCgformEnhanceJava.getCgformHeadId());
        Integer integer = this.onlCgformEnhanceJavaMapper.selectCount(onlCgformEnhanceJavaLambdaQueryWrapper);
        if (integer != null) {
            if (integer == 1 && oConvertUtils.isEmpty(onlCgformEnhanceJava.getId())) {
                return false;
            }

            if (integer == 2) {
                return false;
            }
        }

        return true;
    }


    @Override
    public List<String> queryOnlinetables() {
        return ((OnlCgformHeadMapper) this.baseMapper).queryOnlineTables();
    }

    @Override
    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void saveDbTable2Online(String tbname) {
        OnlCgformHead onlCgformHead = new OnlCgformHead();
        onlCgformHead.setTableType(1);
        onlCgformHead.setIsCheckbox("Y");
        onlCgformHead.setIsDbSynch("Y");
        onlCgformHead.setIsTree("N");
        onlCgformHead.setIsPage("Y");
        onlCgformHead.setQueryMode("group");
        onlCgformHead.setTableName(tbname.toLowerCase());
        onlCgformHead.setTableTxt(tbname);
        onlCgformHead.setTableVersion(1);
        onlCgformHead.setFormTemplate("1");
        onlCgformHead.setCopyType(0);
        onlCgformHead.setScroll(1);
        onlCgformHead.setThemeTemplate("normal");
        String id = UUIDGenerator.generate();
        onlCgformHead.setId(id);
        ArrayList arrayList = new ArrayList();

        try {
            List sqlList = DbReadTableUtil.getColumnList(tbname);

            for (int i = 0; i < sqlList.size(); ++i) {
                ColumnVo columnVo = (ColumnVo) sqlList.get(i);
                log.info("  columnt : " + columnVo.toString());
                String fieldDbName = columnVo.getFieldDbName();
                OnlCgformField onlCgformField = new OnlCgformField();
                onlCgformField.setCgformHeadId(id);
                onlCgformField.setDbFieldNameOld(columnVo.getFieldDbName().toLowerCase());
                onlCgformField.setDbFieldName(columnVo.getFieldDbName().toLowerCase());
                if (oConvertUtils.isNotEmpty(columnVo.getFiledComment())) {
                    onlCgformField.setDbFieldTxt(columnVo.getFiledComment());
                } else {
                    onlCgformField.setDbFieldTxt(columnVo.getFieldName());
                }

                onlCgformField.setDbIsKey(0);
                onlCgformField.setIsShowForm(1);
                onlCgformField.setIsQuery(0);
                onlCgformField.setFieldMustInput("0");
                onlCgformField.setIsShowList(1);
                onlCgformField.setOrderNum(i + 1);
                onlCgformField.setQueryMode("single");
                onlCgformField.setDbLength(oConvertUtils.getInt(columnVo.getPrecision()));
                onlCgformField.setFieldLength(120);
                onlCgformField.setDbPointLength(oConvertUtils.getInt(columnVo.getScale()));
                onlCgformField.setFieldShowType("text");
                onlCgformField.setDbIsNull("Y".equals(columnVo.getNullable()) ? 1 : 0);
                onlCgformField.setIsReadOnly(0);
                if ("id".equalsIgnoreCase(fieldDbName)) {
                    String[] strings = new String[]{"java.lang.Integer", "java.lang.Long"};
                    String fieldType = columnVo.getFieldType();
                    if (Arrays.asList(strings).contains(fieldType)) {
                        onlCgformHead.setIdType("NATIVE");
                    } else {
                        onlCgformHead.setIdType("UUID");
                    }

                    onlCgformField.setDbIsKey(1);
                    onlCgformField.setIsShowForm(0);
                    onlCgformField.setIsShowList(0);
                    onlCgformField.setIsReadOnly(1);
                }

                if ("java.lang.Integer".equalsIgnoreCase(columnVo.getFieldType())) {
                    onlCgformField.setDbType("int");
                } else if ("java.lang.Long".equalsIgnoreCase(columnVo.getFieldType())) {
                    onlCgformField.setDbType("int");
                } else if ("java.util.Date".equalsIgnoreCase(columnVo.getFieldType())) {
                    onlCgformField.setDbType("Date");
                    onlCgformField.setFieldShowType("date");
                } else if (!"java.lang.Double".equalsIgnoreCase(columnVo.getFieldType()) && !"java.lang.Float".equalsIgnoreCase(columnVo.getFieldType())) {
                    if (!"java.math.BigDecimal".equalsIgnoreCase(columnVo.getFieldType()) && !"BigDecimal".equalsIgnoreCase(columnVo.getFieldType())) {
                        if (!"byte[]".equalsIgnoreCase(columnVo.getFieldType()) && !columnVo.getFieldType().contains("blob")) {
                            if (!"java.lang.Object".equals(columnVo.getFieldType()) || !"text".equalsIgnoreCase(columnVo.getFieldDbType()) && !"ntext".equalsIgnoreCase(columnVo.getFieldDbType())) {
                                if ("java.lang.Object".equals(columnVo.getFieldType()) && "image".equalsIgnoreCase(columnVo.getFieldDbType())) {
                                    onlCgformField.setDbType("Blob");
                                } else {
                                    onlCgformField.setDbType("string");
                                }
                            } else {
                                onlCgformField.setDbType("Text");
                                onlCgformField.setFieldShowType("textarea");
                            }
                        } else {
                            onlCgformField.setDbType("Blob");
                            columnVo.setCharMaxLength((String) null);
                        }
                    } else {
                        onlCgformField.setDbType("BigDecimal");
                    }
                } else {
                    onlCgformField.setDbType("double");
                }

                if (oConvertUtils.isEmpty(columnVo.getPrecision()) && oConvertUtils.isNotEmpty(columnVo.getCharMaxLength())) {
                    if (Long.valueOf(columnVo.getCharMaxLength()) >= 3000L) {
                        onlCgformField.setDbType("Text");
                        onlCgformField.setFieldShowType("textarea");

                        try {
                            onlCgformField.setDbLength(Integer.valueOf(columnVo.getCharMaxLength()));
                        } catch (Exception e) {
                            log.error(e.getMessage(), e);
                        }
                    } else {
                        onlCgformField.setDbLength(Integer.valueOf(columnVo.getCharMaxLength()));
                    }
                } else {
                    if (oConvertUtils.isNotEmpty(columnVo.getPrecision())) {
                        onlCgformField.setDbLength(Integer.valueOf(columnVo.getPrecision()));
                    } else if ("int".equals(onlCgformField.getDbType())) {
                        onlCgformField.setDbLength(10);
                    }

                    if (oConvertUtils.isNotEmpty(columnVo.getScale())) {
                        onlCgformField.setDbPointLength(Integer.valueOf(columnVo.getScale()));
                    }
                }

                if (oConvertUtils.getInt(columnVo.getPrecision()) == -1 && oConvertUtils.getInt(columnVo.getScale()) == 0) {
                    onlCgformField.setDbType("Text");
                }

                if ("Blob".equals(onlCgformField.getDbType()) || "Text".equals(onlCgformField.getDbType()) || "Date".equals(onlCgformField.getDbType())) {
                    onlCgformField.setDbLength(0);
                    onlCgformField.setDbPointLength(0);
                }

                arrayList.add(onlCgformField);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        if (oConvertUtils.isEmpty(onlCgformHead.getFormCategory())) {
            onlCgformHead.setFormCategory("bdfl_include");
        }

        this.save(onlCgformHead);
        this.fieldService.saveBatch(arrayList);
    }

    private void saveOnlCgformHead(OnlCgformHead onlCgformHead, List<OnlCgformField> onlCgformFields) {
        if (onlCgformHead.getTableType() == 3) {
            onlCgformHead = this.baseMapper.selectById(onlCgformHead.getId());
            for (int i = 0; i < onlCgformFields.size(); ++i) {
                OnlCgformField onlCgformField = onlCgformFields.get(i);
                String mainTable = onlCgformField.getMainTable();
                if (!oConvertUtils.isEmpty(mainTable)) {
                    OnlCgformHead onlCgformHead1 = this.baseMapper.selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, mainTable));
                    if (onlCgformHead1 != null) {
                        String tableStr = onlCgformHead1.getSubTableStr();
                        if (oConvertUtils.isEmpty(tableStr)) {
                            tableStr = onlCgformHead.getTableName();
                        } else if (tableStr.indexOf(onlCgformHead.getTableName()) < 0) {
                            ArrayList tableStrs = new ArrayList(Arrays.asList(tableStr.split(",")));

                            for (int j = 0; j < tableStrs.size(); ++j) {
                                String tableStr1 = (String) tableStrs.get(j);
                                OnlCgformHead onlCgformHead2 = this.baseMapper.selectOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, tableStr1));
                                if (onlCgformHead2 != null && onlCgformHead.getTabOrderNum() < oConvertUtils.getInt(onlCgformHead2.getTabOrderNum(), 0)) {
                                    tableStrs.add(j, onlCgformHead.getTableName());
                                    break;
                                }
                            }

                            if (tableStrs.indexOf(onlCgformHead.getTableName()) < 0) {
                                tableStrs.add(onlCgformHead.getTableName());
                            }

                            tableStr = String.join(",", tableStrs);
                        }

                        onlCgformHead1.setSubTableStr(tableStr);
                        ((OnlCgformHeadMapper) this.baseMapper).updateById(onlCgformHead1);
                        break;
                    }
                }
            }
        } else {
            List onlCgformHeads = ((OnlCgformHeadMapper) this.baseMapper).selectList((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).like(OnlCgformHead::getSubTableStr, onlCgformHead.getTableName()));
            if (onlCgformHeads != null && onlCgformHeads.size() > 0) {
                Iterator iterator = onlCgformHeads.iterator();

                while (iterator.hasNext()) {
                    OnlCgformHead onlCgformHead1 = (OnlCgformHead) iterator.next();
                    String subTableStr = onlCgformHead1.getSubTableStr();
                    if (onlCgformHead1.getSubTableStr().equals(onlCgformHead.getTableName())) {
                        subTableStr = "";
                    } else if (onlCgformHead1.getSubTableStr().startsWith(onlCgformHead.getTableName() + ",")) {
                        subTableStr = subTableStr.replace(onlCgformHead.getTableName() + ",", "");
                    } else if (onlCgformHead1.getSubTableStr().endsWith("," + onlCgformHead.getTableName())) {
                        subTableStr = subTableStr.replace("," + onlCgformHead.getTableName(), "");
                    } else if (onlCgformHead1.getSubTableStr().indexOf("," + onlCgformHead.getTableName() + ",") != -1) {
                        subTableStr = subTableStr.replace("," + onlCgformHead.getTableName() + ",", ",");
                    }

                    onlCgformHead1.setSubTableStr(subTableStr);
                    ((OnlCgformHeadMapper) this.baseMapper).updateById(onlCgformHead1);
                }
            }
        }

    }

    @Override
    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void saveManyFormData(String code, JSONObject json, String xAccessToken) throws DBException, BusinessException {
        OnlCgformHead onlCgformHead = (OnlCgformHead) this.getById(code);
        if (onlCgformHead == null) {
            throw new DBException("数据库主表ID[" + code + "]不存在");
        } else {
            String add = "add";
            this.executeEnhanceJava(add, "start", onlCgformHead, json);
            String substring = SqlSymbolUtil.getSubstring(onlCgformHead.getTableName());
            if (onlCgformHead.getTableType() == 2) {
                String subTableStr = onlCgformHead.getSubTableStr();
                if (oConvertUtils.isNotEmpty(subTableStr)) {
                    String[] subTableStrs = subTableStr.split(",");
                    String[] subTableStrs1 = subTableStrs;
                    int length = subTableStrs.length;

                    for (int i = 0; i < length; ++i) {
                        String ubTableStr1 = subTableStrs1[i];
                        JSONArray jsonArray = json.getJSONArray(ubTableStr1);
                        if (jsonArray != null && jsonArray.size() != 0) {
                            OnlCgformHead onlCgformHead1 = (OnlCgformHead) ((OnlCgformHeadMapper) this.baseMapper).selectOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, ubTableStr1));
                            if (onlCgformHead1 != null) {
                                List onlCgformFields = this.fieldService.list((Wrapper) (new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, onlCgformHead1.getId()));
                                String s = "";
                                String s1 = null;
                                Iterator iterator = onlCgformFields.iterator();

                                while (iterator.hasNext()) {
                                    OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                                    if (!oConvertUtils.isEmpty(onlCgformField.getMainField())) {
                                        s = onlCgformField.getDbFieldName();
                                        String mainField = onlCgformField.getMainField();
                                        if (json.get(mainField.toLowerCase()) != null) {
                                            s1 = json.getString(mainField.toLowerCase());
                                        }

                                        if (json.get(mainField.toUpperCase()) != null) {
                                            s1 = json.getString(mainField.toUpperCase());
                                        }
                                    }
                                }

                                for (int j = 0; j < jsonArray.size(); ++j) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(j);
                                    if (s1 != null) {
                                        jsonObject.put(s, s1);
                                    }

                                    this.fieldService.saveFormData(onlCgformFields, ubTableStr1, jsonObject);
                                }
                            }
                        }
                    }
                }
            }

            if ("Y".equals(onlCgformHead.getIsTree())) {
                this.fieldService.saveTreeFormData(code, substring, json, onlCgformHead.getTreeIdField(), onlCgformHead.getTreeParentIdField());
            } else {
                this.fieldService.saveFormData(code, substring, json, false);
            }

            this.executeEnhanceSql(add, code, json);
            this.executeEnhanceJava(add, "end", onlCgformHead, json);
            if (oConvertUtils.isNotEmpty(json.get("bpm_status")) || oConvertUtils.isNotEmpty(json.get("bpm_status".toUpperCase()))) {
                try {
                    HttpHeaders httpHeaders = new HttpHeaders();
                    httpHeaders.setContentType(MediaType.parseMediaType("application/json;charset=UTF-8"));
                    httpHeaders.set("Accept", "application/json;charset=UTF-8");
                    httpHeaders.set("X-Access-Token", xAccessToken);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("flowCode", "onl_" + onlCgformHead.getTableName());
                    jsonObject.put("id", json.get("id"));
                    jsonObject.put("formUrl", "modules/bpm/task/form/OnlineFormDetail");
                    jsonObject.put("formUrlMobile", "online/OnlineDetailForm");
                    String baseUrl = RestUtil.getBaseUrl() + "/process/extActProcess/saveMutilProcessDraft";
                    JSONObject jsonObject1 = (JSONObject) RestUtil.request(baseUrl, HttpMethod.POST, httpHeaders, (JSONObject) null, jsonObject, JSONObject.class).getBody();
                    if (jsonObject1 != null) {
                        String result = jsonObject1.getString("result");
                        log.info("保存流程草稿 dataId : " + result);
                    }
                } catch (Exception e) {
                    log.error("保存流程草稿异常, " + e.getMessage(), e);
                }
            }

        }
    }

    @Override
    public Map<String, Object> querySubFormData(String table, String mainId) throws DBException {
        new HashMap();
        OnlCgformHead onlCgformHead = (OnlCgformHead) this.getOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, table));
        if (onlCgformHead == null) {
            throw new DBException("数据库子表[" + table + "]不存在");
        } else {
            List formFields = this.fieldService.queryFormFields(onlCgformHead.getId(), false);
            String s = null;
            Iterator iterator = formFields.iterator();

            while (iterator.hasNext()) {
                OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                if (oConvertUtils.isNotEmpty(onlCgformField.getMainField())) {
                    s = onlCgformField.getDbFieldName();
                    break;
                }
            }

            List subFormData = this.fieldService.querySubFormData(formFields, table, s, mainId);
            if (subFormData != null && subFormData.size() == 0) {
                throw new DBException("数据库子表[" + table + "]未找到相关信息,主表ID为" + mainId);
            } else if (subFormData.size() > 1) {
                throw new DBException("数据库子表[" + table + "]存在多条记录,主表ID为" + mainId);
            } else {
                Map map = (Map) subFormData.get(0);
                return map;
            }
        }
    }

    @Override
    public List<Map<String, Object>> queryManySubFormData(String table, String mainId) throws DBException {
        OnlCgformHead onlCgformHead = this.getOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, table));
        if (onlCgformHead == null) {
            throw new DBException("数据库子表[" + table + "]不存在");
        } else {
            List formFields = this.fieldService.queryFormFields(onlCgformHead.getId(), false);
            String s = null;

            for (Object formField : formFields) {
                OnlCgformField onlCgformField = (OnlCgformField) formField;
                if (oConvertUtils.isNotEmpty(onlCgformField.getMainField())) {
                    s = onlCgformField.getDbFieldName();
                    break;
                }
            }

            List subFormDatas = this.fieldService.querySubFormData(formFields, table, s, mainId);
            if (subFormDatas != null && subFormDatas.size() == 0) {
                throw new DBException("数据库子表[" + table + "]未找到相关信息,主表ID为" + mainId);
            } else {
                ArrayList arrayList = new ArrayList(subFormDatas.size());

                for (Object subFormData : subFormDatas) {
                    Map map = (Map) subFormData;
                    arrayList.add(SqlSymbolUtil.getValueType(map));
                }

                return arrayList;
            }
        }
    }

    @Override
    public Map<String, Object> queryManyFormData(String code, String id) throws DBException {
        OnlCgformHead onlCgformHead = (OnlCgformHead) this.getById(code);
        if (onlCgformHead == null) {
            throw new DBException("数据库主表ID[" + code + "]不存在");
        } else {
//            List fields = this.fieldService.queryFormFields(code, true);
            List fields = this.fieldService.queryFormFields(code, false);
            for (Object field : fields) {
                log.info("字段：" + field);
            }
            Map formData = this.fieldService.queryFormData(fields, onlCgformHead.getTableName(), id);
            if (onlCgformHead.getTableType() == 2) {
                String subTableStr = onlCgformHead.getSubTableStr();
                if (oConvertUtils.isNotEmpty(subTableStr)) {
                    String[] subTableStrs = subTableStr.split(",");
                    String[] subTableStrs1 = subTableStrs;
                    int length = subTableStrs.length;

                    for (int i = 0; i < length; ++i) {
                        String subTableStr1 = subTableStrs1[i];
                        OnlCgformHead onlCgformHead1 = (OnlCgformHead) ((OnlCgformHeadMapper) this.baseMapper).selectOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, subTableStr1));
                        if (onlCgformHead1 != null) {
                            List fields1 = this.fieldService.queryFormFields(onlCgformHead1.getId(), false);
                            String s = "";
                            String s1 = null;
                            Iterator iterator = fields1.iterator();

                            while (iterator.hasNext()) {
                                OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                                if (!oConvertUtils.isEmpty(onlCgformField.getMainField())) {
                                    s = onlCgformField.getDbFieldName();
                                    String mainField = onlCgformField.getMainField();
                                    if (null == formData.get(mainField)) {
                                        s1 = formData.get(mainField.toUpperCase()).toString();
                                    } else {
                                        s1 = formData.get(mainField).toString();
                                    }
                                }
                            }

                            List subFormDatas = this.fieldService.querySubFormData(fields1, subTableStr1, s, s1);
                            if (subFormDatas != null && subFormDatas.size() != 0) {
                                formData.put(subTableStr1, SqlSymbolUtil.getMapList(subFormDatas));
                            } else {
                                formData.put(subTableStr1, new String[0]);
                            }
                        }
                    }
                }
            }

            return formData;
        }
    }

    @Override
    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void editManyFormData(String code, JSONObject json) throws DBException, BusinessException {
        OnlCgformHead onlCgformHead = (OnlCgformHead) this.getById(code);
        if (onlCgformHead == null) {
            throw new DBException("数据库主表ID[" + code + "]不存在");
        } else {
            String edit = "edit";
            this.executeEnhanceJava(edit, "start", onlCgformHead, json);
            String tableName = onlCgformHead.getTableName();
            if ("Y".equals(onlCgformHead.getIsTree())) {
                this.fieldService.editTreeFormData(code, tableName, json, onlCgformHead.getTreeIdField(), onlCgformHead.getTreeParentIdField());
            } else {
                this.fieldService.editFormData(code, tableName, json, false);
            }

            if (onlCgformHead.getTableType() == 2) {
                String subTableStr = onlCgformHead.getSubTableStr();
                if (oConvertUtils.isNotEmpty(subTableStr)) {
                    String[] subTableStrs = subTableStr.split(",");
                    String[] subTableStrs1 = subTableStrs;
                    int length = subTableStrs.length;

                    for (int i = 0; i < length; ++i) {
                        String subTableStr1 = subTableStrs1[i];
                        OnlCgformHead onlCgformHead1 = (OnlCgformHead) ((OnlCgformHeadMapper) this.baseMapper).selectOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, subTableStr1));
                        if (onlCgformHead1 != null) {
                            List onlCgformFields = this.fieldService.list((Wrapper) (new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, onlCgformHead1.getId()));
                            String s = "";
                            String s1 = null;
                            Iterator iterator = onlCgformFields.iterator();

                            while (iterator.hasNext()) {
                                OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                                if (!oConvertUtils.isEmpty(onlCgformField.getMainField())) {
                                    s = onlCgformField.getDbFieldName();
                                    String mainField = onlCgformField.getMainField();
                                    if (json.get(mainField.toLowerCase()) != null) {
                                        s1 = json.getString(mainField.toLowerCase());
                                    }

                                    if (json.get(mainField.toUpperCase()) != null) {
                                        s1 = json.getString(mainField.toUpperCase());
                                    }
                                }
                            }

                            if (!oConvertUtils.isEmpty(s1)) {
                                this.fieldService.deleteAutoList(subTableStr1, s, s1);
                                JSONArray jsonArray = json.getJSONArray(subTableStr1);
                                if (jsonArray != null && jsonArray.size() != 0) {
                                    for (int j = 0; j < jsonArray.size(); ++j) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(j);
                                        if (s1 != null) {
                                            jsonObject.put(s, s1);
                                        }

                                        this.fieldService.saveFormData(onlCgformFields, subTableStr1, jsonObject);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            this.executeEnhanceJava(edit, "end", onlCgformHead, json);
            this.executeEnhanceSql(edit, code, json);
        }
    }

    @Override
    public int executeEnhanceJava(String buttonCode, String eventType, OnlCgformHead head, JSONObject json) throws BusinessException {
        LambdaQueryWrapper<OnlCgformEnhanceJava> onlCgformEnhanceJavaLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformEnhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getActiveStatus, "1");
        onlCgformEnhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getButtonCode, buttonCode);
        onlCgformEnhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getCgformHeadId, head.getId());
        onlCgformEnhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getEvent, eventType);
        OnlCgformEnhanceJava onlCgformEnhanceJava = this.onlCgformEnhanceJavaMapper.selectOne(onlCgformEnhanceJavaLambdaQueryWrapper);
        Object javaClass = this.getJavaClass(onlCgformEnhanceJava);
        if (javaClass != null && javaClass instanceof CgformEnhanceJavaInter) {
            CgformEnhanceJavaInter cgformEnhanceJavaInter = (CgformEnhanceJavaInter) javaClass;
            return cgformEnhanceJavaInter.execute(head.getTableName(), json);
        } else {
            return 1;
        }
    }

    @Override
    public void executeEnhanceExport(OnlCgformHead head, List<Map<String, Object>> dataList) throws BusinessException {
        this.executeEnhanceList(head, "export", dataList);
    }

    @Override
    public void executeEnhanceList(OnlCgformHead head, String buttonCode, List<Map<String, Object>> dataList) throws BusinessException {
        LambdaQueryWrapper<OnlCgformEnhanceJava> enhanceJavaLambdaQueryWrapper = new LambdaQueryWrapper();
        enhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getActiveStatus, "1");
        enhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getButtonCode, buttonCode);
        enhanceJavaLambdaQueryWrapper.eq(OnlCgformEnhanceJava::getCgformHeadId, head.getId());
        List onlCgformEnhanceJavas = this.onlCgformEnhanceJavaMapper.selectList(enhanceJavaLambdaQueryWrapper);
        if (onlCgformEnhanceJavas != null && onlCgformEnhanceJavas.size() > 0) {
            Object javaClass = this.getJavaClass((OnlCgformEnhanceJava) onlCgformEnhanceJavas.get(0));
            if (javaClass instanceof CgformEnhanceJavaListInter) {
                CgformEnhanceJavaListInter cgformEnhanceJavaListInter = (CgformEnhanceJavaListInter) javaClass;
                cgformEnhanceJavaListInter.execute(head.getTableName(), dataList);
            }
        }

    }

    private Object getJavaClass(OnlCgformEnhanceJava onlCgformEnhanceJava) {
        if (onlCgformEnhanceJava != null) {
            String cgJavaType = onlCgformEnhanceJava.getCgJavaType();
            String cgJavaValue = onlCgformEnhanceJava.getCgJavaValue();
            if (oConvertUtils.isNotEmpty(cgJavaValue)) {
                Object object = null;
                if ("class".equals(cgJavaType)) {
                    try {
                        object = MyClassLoader.getClassByScn(cgJavaValue).newInstance();
                    } catch (InstantiationException e) {
                        log.error(e.getMessage(), e);
                    } catch (IllegalAccessException e) {
                        log.error(e.getMessage(), e);
                    }
                } else if ("spring".equals(cgJavaType)) {
                    object = SpringContextUtils.getBean(cgJavaValue);
                }

                return object;
            }
        }

        return null;
    }

    @Override
    public void executeEnhanceSql(String buttonCode, String formId, JSONObject json) {
        LambdaQueryWrapper<OnlCgformEnhanceSql> onlCgformEnhanceSqlLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformEnhanceSqlLambdaQueryWrapper.eq(OnlCgformEnhanceSql::getButtonCode, buttonCode);
        onlCgformEnhanceSqlLambdaQueryWrapper.eq(OnlCgformEnhanceSql::getCgformHeadId, formId);
        OnlCgformEnhanceSql onlCgformEnhanceSql = (OnlCgformEnhanceSql) this.onlCgformEnhanceSqlMapper.selectOne(onlCgformEnhanceSqlLambdaQueryWrapper);
        if (onlCgformEnhanceSql != null) {
            String sql = SqlSymbolUtil.replaceSql(onlCgformEnhanceSql.getCgbSql(), json);
            String[] sqls = sql.split(";");
            String[] sqls1 = sqls;
            int length = sqls.length;

            for (int i = 0; i < length; ++i) {
                String sql1 = sqls1[i];
                if (sql1 != null && !"".equals(sql1.toLowerCase().trim())) {
                    log.info(" online sql 增强： " + sql1);
                    ((OnlCgformHeadMapper) this.baseMapper).executeDDL(sql1);
                }
            }
        }

    }

    @Override
    public void executeCustomerButton(String buttonCode, String formId, String dataId) throws BusinessException {
        OnlCgformHead onlCgformHead = (OnlCgformHead) this.getById(formId);
        if (onlCgformHead == null) {
            throw new BusinessException("未找到表配置信息");
        } else {
            Map map = ((OnlCgformHeadMapper) this.baseMapper).queryOneByTableNameAndId(SqlSymbolUtil.getSubstring(onlCgformHead.getTableName()), dataId);
            JSONObject jsonObject = JSONObject.parseObject(JSON.toJSONString(map));
            this.executeEnhanceJava(buttonCode, "start", onlCgformHead, jsonObject);
            this.executeEnhanceSql(buttonCode, formId, jsonObject);
            this.executeEnhanceJava(buttonCode, "end", onlCgformHead, jsonObject);
        }
    }

    @Override
    public OnlCgformEnhanceJs queryEnhanceJs(String formId, String cgJsType) {
        LambdaQueryWrapper<OnlCgformEnhanceJs> onlCgformEnhanceJsLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformEnhanceJsLambdaQueryWrapper.eq(OnlCgformEnhanceJs::getCgformHeadId, formId);
        onlCgformEnhanceJsLambdaQueryWrapper.eq(OnlCgformEnhanceJs::getCgJsType, cgJsType);
        return (OnlCgformEnhanceJs) this.onlCgformEnhanceJsMapper.selectOne(onlCgformEnhanceJsLambdaQueryWrapper);
    }

    @Override
    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void deleteOneTableInfo(String formId, String dataId) throws BusinessException {
        OnlCgformHead onlCgformHead = (OnlCgformHead) this.getById(formId);
        if (onlCgformHead == null) {
            throw new BusinessException("未找到表配置信息");
        } else {
            String substring = SqlSymbolUtil.getSubstring(onlCgformHead.getTableName());
            Map map = ((OnlCgformHeadMapper) this.baseMapper).queryOneByTableNameAndId(substring, dataId);
            if (map == null) {
                throw new BusinessException("未找到数据信息");
            } else {
                String delete = "delete";
                JSONObject jsonObject = JSONObject.parseObject(JSON.toJSONString(map));
                this.executeEnhanceJava(delete, "start", onlCgformHead, jsonObject);
                this.updateParentNode(onlCgformHead, dataId);
                if (onlCgformHead.getTableType() == 2) {
                    this.fieldService.deleteAutoListMainAndSub(onlCgformHead, dataId);
                } else {
                    String sql = "delete from " + substring + " where id = '" + dataId + "'";
                    ((OnlCgformHeadMapper) this.baseMapper).deleteOne(sql);
                }

                this.executeEnhanceSql(delete, formId, jsonObject);
                this.executeEnhanceJava(delete, "end", onlCgformHead, jsonObject);
            }
        }
    }

    @Override
    public JSONObject queryFormItem(OnlCgformHead head) {
        List availableFieldList = this.fieldService.queryAvailableFields(head.getId(), head.getTableName(), head.getTaskId(), false);
        ArrayList arrayList = new ArrayList();
        List list;
        if (oConvertUtils.isEmpty(head.getTaskId())) {
            list = this.fieldService.queryDisabledFields(head.getTableName());
            if (list != null && list.size() > 0 && list.get(0) != null) {
                arrayList.addAll(list);
            }
        }

        JSONObject jsonObject = SqlSymbolUtil.getFiledJson(availableFieldList, arrayList, (FieldModel) null);
        if (head.getTableType() == 2) {
            String headSubTableStr = head.getSubTableStr();
            if (oConvertUtils.isNotEmpty(headSubTableStr)) {
                String[] heads = headSubTableStr.split(",");
                int length = heads.length;

                for (int i = 0; i < length; ++i) {
                    String head1 = heads[i];
                    OnlCgformHead onlCgformHead = (OnlCgformHead) ((OnlCgformHeadMapper) this.baseMapper).selectOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, head1));
                    if (onlCgformHead != null) {
                        List availableFields = this.fieldService.queryAvailableFields(onlCgformHead.getId(), onlCgformHead.getTableName(), (String) null, false);
                        List disabledFields = this.fieldService.queryDisabledFields(onlCgformHead.getTableName());
                        JSONObject jsonObject1 = new JSONObject();
                        if (1 == onlCgformHead.getRelationType()) {
                            jsonObject1 = SqlSymbolUtil.getFiledJson(availableFields, disabledFields, (FieldModel) null);
                        } else {
                            jsonObject1.put("columns", SqlSymbolUtil.getColumns(availableFields, disabledFields));
                        }

                        jsonObject1.put("relationType", onlCgformHead.getRelationType());
                        jsonObject1.put("view", "tab");
                        jsonObject1.put("order", onlCgformHead.getTabOrderNum());
                        jsonObject1.put("formTemplate", onlCgformHead.getFormTemplate());
                        jsonObject1.put("describe", onlCgformHead.getTableTxt());
                        jsonObject1.put("key", onlCgformHead.getTableName());
                        jsonObject.getJSONObject("properties").put(onlCgformHead.getTableName(), jsonObject1);
                    }
                }
            }
        }

        return jsonObject;
    }

    @Override
    public List<String> generateCode(OnlGenerateModel model) throws Exception {
        TableVo tableVo = new TableVo();
        tableVo.setEntityName(model.getEntityName());
        tableVo.setEntityPackage(model.getEntityPackage());
        tableVo.setFtlDescription(model.getFtlDescription());
        tableVo.setTableName(model.getTableName());
        tableVo.setSearchFieldNum(-1);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList1 = new ArrayList();
        this.getField(model.getCode(), arrayList, arrayList1);
        OnlCgformHead onlCgformHead = (OnlCgformHead) ((OnlCgformHeadMapper) this.baseMapper).selectOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getId, model.getCode()));
        HashMap hashMap = new HashMap();
        hashMap.put("scroll", onlCgformHead.getScroll() == null ? "0" : onlCgformHead.getScroll().toString());
        if ("Y".equals(onlCgformHead.getIsTree())) {
            hashMap.put("pidField", onlCgformHead.getTreeParentIdField());
            hashMap.put("hasChildren", onlCgformHead.getTreeIdField());
            hashMap.put("textField", onlCgformHead.getTreeFieldname());
        }

        tableVo.setExtendParams(hashMap);
        CgformEnum cgformEnum = CgformEnum.getCgformEnumByConfig(model.getJspMode());
        List list = (new CodeGenerateOne(tableVo, arrayList, arrayList1)).generateCodeFile(model.getProjectPath(), cgformEnum.getTemplatePath(), cgformEnum.getStylePath());
        return list;
    }

    @Override
    public List<String> generateOneToMany(OnlGenerateModel model) throws Exception {
        MainTableVo mainTableVo = new MainTableVo();
        mainTableVo.setEntityName(model.getEntityName());
        mainTableVo.setEntityPackage(model.getEntityPackage());
        mainTableVo.setFtlDescription(model.getFtlDescription());
        mainTableVo.setTableName(model.getTableName());
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList1 = new ArrayList();
        this.getField(model.getCode(), arrayList, arrayList1);
        List subList = model.getSubList();
        ArrayList arrayList2 = new ArrayList();
        Iterator iterator = subList.iterator();

        while (iterator.hasNext()) {
            OnlGenerateModel onlGenerateModel = (OnlGenerateModel) iterator.next();
            OnlCgformHead onlCgformHead = (OnlCgformHead) ((OnlCgformHeadMapper) this.baseMapper).selectOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, onlGenerateModel.getTableName()));
            if (onlCgformHead != null) {
                SubTableVo subTableVo = new SubTableVo();
                subTableVo.setEntityName(onlGenerateModel.getEntityName());
                subTableVo.setEntityPackage(model.getEntityPackage());
                subTableVo.setTableName(onlGenerateModel.getTableName());
                subTableVo.setFtlDescription(onlGenerateModel.getFtlDescription());
                Integer integer = onlCgformHead.getRelationType();
                subTableVo.setForeignRelationType(integer == 1 ? "1" : "0");
                ArrayList arrayList3 = new ArrayList();
                ArrayList arrayList4 = new ArrayList();
                OnlCgformField onlCgformField = this.getField(onlCgformHead.getId(), arrayList3, arrayList4);
                if (onlCgformField != null) {
                    subTableVo.setOriginalForeignKeys(new String[]{onlCgformField.getDbFieldName()});
                    subTableVo.setForeignKeys(new String[]{onlCgformField.getDbFieldName()});
                    subTableVo.setColums(arrayList3);
                    subTableVo.setOriginalColumns(arrayList4);
                    arrayList2.add(subTableVo);
                }
            }
        }

        CgformEnum cgformEnum = CgformEnum.getCgformEnumByConfig(model.getJspMode());
        List list = (new CodeGenerateOneToMany(mainTableVo, arrayList, arrayList1, arrayList2)).generateCodeFile(model.getProjectPath(), cgformEnum.getTemplatePath(), cgformEnum.getStylePath());
        return list;
    }

    private OnlCgformField getField(String s, List<ColumnVo> columnVos, List<ColumnVo> columnVos1) {
        LambdaQueryWrapper<OnlCgformField> onlCgformFieldLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, s);
        onlCgformFieldLambdaQueryWrapper.orderByAsc(OnlCgformField::getOrderNum);
        List onlCgformFields = this.fieldService.list(onlCgformFieldLambdaQueryWrapper);
        OnlCgformField onlCgformField = null;
        Iterator iterator = onlCgformFields.iterator();

        while (true) {
            OnlCgformField onlCgformField1;
            ColumnVo columnVo;
            do {
                if (!iterator.hasNext()) {
                    return onlCgformField;
                }

                onlCgformField1 = (OnlCgformField) iterator.next();
                if (oConvertUtils.isNotEmpty(onlCgformField1.getMainTable())) {
                    onlCgformField = onlCgformField1;
                }

                columnVo = new ColumnVo();
                columnVo.setFieldLength(onlCgformField1.getFieldLength());
                columnVo.setFieldHref(onlCgformField1.getFieldHref());
                columnVo.setFieldValidType(onlCgformField1.getFieldValidType());
                columnVo.setFieldDefault(onlCgformField1.getDbDefaultVal());
                columnVo.setFieldShowType(onlCgformField1.getFieldShowType());
                columnVo.setFieldOrderNum(onlCgformField1.getOrderNum());
                columnVo.setIsKey(onlCgformField1.getDbIsKey() == 1 ? "Y" : "N");
                columnVo.setIsShow(onlCgformField1.getIsShowForm() == 1 ? "Y" : "N");
                columnVo.setIsShowList(onlCgformField1.getIsShowList() == 1 ? "Y" : "N");
                columnVo.setIsQuery(onlCgformField1.getIsQuery() == 1 ? "Y" : "N");
                columnVo.setQueryMode(onlCgformField1.getQueryMode());
                columnVo.setDictField(onlCgformField1.getDictField());
                if (oConvertUtils.isNotEmpty(onlCgformField1.getDictTable()) && onlCgformField1.getDictTable().indexOf("where") > 0) {
                    columnVo.setDictTable(onlCgformField1.getDictTable().split("where")[0].trim());
                } else {
                    columnVo.setDictTable(onlCgformField1.getDictTable());
                }

                columnVo.setDictText(onlCgformField1.getDictText());
                columnVo.setFieldDbName(onlCgformField1.getDbFieldName());
                columnVo.setFieldName(oConvertUtils.camelName(onlCgformField1.getDbFieldName()));
                columnVo.setFiledComment(onlCgformField1.getDbFieldTxt());
                columnVo.setFieldDbType(onlCgformField1.getDbType());
                columnVo.setFieldType(this.toLowerCase(onlCgformField1.getDbType()));
                columnVo.setClassType(onlCgformField1.getFieldShowType());
                columnVo.setClassTypeRow(onlCgformField1.getFieldShowType());
                if (onlCgformField1.getDbIsNull() != 0 && !"*".equals(onlCgformField1.getFieldValidType()) && !"1".equals(onlCgformField1.getFieldMustInput())) {
                    columnVo.setNullable("Y");
                } else {
                    columnVo.setNullable("N");
                }

                columnVos1.add(columnVo);
            } while (onlCgformField1.getIsShowForm() != 1 && onlCgformField1.getIsShowList() != 1);

            columnVos.add(columnVo);
        }
    }

    private String toLowerCase(String dbType) {
        dbType = dbType.toLowerCase();
        if (dbType.indexOf("int") >= 0) {
            return "java.lang.Integer";
        } else if (dbType.indexOf("double") >= 0) {
            return "java.lang.Double";
        } else if (dbType.indexOf("decimal") >= 0) {
            return "java.math.BigDecimal";
        } else {
            return dbType.indexOf("date") >= 0 ? "java.util.Date" : "java.lang.String";
        }
    }

    @Override
    public void addCrazyFormData(String tbname, JSONObject json) throws DBException, UnsupportedEncodingException {
        OnlCgformHead onlCgformHead = (OnlCgformHead) this.getOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, tbname));
        if (onlCgformHead == null) {
            throw new DBException("数据库主表[" + tbname + "]不存在");
        } else {
            if (onlCgformHead.getTableType() == 2) {
                String subTableStr = onlCgformHead.getSubTableStr();
                if (subTableStr != null) {
                    String[] subTableStrs = subTableStr.split(",");
                    String[] subTableStrs1 = subTableStrs;
                    int length = subTableStrs.length;

                    for (int i = 0; i < length; ++i) {
                        String subTableStrs2 = subTableStrs1[i];
                        String jsonString = json.getString("sub-table-design_" + subTableStrs2);
                        if (!oConvertUtils.isEmpty(jsonString)) {
                            JSONArray jsonArray = JSONArray.parseArray(URLDecoder.decode(jsonString, "UTF-8"));
                            if (jsonArray != null && jsonArray.size() != 0) {
                                OnlCgformHead onlCgformHead1 = (OnlCgformHead) ((OnlCgformHeadMapper) this.baseMapper).selectOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, subTableStrs2));
                                if (onlCgformHead1 != null) {
                                    List onlCgformFields = this.fieldService.list((Wrapper) (new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, onlCgformHead1.getId()));
                                    String s = "";
                                    String s1 = null;
                                    Iterator iterator = onlCgformFields.iterator();

                                    while (iterator.hasNext()) {
                                        OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                                        if (!oConvertUtils.isEmpty(onlCgformField.getMainField())) {
                                            s = onlCgformField.getDbFieldName();
                                            String mainField = onlCgformField.getMainField();
                                            s1 = json.getString(mainField);
                                        }
                                    }

                                    for (int j = 0; j < jsonArray.size(); ++j) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(j);
                                        if (s1 != null) {
                                            jsonObject.put(s, s1);
                                        }

                                        this.fieldService.executeInsertSql(SqlSymbolUtil.getFMakingInsertSql(subTableStrs2, onlCgformFields, jsonObject));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            this.fieldService.saveFormData(onlCgformHead.getId(), tbname, json, true);
        }
    }

    @Override
    public void editCrazyFormData(String tbname, JSONObject json) throws DBException, UnsupportedEncodingException {
        OnlCgformHead onlCgformHead = (OnlCgformHead) this.getOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, tbname));
        if (onlCgformHead == null) {
            throw new DBException("数据库主表[" + tbname + "]不存在");
        } else {
            if (onlCgformHead.getTableType() == 2) {
                String subTableStr = onlCgformHead.getSubTableStr();
                String[] subTableStrs = subTableStr.split(",");
                String[] subTableStrs1 = subTableStrs;
                int length = subTableStrs.length;

                for (int i = 0; i < length; ++i) {
                    String subTableStr1 = subTableStrs1[i];
                    OnlCgformHead onlCgformHead1 = (OnlCgformHead) ((OnlCgformHeadMapper) this.baseMapper).selectOne((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, subTableStr1));
                    if (onlCgformHead1 != null) {
                        List onlCgformFields = this.fieldService.list((Wrapper) (new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, onlCgformHead1.getId()));
                        String s = "";
                        String s1 = null;
                        Iterator iterator = onlCgformFields.iterator();

                        while (iterator.hasNext()) {
                            OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                            if (!oConvertUtils.isEmpty(onlCgformField.getMainField())) {
                                s = onlCgformField.getDbFieldName();
                                String mainField = onlCgformField.getMainField();
                                s1 = json.getString(mainField);
                            }
                        }

                        if (!oConvertUtils.isEmpty(s1)) {
                            this.fieldService.deleteAutoList(subTableStr1, s, s1);
                            String jsonString = json.getString("sub-table-design_" + subTableStr1);
                            if (!oConvertUtils.isEmpty(jsonString)) {
                                JSONArray jsonArray = JSONArray.parseArray(URLDecoder.decode(jsonString, "UTF-8"));
                                if (jsonArray != null && jsonArray.size() != 0) {
                                    for (int j = 0; j < jsonArray.size(); ++j) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(j);
                                        if (s1 != null) {
                                            jsonObject.put(s, s1);
                                        }

                                        this.fieldService.executeInsertSql(SqlSymbolUtil.getFMakingInsertSql(subTableStr1, onlCgformFields, jsonObject));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            this.fieldService.editFormData(onlCgformHead.getId(), tbname, json, true);
        }
    }

    @Override
    public Integer getMaxCopyVersion(String physicId) {
        Integer maxCopyVersion = ((OnlCgformHeadMapper) this.baseMapper).getMaxCopyVersion(physicId);
        return maxCopyVersion == null ? 0 : maxCopyVersion;
    }

    @Override
    public void copyOnlineTableConfig(OnlCgformHead physicTable) throws Exception {
        String physicTableId = physicTable.getId();
        int maxCopyVersion = this.getMaxCopyVersion(physicTableId);
        OnlCgformHead onlCgformHead = new OnlCgformHead();
        String id = UUIDGenerator.generate();
        onlCgformHead.setId(id);
        onlCgformHead.setPhysicId(physicTableId);
        onlCgformHead.setCopyType(1);
        ++maxCopyVersion;
        onlCgformHead.setCopyVersion(maxCopyVersion);
        onlCgformHead.setTableName(physicTable.getTableName() + "$" + maxCopyVersion);
        onlCgformHead.setTableTxt(physicTable.getTableTxt());
        onlCgformHead.setFormCategory(physicTable.getFormCategory());
        onlCgformHead.setFormTemplate(physicTable.getFormTemplate());
        onlCgformHead.setFormTemplateMobile(physicTable.getFormTemplateMobile());
        onlCgformHead.setIdSequence(physicTable.getIdSequence());
        onlCgformHead.setIdType(physicTable.getIdType());
        onlCgformHead.setIsCheckbox(physicTable.getIsCheckbox());
        onlCgformHead.setIsPage(physicTable.getIsPage());
        onlCgformHead.setIsTree(physicTable.getIsTree());
        onlCgformHead.setQueryMode(physicTable.getQueryMode());
        onlCgformHead.setTableVersion(physicTable.getTableVersion());
        onlCgformHead.setTableType(1);
        onlCgformHead.setIsDbSynch("N");
        onlCgformHead.setTreeParentIdField(physicTable.getTreeParentIdField());
        onlCgformHead.setTreeFieldname(physicTable.getTreeFieldname());
        onlCgformHead.setTreeIdField(physicTable.getTreeIdField());
        onlCgformHead.setRelationType((Integer) null);
        onlCgformHead.setTabOrderNum((Integer) null);
        onlCgformHead.setSubTableStr((String) null);
        onlCgformHead.setThemeTemplate(physicTable.getThemeTemplate());
        onlCgformHead.setScroll(physicTable.getScroll());
        List onlCgformFields = this.fieldService.list((Wrapper) (new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, physicTableId));
        Iterator iterator = onlCgformFields.iterator();

        while (iterator.hasNext()) {
            OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
            OnlCgformField onlCgformField1 = new OnlCgformField();
            onlCgformField1.setCgformHeadId(id);
            this.copyBeans(onlCgformField, onlCgformField1);
            this.fieldService.save(onlCgformField1);
        }

        ((OnlCgformHeadMapper) this.baseMapper).insert(onlCgformHead);
    }

    @Override
    public void initCopyState(List<OnlCgformHead> headList) {
        List copyPhysicId = ((OnlCgformHeadMapper) this.baseMapper).queryCopyPhysicId();
        Iterator iterator = headList.iterator();

        while (iterator.hasNext()) {
            OnlCgformHead onlCgformHead = (OnlCgformHead) iterator.next();
            if (copyPhysicId.contains(onlCgformHead.getId())) {
                onlCgformHead.setHascopy(1);
            } else {
                onlCgformHead.setHascopy(0);
            }
        }

    }

    @Override
    public void deleteBatch(String ids, String flag) {
        String[] ids1 = ids.split(",");
        if ("1".equals(flag)) {
            String[] ids2 = ids1;
            int length = ids1.length;

            for (int i = 0; i < length; ++i) {
                String id = ids2[i];

                try {
                    this.deleteRecordAndTable(id);
                } catch (DBException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            this.removeByIds(Arrays.asList(ids1));
        }

    }

    @Override
    public void updateParentNode(OnlCgformHead head, String dataId) {
        if ("Y".equals(head.getIsTree())) {
            String tableName = SqlSymbolUtil.getSubstring(head.getTableName());
            String treeParentIdField = head.getTreeParentIdField();
            Map OnlCgformHead = ((OnlCgformHeadMapper) this.baseMapper).queryOneByTableNameAndId(tableName, dataId);
            String s = null;
            if (OnlCgformHead.get(treeParentIdField) != null && !"0".equals(OnlCgformHead.get(treeParentIdField))) {
                s = OnlCgformHead.get(treeParentIdField).toString();
            } else if (OnlCgformHead.get(treeParentIdField.toUpperCase()) != null && !"0".equals(OnlCgformHead.get(treeParentIdField.toUpperCase()))) {
                s = OnlCgformHead.get(treeParentIdField.toUpperCase()).toString();
            }

            if (s != null) {
                Integer childNode = ((OnlCgformHeadMapper) this.baseMapper).queryChildNode(tableName, treeParentIdField, s);
                if (childNode == 1) {
                    String treeIdField = head.getTreeIdField();
                    this.fieldService.updateTreeNodeNoChild(tableName, treeIdField, s);
                }
            }
        }

    }

    private void saveField(OnlCgformHead onlCgformHead, List<OnlCgformField> onlCgformFieldList) {
        List onlCgformHeadList = this.list((Wrapper) (new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getPhysicId, onlCgformHead.getId()));
        if (onlCgformHeadList != null && onlCgformHeadList.size() > 0) {
            Iterator iterator = onlCgformHeadList.iterator();

            while (true) {
                List list;
                String s;
                ArrayList arrayList;
                Iterator iterator1;
                label108:
                do {
                    while (iterator.hasNext()) {
                        OnlCgformHead onlCgformHead1 = (OnlCgformHead) iterator.next();
                        list = this.fieldService.list((Wrapper) (new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, onlCgformHead1.getId()));
                        OnlCgformField onlCgformField;
                        if (list != null && list.size() != 0) {
                            HashMap hashMap = new HashMap();
                            Iterator iterator2 = list.iterator();

                            while (iterator2.hasNext()) {
                                onlCgformField = (OnlCgformField) iterator2.next();
                                hashMap.put(onlCgformField.getDbFieldName(), 1);
                            }

                            HashMap hashMap1 = new HashMap();
                            Iterator iterator3 = onlCgformFieldList.iterator();

                            while (iterator3.hasNext()) {
                                OnlCgformField onlCgformField1 = (OnlCgformField) iterator3.next();
                                hashMap1.put(onlCgformField1.getDbFieldName(), 1);
                            }

                            arrayList = new ArrayList();
                            ArrayList arrayList1 = new ArrayList();
                            Iterator iterator4 = hashMap1.keySet().iterator();

                            while (iterator4.hasNext()) {
                                String s1 = (String) iterator4.next();
                                if (hashMap.get(s1) == null) {
                                    arrayList1.add(s1);
                                } else {
                                    arrayList.add(s1);
                                }
                            }

                            ArrayList arrayList2 = new ArrayList();
                            iterator1 = hashMap.keySet().iterator();

                            while (iterator1.hasNext()) {
                                s = (String) iterator1.next();
                                if (hashMap1.get(s) == null) {
                                    arrayList2.add(s);
                                }
                            }

                            OnlCgformField onlCgformField1;
                            if (arrayList2.size() > 0) {
                                iterator1 = list.iterator();

                                while (iterator1.hasNext()) {
                                    onlCgformField1 = (OnlCgformField) iterator1.next();
                                    if (arrayList2.contains(onlCgformField1.getDbFieldName())) {
                                        this.fieldService.removeById(onlCgformField1.getId());
                                    }
                                }
                            }

                            if (arrayList1.size() > 0) {
                                iterator1 = onlCgformFieldList.iterator();

                                while (iterator1.hasNext()) {
                                    onlCgformField1 = (OnlCgformField) iterator1.next();
                                    if (arrayList1.contains(onlCgformField1.getDbFieldName())) {
                                        OnlCgformField onlCgformField2 = new OnlCgformField();
                                        onlCgformField2.setCgformHeadId(onlCgformHead1.getId());
                                        this.copyBeans(onlCgformField1, onlCgformField2);
                                        this.fieldService.save(onlCgformField2);
                                    }
                                }
                            }
                            continue label108;
                        }

                        Iterator iterator2 = onlCgformFieldList.iterator();

                        while (iterator2.hasNext()) {
                            OnlCgformField onlCgformField1 = (OnlCgformField) iterator2.next();
                            onlCgformField = new OnlCgformField();
                            onlCgformField.setCgformHeadId(onlCgformHead1.getId());
                            this.copyBeans(onlCgformField1, onlCgformField);
                            this.fieldService.save(onlCgformField);
                        }
                    }

                    return;
                } while (arrayList.size() <= 0);

                iterator1 = arrayList.iterator();

                while (iterator1.hasNext()) {
                    s = (String) iterator1.next();
                    this.updateField(s, onlCgformFieldList, list);
                }
            }
        }
    }

    private void updateField(String s, List<OnlCgformField> onlCgformFieldList, List<OnlCgformField> onlCgformFieldList1) {
        OnlCgformField onlCgformField = null;
        Iterator iterator = onlCgformFieldList.iterator();

        while (iterator.hasNext()) {
            OnlCgformField onlCgformField1 = (OnlCgformField) iterator.next();
            if (s.equals(onlCgformField1.getDbFieldName())) {
                onlCgformField = onlCgformField1;
            }
        }

        OnlCgformField onlCgformField1 = null;
        Iterator iterator1 = onlCgformFieldList1.iterator();

        while (iterator1.hasNext()) {
            OnlCgformField onlCgformField2 = (OnlCgformField) iterator1.next();
            if (s.equals(onlCgformField2.getDbFieldName())) {
                onlCgformField1 = onlCgformField2;
            }
        }

        if (onlCgformField != null && onlCgformField1 != null) {
            boolean b = false;
            if (!onlCgformField.getDbType().equals(onlCgformField1.getDbType())) {
                onlCgformField1.setDbType(onlCgformField.getDbType());
                b = true;
            }

            if (!onlCgformField.getDbDefaultVal().equals(onlCgformField1.getDbDefaultVal())) {
                onlCgformField1.setDbDefaultVal(onlCgformField.getDbDefaultVal());
                b = true;
            }

            if (!onlCgformField.getDbLength().equals(onlCgformField1.getDbLength())) {
                onlCgformField1.setDbLength(onlCgformField.getDbLength());
                b = true;
            }

            if (!onlCgformField.getDbIsNull().equals(onlCgformField1.getDbIsNull())) {
                onlCgformField1.setDbIsNull(onlCgformField.getDbIsNull());
                b = true;
            }

            if (b) {
                this.fieldService.updateById(onlCgformField1);
            }
        }

    }

    private void copyBeans(OnlCgformField onlCgformField1, OnlCgformField onlCgformField2) {
        onlCgformField2.setDbDefaultVal(onlCgformField1.getDbDefaultVal());
        onlCgformField2.setDbFieldName(onlCgformField1.getDbFieldName());
        onlCgformField2.setDbFieldNameOld(onlCgformField1.getDbFieldNameOld());
        onlCgformField2.setDbFieldTxt(onlCgformField1.getDbFieldTxt());
        onlCgformField2.setDbIsKey(onlCgformField1.getDbIsKey());
        onlCgformField2.setDbIsNull(onlCgformField1.getDbIsNull());
        onlCgformField2.setDbLength(onlCgformField1.getDbLength());
        onlCgformField2.setDbPointLength(onlCgformField1.getDbPointLength());
        onlCgformField2.setDbType(onlCgformField1.getDbType());
        onlCgformField2.setDictField(onlCgformField1.getDictField());
        onlCgformField2.setDictTable(onlCgformField1.getDictTable());
        onlCgformField2.setDictText(onlCgformField1.getDictText());
        onlCgformField2.setFieldExtendJson(onlCgformField1.getFieldExtendJson());
        onlCgformField2.setFieldHref(onlCgformField1.getFieldHref());
        onlCgformField2.setFieldLength(onlCgformField1.getFieldLength());
        onlCgformField2.setFieldMustInput(onlCgformField1.getFieldMustInput());
        onlCgformField2.setFieldShowType(onlCgformField1.getFieldShowType());
        onlCgformField2.setFieldValidType(onlCgformField1.getFieldValidType());
        onlCgformField2.setFieldDefaultValue(onlCgformField1.getFieldDefaultValue());
        onlCgformField2.setIsQuery(onlCgformField1.getIsQuery());
        onlCgformField2.setIsShowForm(onlCgformField1.getIsShowForm());
        onlCgformField2.setIsShowList(onlCgformField1.getIsShowList());
        onlCgformField2.setMainField((String) null);
        onlCgformField2.setMainTable((String) null);
        onlCgformField2.setOrderNum(onlCgformField1.getOrderNum());
        onlCgformField2.setQueryMode(onlCgformField1.getQueryMode());
        onlCgformField2.setIsReadOnly(onlCgformField1.getIsReadOnly());
        onlCgformField2.setSortFlag(onlCgformField1.getSortFlag());
        onlCgformField2.setQueryDefVal(onlCgformField1.getQueryDefVal());
        onlCgformField2.setQueryConfigFlag(onlCgformField1.getQueryConfigFlag());
        onlCgformField2.setQueryDictField(onlCgformField1.getQueryDictField());
        onlCgformField2.setQueryDictTable(onlCgformField1.getQueryDictTable());
        onlCgformField2.setQueryDictText(onlCgformField1.getQueryDictText());
        onlCgformField2.setQueryMustInput(onlCgformField1.getQueryMustInput());
        onlCgformField2.setQueryShowType(onlCgformField1.getQueryShowType());
        onlCgformField2.setQueryValidType(onlCgformField1.getQueryValidType());
        onlCgformField2.setConverter(onlCgformField1.getConverter());
    }

    private void setFieldLength(OnlCgformField field) {
        if ("Text".equals(field.getDbType()) || "Blob".equals(field.getDbType())) {
            field.setDbLength(0);
            field.setDbPointLength(0);
        }

    }
}
