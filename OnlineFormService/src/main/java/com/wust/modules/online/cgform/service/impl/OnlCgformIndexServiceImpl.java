package com.wust.modules.online.cgform.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wust.common.constant.CommonConstant;
import com.wust.modules.online.cgform.entity.OnlCgformIndex;
import com.wust.modules.online.cgform.mapper.OnlCgformHeadMapper;
import com.wust.modules.online.cgform.mapper.OnlCgformIndexMapper;
import com.wust.modules.online.cgform.service.IOnlCgformIndexService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wanheng
 */
@Slf4j
@Service("onlCgformIndexServiceImpl")
public class OnlCgformIndexServiceImpl extends ServiceImpl<OnlCgformIndexMapper, OnlCgformIndex> implements IOnlCgformIndexService {
    @Resource
    private OnlCgformHeadMapper onlCgformHeadMapper;


    @Override
    public void createIndex(String code, String databaseType, String tbname) {
        LambdaQueryWrapper<OnlCgformIndex> onlCgformIndexLambdaQueryWrapper = new LambdaQueryWrapper();
        onlCgformIndexLambdaQueryWrapper.eq(OnlCgformIndex::getCgformHeadId, code);
        List onlCgformIndices = this.list(onlCgformIndexLambdaQueryWrapper);
        if (onlCgformIndices != null && onlCgformIndices.size() > 0) {

            for (Object cgformIndex : onlCgformIndices) {
                OnlCgformIndex onlCgformIndex = (OnlCgformIndex) cgformIndex;
                if (!CommonConstant.DEL_FLAG_1.equals(onlCgformIndex.getDelFlag()) && "N".equals(onlCgformIndex.getIsDbSynch())) {
                    String s;
                    String indexName = onlCgformIndex.getIndexName();
                    String indexField = onlCgformIndex.getIndexField();
                    String index = "normal".equals(onlCgformIndex.getIndexType()) ? " index " : onlCgformIndex.getIndexType() + " index ";
                    byte b = -1;
                    switch (databaseType.hashCode()) {
                        case -1955532418:
                            if ("ORACLE".equals(databaseType)) {
                                b = 1;
                            }
                            break;
                        case -1620389036:
                            if ("POSTGRESQL".equals(databaseType)) {
                                b = 3;
                            }
                            break;
                        case 73844866:
                            if ("MYSQL".equals(databaseType)) {
                                b = 0;
                            }
                            break;
                        case 912124529:
                            if ("SQLSERVER".equals(databaseType)) {
                                b = 2;
                            }
                        default:
                    }

                    switch (b) {
                        case 0:
                            s = "create " + index + indexName + " on " + tbname + "(" + indexField + ")";
                            break;
                        case 1:
                            s = "create " + index + indexName + " on " + tbname + "(" + indexField + ")";
                            break;
                        case 2:
                            s = "create " + index + indexName + " on " + tbname + "(" + indexField + ")";
                            break;
                        case 3:
                            s = "create " + index + indexName + " on " + tbname + "(" + indexField + ")";
                            break;
                        default:
                            s = "create " + index + indexName + " on " + tbname + "(" + indexField + ")";
                    }

                    log.info(" 创建索引 executeDDL ：" + s);
                    this.onlCgformHeadMapper.executeDDL(s);
                    onlCgformIndex.setIsDbSynch("Y");
                    this.updateById(onlCgformIndex);
                }
            }
        }

    }

    @Override
    public boolean isExistIndex(String countSql) {
        if (countSql == null) {
            return true;
        } else {
            int i = this.baseMapper.queryIndexCount(countSql);
            return i > 0;
        }
    }

    @Override
    public List<OnlCgformIndex> getCgformIndexsByCgformId(String cgformId) {
        return this.baseMapper.selectList((Wrapper)(new LambdaQueryWrapper<OnlCgformIndex>()).in(OnlCgformIndex::getCgformHeadId, new Object[]{cgformId}));
    }
}
