package com.wust.modules.online.cgform.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import java.util.List;
import java.util.Map;

import com.wust.modules.online.cgform.converter.ConverterUtil;
import com.wust.modules.online.cgform.entity.OnlCgformField;
import com.wust.modules.online.cgform.entity.OnlCgformHead;
import com.wust.modules.online.cgform.mapper.OnlCgformFieldMapper;
import com.wust.modules.online.cgform.util.SqlSymbolUtil;
import com.wust.modules.online.config.exception.BusinessException;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import com.wust.modules.online.cgform.service.IOnlCgformHeadService;
import com.wust.modules.online.cgform.service.IOnlCgformBatchService;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author wanheng
 */
@Service("onlCgformSqlServiceImpl")
public class OnlCgformSqlServiceImpl implements IOnlCgformBatchService {

    @Resource
    private SqlSessionTemplate sqlSessionTemplate;
    @Resource
    private IOnlCgformHeadService onlCgformHeadService;


    @Override
    public void saveBatchOnlineTable(OnlCgformHead head, List<OnlCgformField> fieldList, List<Map<String, Object>> dataList) {
        SqlSession sqlSession = null;

        try {
            ConverterUtil.converter(2, dataList, fieldList);
            sqlSession = this.sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH, false);
            OnlCgformFieldMapper onlCgformFieldMapper = (OnlCgformFieldMapper) sqlSession.getMapper(OnlCgformFieldMapper.class);
            short st = 1000;
            int i;
            String s;
            if (st >= dataList.size()) {
                for(i = 0; i < dataList.size(); ++i) {
                    s = JSON.toJSONString(dataList.get(i));
                    this.insertOrUpdate(s, head, fieldList, onlCgformFieldMapper);
                }
            } else {
                for(i = 0; i < dataList.size(); ++i) {
                    s = JSON.toJSONString(dataList.get(i));
                    this.insertOrUpdate(s, head, fieldList, onlCgformFieldMapper);
                    if (i % st == 0) {
                        sqlSession.commit();
                        sqlSession.clearCache();
                    }
                }
            }

            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }

    }

    private void insertOrUpdate(String s, OnlCgformHead onlCgformHead, List<OnlCgformField> onlCgformFieldList, OnlCgformFieldMapper onlCgformFieldMapper) throws BusinessException {
        JSONObject jsonObject = JSONObject.parseObject(s);
        int i = this.onlCgformHeadService.executeEnhanceJava("import", "start", onlCgformHead, jsonObject);
        String tableName = onlCgformHead.getTableName();
        Map map;
        if (1 == i) {
            map = SqlSymbolUtil.getInsertSql(tableName, onlCgformFieldList, jsonObject);
            onlCgformFieldMapper.executeInsertSql(map);
        } else if (2 == i) {
            map = SqlSymbolUtil.getUpdateSql(tableName, onlCgformFieldList, jsonObject);
            onlCgformFieldMapper.executeUpdateSql(map);
        }

    }
}
