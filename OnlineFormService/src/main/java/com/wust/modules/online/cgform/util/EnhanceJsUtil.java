package com.wust.modules.online.cgform.util;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.wust.common.util.oConvertUtils;
import com.wust.modules.online.cgform.entity.OnlCgformButton;
import com.wust.modules.online.cgform.entity.OnlCgformEnhanceJs;
import com.wust.modules.online.cgform.entity.OnlCgformField;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wanheng
 */
@Slf4j
public class EnhanceJsUtil {

    private static Pattern pattern = Pattern.compile("(\\s{1}onlChange\\s*\\(\\)\\s*\\{)");

    public static String getCgJs(String cgJs, String buttonCode) {
        String s = "(" + buttonCode + "\\s*\\(row\\)\\s*\\{)";
        String s1 = buttonCode + ":function(that,row){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;";
        String s2 = getJsFunction(cgJs, "\\}\\s*\r*\n*\\s*" + s, "}," + s1);
        if (s2 == null) {
            cgJs = getCgJs1(cgJs, s, s1);
        } else {
            cgJs = s2;
        }

        cgJs = getCgJs1(cgJs, buttonCode, null);
        return cgJs;
    }

    public static String getCgJs(String cgJs, String buttonCode, String s) {
        String s1 = "(" + oConvertUtils.getString(s) + buttonCode + "\\s*\\(\\)\\s*\\{)";
        String s2 = buttonCode + ":function(that){const getAction=this._getAction,postAction=this._postAction,deleteAction=this._deleteAction;";
        String jsFunction = getJsFunction(cgJs, "\\}\\s*\r*\n*\\s*" + s1, "}," + s2);
        if (jsFunction == null) {
            cgJs = getCgJs1(cgJs, s1, s2);
        } else {
            cgJs = jsFunction;
        }

        return cgJs;
    }

    public static String getJsFunction(String cgJs, String s1, String s2) {
        Pattern pattern = Pattern.compile(s1);
        Matcher matcher = pattern.matcher(cgJs);
        if (matcher.find()) {
            cgJs = cgJs.replace(matcher.group(0), s2);
            return cgJs;
        } else {
            return null;
        }
    }

    public static String getCgJs1(String cgJs, String s1, String s2) {
        String jsFunction = getJsFunction(cgJs, s1, s2);
        return jsFunction != null ? jsFunction : cgJs;
    }

    public static String getJsFunction(String cgJs, String dbFieldName) {
        String s = "(\\s+" + dbFieldName + "\\s*\\(\\)\\s*\\{)";
        String s1 = dbFieldName + ":function(that,event){";
        String jsFunction = getJsFunction(cgJs, "\\}\\s*\r*\n*\\s*" + s, "}," + s1);
        if (jsFunction == null) {
            cgJs = getCgJs(cgJs, s, s1);
        } else {
            cgJs = jsFunction;
        }

        return cgJs;
    }

    public static String getCgJs(String cgJs) {
        String s = "function OnlineEnhanceJs(getAction,postAction,deleteAction){return {_getAction:getAction,_postAction:postAction,_deleteAction:deleteAction," + cgJs + "}}";
        log.info("最终的增强JS", s);
        return s;
    }

    public static String getJsFunction(String CgJs, List<OnlCgformButton> onlCgformButtonList) {
        CgJs = getCgJs(CgJs, onlCgformButtonList);
        String s = "function OnlineEnhanceJs(getAction,postAction,deleteAction){return {_getAction:getAction,_postAction:postAction,_deleteAction:deleteAction," + CgJs + "}}";
        log.info("最终的增强JS", s);
        return s;
    }

    public static String getCgJs(String CgJs, List<OnlCgformButton> onlCgformButtonList) {
        if (onlCgformButtonList != null) {
            Iterator iterator = onlCgformButtonList.iterator();

            label37:
            while(true) {
                while(true) {
                    if (!iterator.hasNext()) {
                        break label37;
                    }

                    OnlCgformButton onlCgformButton = (OnlCgformButton) iterator.next();
                    String buttonCode = onlCgformButton.getButtonCode();
                    if ("link".equals(onlCgformButton.getButtonStyle())) {
                        CgJs = getCgJs(CgJs, buttonCode);
                    } else if ("button".equals(onlCgformButton.getButtonStyle()) || "form".equals(onlCgformButton.getButtonStyle())) {
                        CgJs = getCgJs(CgJs, buttonCode, null);
                    }
                }
            }
        }

        String[] strings = "beforeAdd,beforeEdit,afterAdd,afterEdit,beforeDelete,afterDelete,mounted,created,show".split(",");
        for (String s : strings) {
            if ("beforeAdd,afterAdd,mounted,created,show".contains(s)) {
                CgJs = getCgJs(CgJs, s, null);
            } else {
                CgJs = getCgJs(CgJs, s);
            }
        }

        return CgJs;
    }

    public static void getCgJs(OnlCgformEnhanceJs onlCgformEnhanceJs, List<OnlCgformField> onlCgformFieldList) {
        if (onlCgformEnhanceJs != null && !oConvertUtils.isEmpty(onlCgformEnhanceJs.getCgJs())) {
            String cgJs = " " + onlCgformEnhanceJs.getCgJs();
            log.info("one enhanceJs begin==> " + cgJs);

            Matcher matcher = pattern.matcher(cgJs);
            if (matcher.find()) {
                log.info("---JS 增强转换-main--enhanceJsFunctionName----onlChange");
                cgJs = getCgJs(cgJs, "onlChange", "\\s{1}");

                OnlCgformField onlCgformField;
                for(Iterator iterator = onlCgformFieldList.iterator(); iterator.hasNext(); cgJs = getJsFunction(cgJs, onlCgformField.getDbFieldName())) {
                    onlCgformField = (OnlCgformField) iterator.next();
                }
            }

            log.info("one enhanceJs end==> " + cgJs);
            onlCgformEnhanceJs.setCgJs(cgJs);
        }
    }

    public static void getJsFunction(OnlCgformEnhanceJs onlCgformEnhanceJs, String tableName, List<OnlCgformField> onlCgformFieldList) {
        if (onlCgformEnhanceJs != null && !oConvertUtils.isEmpty(onlCgformEnhanceJs.getCgJs())) {
            log.info(" sub enhanceJs begin==> " + onlCgformEnhanceJs);
            String cgJs = onlCgformEnhanceJs.getCgJs();
            String s = tableName + "_" + "onlChange";
            Pattern pattern = Pattern.compile("(" + s + "\\s*\\(\\)\\s*\\{)");
            Matcher matcher = pattern.matcher(cgJs);
            if (matcher.find()) {
                log.info("---JS 增强转换-sub-- enhanceJsFunctionName----" + s);
                cgJs = getCgJs(cgJs, s, null);

                OnlCgformField onlCgformField;
                for(Iterator iterator = onlCgformFieldList.iterator(); iterator.hasNext(); cgJs = getJsFunction(cgJs, onlCgformField.getDbFieldName())) {
                    onlCgformField = (OnlCgformField) iterator.next();
                }
            }

            log.info(" sub enhanceJs end==> " + cgJs);
            onlCgformEnhanceJs.setCgJs(cgJs);
        }
    }
}
