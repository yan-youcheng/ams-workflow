package com.wust.modules.online.cgform.util;

import cn.hutool.core.io.FileUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author wanheng
 */
public class FilesGeneratedUtil {


    public static File getFile(List<String> stringList, String s) throws RuntimeException {
        File file = FileUtil.touch(s);
        if (file == null) {
            return null;
        } else if (!file.getName().endsWith(".zip")) {
            return null;
        } else {
            ZipOutputStream zipOutputStream;

            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                zipOutputStream = new ZipOutputStream(fileOutputStream);
                Iterator iterator = stringList.iterator();

                while(true) {
                    File file1;
                    do {
                        do {
                            if (!iterator.hasNext()) {
                                if (zipOutputStream != null) {
                                    try {
                                        zipOutputStream.close();
                                    } catch (IOException e) {
                                        System.out.println("ZipUtil toZip close exception" + e);
                                    }
                                }

                                fileOutputStream.close();
                                return file;
                            }

                            String s1 = (String) iterator.next();
                            s1 = URLDecoder.decode(s1, "UTF-8").replace("生成成功：", "");
                            file1 = new File(s1);
                        } while(file1 == null);
                    } while(!file1.exists());

                    byte[] bts = new byte[4096];
                    String fileName = null;
                    if (file1.getAbsolutePath().indexOf("src\\") != -1) {
                        fileName = file1.getAbsolutePath().substring(file1.getAbsolutePath().indexOf("src\\") - 1);
                    } else {
                        fileName = file1.getAbsolutePath().substring(file1.getAbsolutePath().indexOf("src/") - 1);
                    }

                    zipOutputStream.putNextEntry(new ZipEntry(fileName));
                    FileInputStream fileInputStream = new FileInputStream(file1);

                    int i;
                    while((i = fileInputStream.read(bts)) != -1) {
                        zipOutputStream.write(bts, 0, i);
                    }

                    fileInputStream.close();
                    zipOutputStream.closeEntry();
                }
            } catch (Exception e) {
                throw new RuntimeException("zipFile error from ZipUtils", e);
            }
        }
    }
}
