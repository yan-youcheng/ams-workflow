package com.wust.modules.online.cgform.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;

import com.wust.common.exception.OnlineBootException;
import com.wust.common.system.api.SysBaseApi;
import com.wust.common.system.query.MatchTypeEnum;
import com.wust.common.system.query.QueryGenerator;
import com.wust.common.system.vo.SysPermissionDataRuleModel;
import com.wust.common.util.*;
import com.wust.common.util.jsonschema.BaseColumn;
import com.wust.common.util.jsonschema.AbstractCommonProperty;
import com.wust.common.util.jsonschema.JsonSchemaDescrip;
import com.wust.common.util.jsonschema.JsonSchemaUtil;
import com.wust.common.util.jsonschema.validate.*;
import com.wust.common.util.search.QueryRuleEnum;
import com.wust.modules.core.util.ApplicationContextUtil;
import com.wust.modules.online.cgform.enums.CgformValidPatternEnum;
import com.wust.modules.online.cgform.model.FieldModel;
import com.wust.modules.online.config.exception.DBException;
import com.wust.modules.online.config.util.TableUtil;
import com.wust.modules.system.entity.SysUser;
import com.wust.modules.system.service.ISysService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import com.wust.modules.online.cgform.entity.OnlCgformEnhanceJava;
import com.wust.modules.online.cgform.entity.OnlCgformField;
import com.wust.modules.online.cgform.entity.OnlCgformHead;
import com.wust.modules.online.cgform.entity.OnlCgformIndex;
import com.wust.modules.online.cgform.mapper.OnlCgformHeadMapper;
import com.wust.modules.online.cgform.service.IOnlCgformFieldService;

/**
 * @author wanheng
 */
@NoArgsConstructor
@Slf4j
public class SqlSymbolUtil {

    private static String au;

    public static void getSelect(String tbname, List<OnlCgformField> onlCgformFieldList, StringBuffer stringBuffer) {
        if (onlCgformFieldList != null && onlCgformFieldList.size() != 0) {
            stringBuffer.append("SELECT ");
            int size = onlCgformFieldList.size();
            boolean b = false;

            for (int i = 0; i < size; ++i) {
                OnlCgformField onlCgformField = onlCgformFieldList.get(i);
                if ("id".equals(onlCgformField.getDbFieldName())) {
                    b = true;
                }

                if ("cat_tree".equals(onlCgformField.getFieldShowType()) && oConvertUtils.isNotEmpty(onlCgformField.getDictText())) {
                    stringBuffer.append(onlCgformField.getDictText()).append(",");
                }

                if (i == size - 1) {
                    stringBuffer.append(onlCgformField.getDbFieldName()).append(" ");
                } else {
                    stringBuffer.append(onlCgformField.getDbFieldName()).append(",");
                }
            }

            if (!b) {
                stringBuffer.append(",id");
            }
        } else {
            stringBuffer.append("SELECT id");
        }

        stringBuffer.append(" FROM ").append(getSubstring(tbname));
    }

    public static String toDateyMdHms(String s) {
        return " to_date('" + s + "','yyyy-MM-dd HH24:mi:ss')";
    }

    public static String toDateyMd(String s) {
        return " to_date('" + s + "','yyyy-MM-dd')";
    }

    public static boolean isListType(String fieldShowType) {
        if ("list".equals(fieldShowType)) {
            return true;
        } else if ("radio".equals(fieldShowType)) {
            return true;
        } else if ("checkbox".equals(fieldShowType)) {
            return true;
        } else {
            return "list_multi".equals(fieldShowType);
        }
    }

    public static String getByDataType(List<OnlCgformField> onlCgformFields, Map<String, Object> map, List<String> list) {
        StringBuffer stringBuffer = new StringBuffer();
        String s = "";

        try {
            s = TableUtil.getDatabaseType();
        } catch (SQLException | DBException e) {
            e.printStackTrace();
        }

        Map map1 = QueryGenerator.getRuleMap();
        Iterator iterator = map1.keySet().iterator();

        while (iterator.hasNext()) {
            String s1 = (String) iterator.next();
            if (oConvertUtils.isNotEmpty(s1) && s1.startsWith("SQL_RULES_COLUMN")) {
                stringBuffer.append(" AND (").append(QueryGenerator.getSqlRuleValue(((SysPermissionDataRuleModel) map1.get(s1)).getRuleValue()))
                        .append(")");
            }
        }

        iterator = onlCgformFields.iterator();

        while (true) {
            while (true) {
                String s1;
                String s2;
                Object object;
                do {
                    while (true) {
                        OnlCgformField onlCgformField;
                        do {
                            if (!iterator.hasNext()) {
                                return stringBuffer.toString();
                            }

                            onlCgformField = (OnlCgformField) iterator.next();
                            s1 = onlCgformField.getDbFieldName();
                            s2 = onlCgformField.getDbType();
                            if (map1.containsKey(s1)) {
                                rule(s, (SysPermissionDataRuleModel) map1.get(s1), s1, s2, stringBuffer);
                            }

                            if (map1.containsKey(oConvertUtils.camelNames(s1))) {
                                rule(s, (SysPermissionDataRuleModel) map1.get(s1), s1, s2, stringBuffer);
                            }

                            if (list != null && list.contains(s1)) {
                                onlCgformField.setIsQuery(1);
                                onlCgformField.setQueryMode("single");
                            }

                            if (oConvertUtils.isNotEmpty(onlCgformField.getMainField()) && oConvertUtils.isNotEmpty(onlCgformField.getMainTable())) {
                                onlCgformField.setIsQuery(1);
                                onlCgformField.setQueryMode("single");
                            }
                        } while (1 != onlCgformField.getIsQuery());

                        if ("single".equals(onlCgformField.getQueryMode())) {
                            object = map.get(s1);
                            break;
                        }

                        object = map.get(s1 + "_begin");
                        if (object != null) {
                            stringBuffer.append(" AND ").append(s1).append(">=");
                            if (DataTypeUtil.isNumberType(s2)) {
                                stringBuffer.append(object.toString());
                            } else if ("ORACLE".equals(s) && s2.toLowerCase().contains("date")) {
                                stringBuffer.append(toDateyMdHms(object.toString()));
                            } else {
                                stringBuffer.append("'").append(object.toString()).append("'");
                            }
                        }

                        Object object1 = map.get(s1 + "_end");
                        if (object1 != null) {
                            stringBuffer.append(" AND ").append(s1).append("<=");
                            if (DataTypeUtil.isNumberType(s2)) {
                                stringBuffer.append(object1.toString());
                            } else if ("ORACLE".equals(s) && s2.toLowerCase().contains("date")) {
                                stringBuffer.append(toDateyMdHms(object1.toString()));
                            } else {
                                stringBuffer.append("'").append(object1.toString()).append("'");
                            }
                        }
                    }
                } while (object == null);

                if ("ORACLE".equals(s) && s2.toLowerCase().contains("date")) {
                    stringBuffer.append(" AND ").append(s1).append("=").append(toDateyMdHms(object.toString()));
                } else {
                    boolean b = !DataTypeUtil.isNumberType(s2);
                    String singleQueryConditionSql = QueryGenerator.getSingleQueryConditionSql(s1, "", object, b);
                    stringBuffer.append(" AND ").append(singleQueryConditionSql);
                }
            }
        }
    }

    public static String getByParams(Map<String, Object> map) {
        Object object = map.get("superQueryParams");
        if (object != null && !StringUtils.isBlank(object.toString())) {
            IOnlCgformFieldService onlCgformFieldService = SpringContextUtils.getBean(IOnlCgformFieldService.class);
            String s;

            try {
                s = URLDecoder.decode(object.toString(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return "";
            }

            JSONArray jsonArray = JSONArray.parseArray(s);
            Object object1 = map.get("superQueryMatchType");
            MatchTypeEnum matchTypeEnum = MatchTypeEnum.getByValue(object1);
            if (matchTypeEnum == null) {
                matchTypeEnum = MatchTypeEnum.AND;
            }

            HashMap hashMap = new HashMap();
            StringBuilder stringBuilder = (new StringBuilder(" AND ")).append("(");

            for (int i = 0; i < jsonArray.size(); ++i) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String field = jsonObject.getString("field");
                String[] fields = field.split(",");
                if (fields.length == 1) {
                    append(stringBuilder, field, jsonObject, matchTypeEnum, null, i == 0);
                } else if (fields.length == 2) {
                    String field1 = fields[0];
                    String field2 = fields[1];
                    JSONObject jsonObject1 = (JSONObject) hashMap.get(field1);
                    if (jsonObject1 == null) {
                        List cgformFields = onlCgformFieldService.queryFormFieldsByTableName(field1);
                        jsonObject1 = new JSONObject(3);
                        Iterator iterator = cgformFields.iterator();

                        while (iterator.hasNext()) {
                            OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                            if (StringUtils.isNotBlank(onlCgformField.getMainTable())) {
                                jsonObject1.put("subTableName", field1);
                                jsonObject1.put("subField", onlCgformField.getDbFieldName());
                                jsonObject1.put("mainTable", onlCgformField.getMainTable());
                                jsonObject1.put("mainField", onlCgformField.getMainField());
                            }
                        }

                        hashMap.put(field1, jsonObject1);
                    }

                    append(stringBuilder, field2, jsonObject, matchTypeEnum, jsonObject1, i == 0);
                }
            }

            return stringBuilder.append(")").toString();
        } else {
            return "";
        }
    }

    private static void append(StringBuilder stringBuilder, String s, JSONObject jsonObject, MatchTypeEnum matchTypeEnum, JSONObject jsonObject1, boolean b) {
        if (!b) {
            stringBuilder.append(" ").append(matchTypeEnum.getValue()).append(" ");
        }

        String type = jsonObject.getString("type");
        String val = jsonObject.getString("val");
        String sql = appendSpace(type, val);
        QueryRuleEnum queryRuleEnum = QueryRuleEnum.getByValue(jsonObject.getString("rule"));
        if (queryRuleEnum == null) {
            queryRuleEnum = QueryRuleEnum.EQ;
        }

        if (jsonObject1 != null) {
            String subTableName = jsonObject1.getString("subTableName");
            String subField = jsonObject1.getString("subField");
            String mainTable = jsonObject1.getString("mainTable");
            String mainField = jsonObject1.getString("mainField");
            stringBuilder.append("(").append(mainField).append(" IN (SELECT ").append(subField).append(" FROM ").append(subTableName).append(" WHERE ").append(s);
            appendSymbol(stringBuilder, queryRuleEnum, val, sql, type);
            stringBuilder.append("))");
        } else {
            stringBuilder.append(s);
            appendSymbol(stringBuilder, queryRuleEnum, val, sql, type);
        }

    }

    private static void appendSymbol(StringBuilder stringBuilder, QueryRuleEnum queryRuleEnum, String value, String sql, String dataType) {
        if ("date".equals(dataType) && "ORACLE".equalsIgnoreCase(getDatabseType())) {
            sql = sql.replace("'", "");
            if (sql.length() == 10) {
                sql = toDateyMd(sql);
            } else {
                sql = toDateyMdHms(sql);
            }
        }

        switch (queryRuleEnum) {
            case GT:
                stringBuilder.append(">").append(sql);
                break;
            case GE:
                stringBuilder.append(">=").append(sql);
                break;
            case LT:
                stringBuilder.append("<").append(sql);
                break;
            case LE:
                stringBuilder.append("<=").append(sql);
                break;
            case NE:
                stringBuilder.append("!=").append(sql);
                break;
            case IN:
                stringBuilder.append(" IN (");
                String[] values = value.split(",");

                for (int i = 0; i < values.length; ++i) {
                    String value1 = values[i];
                    if (StringUtils.isNotBlank(value1)) {
                        String s = appendSpace(dataType, value1);
                        stringBuilder.append(s);
                        if (i < values.length - 1) {
                            stringBuilder.append(",");
                        }
                    }
                }

                stringBuilder.append(")");
                break;
            case LIKE:
                stringBuilder.append(" like ").append("N").append("'").append("%").append(value).append("%").append("'");
                break;
            case LEFT_LIKE:
                stringBuilder.append(" like ").append("N").append("'").append("%").append(value).append("'");
                break;
            case RIGHT_LIKE:
                stringBuilder.append(" like ").append("N").append("'").append(value).append("%").append("'");
                break;
            case EQ:
            default:
                stringBuilder.append("=").append(sql);
        }

    }

    private static String appendSpace(String dataType, String value) {
        if (!"int".equals(dataType) && !"number".equals(dataType)) {
            if ("date".equals(dataType)) {
                return "'" + value + "'";
            } else {
                return "SQLSERVER".equals(getDatabseType()) ? "N'" + value + "'" : "'" + value + "'";
            }
        } else {
            return value;
        }
    }

    public static Map<String, Object> getParameterMap(HttpServletRequest request) {
        Map map = request.getParameterMap();
        HashMap hashMap = new HashMap();
        Iterator iterator = map.entrySet().iterator();
        String s = "";
        String s1 = "";

        for (Object object = null; iterator.hasNext(); hashMap.put(s, s1)) {
            Entry entry = (Entry) iterator.next();
            s = (String) entry.getKey();
            object = entry.getValue();
            if (!"_t".equals(s) && null != object) {
                if (!(object instanceof String[])) {
                    s1 = object.toString();
                } else {
                    String[] strings = (String[]) ((String[]) object);

                    for (int i = 0; i < strings.length; ++i) {
                        s1 = strings[i] + ",";
                    }

                    s1 = s1.substring(0, s1.length() - 1);
                }
            } else {
                s1 = "";
            }
        }

        return hashMap;
    }

    public static boolean isExistField(String onlCgformField, List<OnlCgformField> onlCgformFieldList) {
        Iterator iterator = onlCgformFieldList.iterator();

        OnlCgformField onlCgformField1;
        do {
            if (!iterator.hasNext()) {
                return false;
            }

            onlCgformField1 = (OnlCgformField) iterator.next();
        } while (!onlCgformField.equals(onlCgformField1.getDbFieldName()));

        return true;
    }

    public static JSONObject getFiledJson(List<OnlCgformField> onlCgformFieldList, List<String> stringList, FieldModel fieldModel) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList1 = new ArrayList();
        SysBaseApi sysBaseAPI = (SysBaseApi) SpringContextUtils.getBean(SysBaseApi.class);
        OnlCgformHeadMapper onlCgformHeadMapper = (OnlCgformHeadMapper) SpringContextUtils.getBean(OnlCgformHeadMapper.class);
        ArrayList arrayList2 = new ArrayList();
        Iterator iterator = onlCgformFieldList.iterator();

        while (true) {
            OnlCgformField onlCgformField;
            String s;
            do {
                do {
                    if (!iterator.hasNext()) {
                        JSONObject jsonObject;
                        JsonSchemaDescrip jsonSchemaDescrip;
                        if (arrayList.size() > 0) {
                            jsonSchemaDescrip = new JsonSchemaDescrip(arrayList);
                            jsonObject = JsonSchemaUtil.getJsonSchema(jsonSchemaDescrip, arrayList1);
                        } else {
                            jsonSchemaDescrip = new JsonSchemaDescrip();
                            jsonObject = JsonSchemaUtil.getJsonSchema(jsonSchemaDescrip, arrayList1);
                        }

                        return jsonObject;
                    }

                    onlCgformField = (OnlCgformField) iterator.next();
                    s = onlCgformField.getDbFieldName();
                } while ("id".equals(s));
            } while (arrayList2.contains(s));

            String dbFieldTxt = onlCgformField.getDbFieldTxt();
            if ("1".equals(onlCgformField.getFieldMustInput())) {
                arrayList.add(s);
            }

            String fieldShowType = onlCgformField.getFieldShowType();
            AbstractCommonProperty property = null;
            if ("switch".equals(fieldShowType)) {
                property = new SwitchProperty(s, dbFieldTxt, onlCgformField.getFieldExtendJson());
            } else if (isListType(fieldShowType)) {
                Object arrayList3 = new ArrayList();
                if (oConvertUtils.isNotEmpty(onlCgformField.getDictTable())) {
                    arrayList3 = sysBaseAPI.queryTableDictItemsByCode(onlCgformField.getDictTable(), onlCgformField.getDictText(), onlCgformField.getDictField());
                } else if (oConvertUtils.isNotEmpty(onlCgformField.getDictField())) {
                    arrayList3 = sysBaseAPI.queryDictItemsByCode(onlCgformField.getDictField());
                }

                property = new StringProperty(s, dbFieldTxt, fieldShowType, onlCgformField.getDbLength(), (List) arrayList3);
                if (DataTypeUtil.isNumberType(onlCgformField.getDbType())) {
                    property.setType("number");
                }
            } else if (DataTypeUtil.isNumberType(onlCgformField.getDbType())) {
                NumberProperty numberProperty = new NumberProperty(s, dbFieldTxt, "number");
                if (CgformValidPatternEnum.INTEGER.getType().equals(onlCgformField.getFieldValidType())) {
                    numberProperty.setPattern(CgformValidPatternEnum.INTEGER.getPattern());
                }

                property = numberProperty;
            } else {
                String dictField;
                if (!"popup".equals(fieldShowType)) {
                    if ("sel_search".equals(fieldShowType)) {
                        property = new DictProperty(s, dbFieldTxt, onlCgformField.getDictTable(), onlCgformField.getDictField(), onlCgformField.getDictText());
                    } else if ("link_down".equals(fieldShowType)) {
                        LinkDownProperty linkDownProperty = new LinkDownProperty(s, dbFieldTxt, onlCgformField.getDictTable());
                        setColumns((LinkDownProperty) linkDownProperty, (List) onlCgformFieldList, (List) arrayList2);
                        property = linkDownProperty;
                    } else {
                        String dictText;
                        String dictTable;
                        if ("sel_tree".equals(fieldShowType)) {
                            dictText = onlCgformField.getDictText();
                            String[] dictTexts = dictText.split(",");
                            dictTable = onlCgformField.getDictTable() + "," + dictTexts[2] + "," + dictTexts[0];
                            TreeSelectProperty treeSelectProperty = new TreeSelectProperty(s, dbFieldTxt, dictTable, dictTexts[1], onlCgformField.getDictField());
                            if (dictTexts.length > 3) {
                                treeSelectProperty.setHasChildField(dictTexts[3]);
                            }

                            property = treeSelectProperty;
                        } else if ("cat_tree".equals(fieldShowType)) {
                            dictText = onlCgformField.getDictText();
                            dictField = onlCgformField.getDictField();
                            dictTable = "0";
                            if (oConvertUtils.isNotEmpty(dictField) && !"0".equals(dictField)) {
                                dictTable = onlCgformHeadMapper.queryCategoryIdByCode(dictField);
                            }

                            if (oConvertUtils.isEmpty(dictText)) {
                                property = new TreeSelectProperty(s, dbFieldTxt, dictTable);
                            } else {
                                property = new TreeSelectProperty(s, dbFieldTxt, dictTable, dictText);
                                HiddenProperty hiddenProperty = new HiddenProperty(dictText, dictText);
                                arrayList1.add(hiddenProperty);
                            }
                        } else if (fieldModel != null && s.equals(fieldModel.getFieldName())) {
                            dictText = fieldModel.getTableName() + "," + fieldModel.getTextField() + "," + fieldModel.getCodeField();
                            TreeSelectProperty treeSelectProperty = new TreeSelectProperty(s, dbFieldTxt, dictText, fieldModel.getPidField(), fieldModel.getPidValue());
                            treeSelectProperty.setHasChildField(fieldModel.getHsaChildField());
                            treeSelectProperty.setPidComponent(1);
                            property = treeSelectProperty;
                        } else {
                            StringProperty stringProperty = new StringProperty(s, dbFieldTxt, fieldShowType, onlCgformField.getDbLength());
                            if (oConvertUtils.isNotEmpty(onlCgformField.getFieldValidType())) {
                                CgformValidPatternEnum cgformValidPatternEnum = CgformValidPatternEnum.getPatternInfoByType(onlCgformField.getFieldValidType());
                                if (cgformValidPatternEnum != null) {
                                    if (CgformValidPatternEnum.NOTNULL == cgformValidPatternEnum) {
                                        arrayList.add(s);
                                    } else {
                                        stringProperty.setPattern(cgformValidPatternEnum.getPattern());
                                        stringProperty.setErrorInfo(cgformValidPatternEnum.getMsg());
                                    }
                                } else {
                                    stringProperty.setPattern(onlCgformField.getFieldValidType());
                                    stringProperty.setErrorInfo("输入的值不合法");
                                }
                            }

                            property = stringProperty;
                        }
                    }
                } else {
                    PopupProperty popupProperty = new PopupProperty(s, dbFieldTxt, onlCgformField.getDictTable(), onlCgformField.getDictText(), onlCgformField.getDictField());
                    dictField = onlCgformField.getDictText();
                    if (dictField != null && !"".equals(dictField)) {
                        String[] dictFields = dictField.split(",");

                        for (String dictField1 : dictFields) {
                            if (!isExistField(dictField1, onlCgformFieldList)) {
                                HiddenProperty hiddenProperty = new HiddenProperty(dictField1, dictField1);
                                hiddenProperty.setOrder(onlCgformField.getOrderNum());
                                arrayList1.add(hiddenProperty);
                            }
                        }
                    }

                    property = popupProperty;
                }
            }

            if (onlCgformField.getIsReadOnly() == 1 || stringList != null && stringList.contains(s)) {
                property.setDisabled(true);
            }

            property.setOrder(onlCgformField.getOrderNum());
            property.setDefVal(onlCgformField.getFieldDefaultValue());
            arrayList1.add(property);
        }
    }


    public static Set<String> getByShowType(List<OnlCgformField> onlCgformFields) {
        HashSet hashSet = new HashSet();
        Iterator iterator = onlCgformFields.iterator();

        OnlCgformField onlCgformField;
        String s;
        while (iterator.hasNext()) {
            onlCgformField = (OnlCgformField) iterator.next();
            if ("popup".equals(onlCgformField.getFieldShowType())) {
                s = onlCgformField.getDictText();
                if (s != null && !"".equals(s)) {
                    hashSet.addAll(Arrays.stream(s.split(",")).collect(Collectors.toSet()));
                }
            }

            if ("cat_tree".equals(onlCgformField.getFieldShowType())) {
                s = onlCgformField.getDictText();
                if (oConvertUtils.isNotEmpty(s)) {
                    hashSet.add(s);
                }
            }
        }

        iterator = onlCgformFields.iterator();

        while (iterator.hasNext()) {
            onlCgformField = (OnlCgformField) iterator.next();
            s = onlCgformField.getDbFieldName();
            if (onlCgformField.getIsShowForm() == 1) {
                hashSet.remove(s);
            }
        }

        return hashSet;
    }

    public static Map<String, Object> getInsertSql(String tbname, List<OnlCgformField> fieldList, JSONObject jsonObject) {
        ISysService sysService = SpringContextUtils.getBean("sysService", ISysService.class);
        HttpServletRequest request = SpringContextUtils.getHttpServletRequest();
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilder1 = new StringBuilder();
        String s = "";

        try {
            s = TableUtil.getDatabaseType();
        } catch (SQLException | DBException e) {
            e.printStackTrace();
        }

        HashMap hashMap = new HashMap();
        boolean b = false;
        String s1 = null;
        String userId = request.getHeader("userId");
        SysUser loginUser = sysService.getSysUserById(userId);
        if (loginUser == null) {
            throw new OnlineBootException("online保存表单数据异常:系统未找到当前登陆用户信息");
        } else {
            Set set = getByShowType(fieldList);
            Iterator iterator = fieldList.iterator();

            while (true) {
                while (iterator.hasNext()) {
                    OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                    String dbFieldName = onlCgformField.getDbFieldName();
                    if (null == dbFieldName) {
                        log.info("--------online保存表单数据遇见空名称的字段------->>" + onlCgformField.getId());
                    } else if ("id".equals(dbFieldName.toLowerCase())) {
                        b = true;
                        s1 = jsonObject.getString(dbFieldName);
                    } else {
                        appendUserData(onlCgformField, loginUser, jsonObject, "CREATE_BY", "CREATE_TIME", "SYS_ORG_CODE");
                        if ("bpm_status".equals(dbFieldName.toLowerCase())) {
                            stringBuilder.append(",").append(dbFieldName);
                            stringBuilder1.append(",'1'");
                        } else {
                            String s2;
                            if (set.contains(dbFieldName)) {
                                stringBuilder.append(",").append(dbFieldName);
                                s2 = DataTypeUtil.getSql(s, onlCgformField, jsonObject, hashMap);
                                stringBuilder1.append(",").append(s2);
                            } else if (onlCgformField.getIsShowForm() == 1 || !oConvertUtils.isEmpty(onlCgformField.getMainField()) || !oConvertUtils.isEmpty(onlCgformField.getDbDefaultVal())) {
                                if (jsonObject.get(dbFieldName) == null) {
                                    if (oConvertUtils.isEmpty(onlCgformField.getDbDefaultVal())) {
                                        continue;
                                    }

                                    jsonObject.put(dbFieldName, onlCgformField.getDbDefaultVal());
                                }

                                if ("".equals(jsonObject.get(dbFieldName))) {
                                    s2 = onlCgformField.getDbType();
                                    if (DataTypeUtil.isNumberType(s2) || DataTypeUtil.isDateType(s2)) {
                                        continue;
                                    }
                                }

                                stringBuilder.append(",").append(dbFieldName);
                                s2 = DataTypeUtil.getSql(s, onlCgformField, jsonObject, hashMap);
                                stringBuilder1.append(",").append(s2);
                            }
                        }
                    }
                }

                if (b) {
                    if (oConvertUtils.isEmpty(s1)) {
                        s1 = getIdWorkerId();
                    }
                } else {
                    s1 = getIdWorkerId();
                }

                String sql = "insert into " + getSubstring(tbname) + "(" + "id" + stringBuilder.toString() + ") values(" + "'" + s1 + "'" + stringBuilder1.toString() + ")";
                hashMap.put("execute_sql_string", sql);
                log.info("--动态表单保存sql-->" + sql);
                return hashMap;
            }
        }
    }

    public static Map<String, Object> getUpdateSql(String s, List<OnlCgformField> onlCgformFields, JSONObject jsonObject) {
        StringBuilder stringBuilder = new StringBuilder();
        HashMap hashMap = new HashMap();
        String s1 = "";

        try {
            s1 = TableUtil.getDatabaseType();
        } catch (SQLException | DBException e) {
            e.printStackTrace();
        }
        SysUser sysUser = LoginUserUtil.getLoginUser();
        if (sysUser == null) {
            throw new OnlineBootException("online修改表单数据异常:系统未找到当前登陆用户信息");
        } else {
            Set showType = getByShowType(onlCgformFields);
            Iterator iterator = onlCgformFields.iterator();

            while (true) {
                while (iterator.hasNext()) {
                    OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                    String dbFieldName = onlCgformField.getDbFieldName();
                    if (null == dbFieldName) {
                        log.info("--------online修改表单数据遇见空名称的字段------->>" + onlCgformField.getId());
                    } else {
                        appendUserData(onlCgformField, sysUser, jsonObject, "UPDATE_BY", "UPDATE_TIME", "SYS_ORG_CODE");
                        String s2;
                        if (showType.contains(dbFieldName) && jsonObject.get(dbFieldName) != null && !"".equals(jsonObject.getString(dbFieldName))) {
                            s2 = DataTypeUtil.getSql(s1, onlCgformField, jsonObject, hashMap);
                            stringBuilder.append(dbFieldName).append("=").append(s2).append(",");
                        } else if (onlCgformField.getIsShowForm() == 1 && !"id".equals(dbFieldName)) {
                            if ("".equals(jsonObject.get(dbFieldName))) {
                                s2 = onlCgformField.getDbType();
                                if (DataTypeUtil.isNumberType(s2) || DataTypeUtil.isDateType(s2)) {
                                    continue;
                                }
                            }

                            s2 = DataTypeUtil.getSql(s1, onlCgformField, jsonObject, hashMap);
                            stringBuilder.append(dbFieldName).append("=").append(s2).append(",");
                        }
                    }
                }

                String s2 = stringBuilder.toString();
                if (s2.endsWith(",")) {
                    s2 = s2.substring(0, s2.length() - 1);
                }

                String sql = "update " + getSubstring(s) + " set " + s2 + " where 1=1  " + " AND " + "id" + "=" + "'" + jsonObject.getString("id") + "'";
                log.info("--动态表单编辑sql-->" + sql);
                hashMap.put("execute_sql_string", sql);
                return hashMap;
            }
        }
    }

    public static String getSelectSql(String tbname, List<OnlCgformField> onlCgformFields, String id) {
        return getSelectSql(tbname, onlCgformFields, "id", id);
    }

    public static String getSelectSql(String tbname, List<OnlCgformField> onlCgformFields, String field, String id) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT ");
        int size = onlCgformFields.size();
        boolean b = false;

        for (int i = 0; i < size; ++i) {
            String dbFieldName = onlCgformFields.get(i).getDbFieldName();
            if ("id".equals(dbFieldName)) {
                b = true;
            }

            stringBuilder.append(dbFieldName);
            if (size > i + 1) {
                stringBuilder.append(",");
            }
        }

        if (!b) {
            stringBuilder.append(",id");
        }

        stringBuilder.append(" FROM ").append(getSubstring(tbname)).append(" where 1=1  ").append(" AND ").append(field).append("=").append("'").append(id).append("'");
        return stringBuilder.toString();
    }

    public static void appendUserData(OnlCgformField onlCgformField, SysUser loginUser, JSONObject jsonObject, String... strings) {
        String dbFieldName = onlCgformField.getDbFieldName();
        boolean b = false;
        int length = strings.length;

        for (int i = 0; i < length; ++i) {
            String s1 = strings[i];
            if (dbFieldName.toUpperCase().equals(s1)) {
                if (onlCgformField.getIsShowForm() == 1) {
                    if (jsonObject.get(dbFieldName) == null) {
                        b = true;
                    }
                } else {
                    onlCgformField.setIsShowForm(1);
                    b = true;
                }

                if (b) {
                    byte bt = -1;
                    switch (s1.hashCode()) {
                        case -909973894:
                            if ("CREATE_BY".equals(s1)) {
                                bt = 0;
                            }
                            break;
                        case -99751974:
                            if ("SYS_ORG_CODE".equals(s1)) {
                                bt = 4;
                            }
                            break;
                        case 837427085:
                            if ("UPDATE_BY".equals(s1)) {
                                bt = 2;
                            }
                            break;
                        case 1609067651:
                            if ("UPDATE_TIME".equals(s1)) {
                                bt = 3;
                            }
                            break;
                        case 1688939568:
                            if ("CREATE_TIME".equals(s1)) {
                                bt = 1;
                            }
                    }

                    switch (bt) {
                        case 0:
                        case 2:
                            jsonObject.put(dbFieldName, loginUser.getUsername());
                            return;
                        case 1:
                        case 3:
                            onlCgformField.setFieldShowType("datetime");
                            jsonObject.put(dbFieldName, DateUtils.formatDateTime());
                            return;
                        case 4:
                            jsonObject.put(dbFieldName, loginUser.getOrgCode());
                    }
                }
                break;
            }
        }

    }

    public static boolean equals(Object object, Object object1) {
        if (oConvertUtils.isEmpty(object) && oConvertUtils.isEmpty(object1)) {
            return true;
        } else {
            return oConvertUtils.isNotEmpty(object) && object.equals(object1);
        }
    }

    public static boolean fieldEquals(OnlCgformField field, OnlCgformField newField) {
        return !equals(field.getDbFieldName(), newField.getDbFieldName()) || !equals(field.getDbFieldTxt(), newField.getDbFieldTxt()) ||
                !equals(field.getDbLength(), newField.getDbLength()) || !equals(field.getDbPointLength(), newField.getDbPointLength()) ||
                !equals(field.getDbType(), newField.getDbType()) || !equals(field.getDbIsNull(), newField.getDbIsNull()) ||
                !equals(field.getDbIsKey(), newField.getDbIsKey()) || !equals(field.getDbDefaultVal(), newField.getDbDefaultVal());
    }

    public static boolean indexEquals(OnlCgformIndex index, OnlCgformIndex newIndex) {
        return !equals(index.getIndexName(), newIndex.getIndexName()) || !equals(index.getIndexField(), newIndex.getIndexField()) ||
                !equals(index.getIndexType(), newIndex.getIndexType());
    }

    public static boolean onlCgformHeadEquals(OnlCgformHead onlCgformHead, OnlCgformHead newOnlCgformHead) {
        return !equals(onlCgformHead.getTableName(), newOnlCgformHead.getTableName()) || !equals(onlCgformHead.getTableTxt(),
                newOnlCgformHead.getTableTxt());
    }

    public static String getSelectSql(String s, List<OnlCgformField> onlCgformFields, Map<String, Object> map) {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilder1 = new StringBuilder();

        for (OnlCgformField onlCgformField : onlCgformFields) {
            String dbFieldName = onlCgformField.getDbFieldName();
            String dbType = onlCgformField.getDbType();
            if (onlCgformField.getIsShowList() == 1) {
                stringBuilder1.append(",").append(dbFieldName);
            }

            boolean b;
            String s1;
            if (oConvertUtils.isNotEmpty(onlCgformField.getMainField())) {
                b = !DataTypeUtil.isNumberType(dbType);
                s1 = QueryGenerator.getSingleQueryConditionSql(dbFieldName, "", map.get(dbFieldName), b);
                if (!"".equals(s1)) {
                    stringBuilder.append(" AND ").append(s1);
                }
            }

            if (onlCgformField.getIsQuery() == 1) {
                if ("single".equals(onlCgformField.getQueryMode())) {
                    if (map.get(dbFieldName) != null) {
                        b = !DataTypeUtil.isNumberType(dbType);
                        s1 = QueryGenerator.getSingleQueryConditionSql(dbFieldName, "", map.get(dbFieldName), b);
                        if (!"".equals(s1)) {
                            stringBuilder.append(" AND ").append(s1);
                        }
                    }
                } else {
                    Object object = map.get(dbFieldName + "_begin");
                    if (object != null) {
                        stringBuilder.append(" AND ").append(dbFieldName).append(">=");
                        if (DataTypeUtil.isNumberType(dbType)) {
                            stringBuilder.append(object.toString());
                        } else {
                            stringBuilder.append("'").append(object.toString()).append("'");
                        }
                    }

                    Object object1 = map.get(dbFieldName + "_end");
                    if (object1 != null) {
                        stringBuilder.append(" AND ").append(dbFieldName).append("<=");
                        if (DataTypeUtil.isNumberType(dbType)) {
                            stringBuilder.append(object1.toString());
                        } else {
                            stringBuilder.append("'").append(object1.toString()).append("'");
                        }
                    }
                }
            }
        }

        return "SELECT id" + stringBuilder1.toString() + " FROM " + getSubstring(s) + " where 1=1  " + stringBuilder.toString();
    }


    public static boolean isNotClass(OnlCgformEnhanceJava onlCgformEnhanceJava) {
        String cgJavaType = onlCgformEnhanceJava.getCgJavaType();
        String cgJavaValue = onlCgformEnhanceJava.getCgJavaValue();
        if (oConvertUtils.isNotEmpty(cgJavaValue)) {
            try {
                if ("class".equals(cgJavaType)) {
                    Class aClass = Class.forName(cgJavaValue);
                    aClass.newInstance();
                }

                if ("spring".equals(cgJavaType)) {
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return true;
            }
        }

        return false;
    }

    public static void sort(List<String> stringList) {
        Collections.sort(stringList, (s1, s2) -> {
            if (s1 != null && s2 != null) {
                if (s1.compareTo(s2) > 0) {
                    return 1;
                } else if (s1.compareTo(s2) < 0) {
                    return -1;
                } else {
                    return s1.compareTo(s2) == 0 ? 0 : 0;
                }
            } else {
                return -1;
            }
        });
    }


    private static String converRuleValue(String ruleValue, boolean b) {
        return b ? "'" + QueryGenerator.converRuleValue(ruleValue) + "'" : QueryGenerator.converRuleValue(ruleValue);
    }

    private static void rule(String s, SysPermissionDataRuleModel sysPermissionDataRuleModel, String s1, String s2, StringBuffer stringBuffer) {
        QueryRuleEnum queryRuleEnum = QueryRuleEnum.getByValue(sysPermissionDataRuleModel.getRuleConditions());
        boolean b = !DataTypeUtil.isNumberType(s2);
        String converRuleValue = converRuleValue(sysPermissionDataRuleModel.getRuleValue(), b);
        if (converRuleValue != null && queryRuleEnum != null) {
            if ("ORACLE".equalsIgnoreCase(s) && "Date".equals(s2)) {
                converRuleValue = converRuleValue.replace("'", "");
                if (converRuleValue.length() == 10) {
                    converRuleValue = toDateyMd(converRuleValue);
                } else {
                    converRuleValue = toDateyMdHms(converRuleValue);
                }
            }

            switch (queryRuleEnum) {
                case GT:
                    stringBuffer.append(" AND " + s1 + ">" + converRuleValue);
                    break;
                case GE:
                    stringBuffer.append(" AND " + s1 + ">=" + converRuleValue);
                    break;
                case LT:
                    stringBuffer.append(" AND " + s1 + "<" + converRuleValue);
                    break;
                case LE:
                    stringBuffer.append(" AND " + s1 + "<=" + converRuleValue);
                    break;
                case NE:
                    stringBuffer.append(" AND " + s1 + " <> " + converRuleValue);
                    break;
                case IN:
                    stringBuffer.append(" AND " + s1 + " IN " + converRuleValue);
                    break;
                case LIKE:
                    stringBuffer.append(" AND " + s1 + " LIKE '%" + QueryGenerator.trimSingleQuote(converRuleValue) + "%'");
                    break;
                case LEFT_LIKE:
                    stringBuffer.append(" AND " + s1 + " LIKE '%" + QueryGenerator.trimSingleQuote(converRuleValue) + "'");
                    break;
                case RIGHT_LIKE:
                    stringBuffer.append(" AND " + s1 + " LIKE '" + QueryGenerator.trimSingleQuote(converRuleValue) + "%'");
                    break;
                case EQ:
                    stringBuffer.append(" AND " + s1 + "=" + converRuleValue);
                    break;
                default:
                    log.info("--查询规则未匹配到---");
            }

        }
    }

    public static String replaceSql(String sql, JSONObject jsonObject) {
        if (jsonObject == null) {
            return sql;
        } else {
            sql = sql.replace("#{UUID}", UUIDGenerator.generate());
            Set sqlRuleParams = QueryGenerator.getSqlRuleParams(sql);
            Iterator iterator = sqlRuleParams.iterator();

            while (true) {
                while (iterator.hasNext()) {
                    String sqlRuleParam = (String) iterator.next();
                    String s;
                    if (jsonObject.get(sqlRuleParam.toUpperCase()) == null && jsonObject.get(sqlRuleParam.toLowerCase()) == null) {
                        s = QueryGenerator.converRuleValue(sqlRuleParam);
                        sql = sql.replace("#{" + sqlRuleParam + "}", s);
                    } else {
                        s = null;
                        if (jsonObject.containsKey(sqlRuleParam.toLowerCase())) {
                            s = jsonObject.getString(sqlRuleParam.toLowerCase());
                        } else if (jsonObject.containsKey(sqlRuleParam.toUpperCase())) {
                            s = jsonObject.getString(sqlRuleParam.toUpperCase());
                        }

                        sql = sql.replace("#{" + sqlRuleParam + "}", s);
                    }
                }

                return sql;
            }
        }
    }

    public static JSONArray getColumns(List<OnlCgformField> onlCgformFieldList, List<String> stringList) {
        JSONArray jsonArray = new JSONArray();
        SysBaseApi sysBaseApi = (SysBaseApi) SpringContextUtils.getBean(SysBaseApi.class);
        Iterator iterator = onlCgformFieldList.iterator();

        while (true) {
            OnlCgformField onlCgformField;
            String dbFieldName;
            do {
                if (!iterator.hasNext()) {
                    return jsonArray;
                }

                onlCgformField = (OnlCgformField) iterator.next();
                dbFieldName = onlCgformField.getDbFieldName();
            } while ("id".equals(dbFieldName));

            JSONObject jsonObject = new JSONObject();
            if (stringList.indexOf(dbFieldName) >= 0) {
                jsonObject.put("disabled", true);
            }

            jsonObject.put("title", onlCgformField.getDbFieldTxt());
            jsonObject.put("key", dbFieldName);
            jsonObject.put("width", "186px");
            String type = getType(onlCgformField);
            jsonObject.put("type", type);
            if ("file".equals(type) || "image".equals(type)) {
                jsonObject.put("responseName", "message");
                jsonObject.put("token", true);
            }

            if ("switch".equals(type)) {
                jsonObject.put("type", "checkbox");
                JSONArray jsonArray1 = new JSONArray();
                if (oConvertUtils.isEmpty(onlCgformField.getFieldExtendJson())) {
                    jsonArray1.add("Y");
                    jsonArray1.add("N");
                } else {
                    jsonArray1 = JSONArray.parseArray(onlCgformField.getFieldExtendJson());
                }

                jsonObject.put("customValue", jsonArray1);
            }

            if ("popup".equals(type)) {
                jsonObject.put("popupCode", onlCgformField.getDictTable());
                jsonObject.put("orgFields", onlCgformField.getDictField());
                jsonObject.put("destFields", onlCgformField.getDictText());
                String dictText = onlCgformField.getDictText();
                if (dictText != null && !"".equals(dictText)) {
                    ArrayList arrayList = new ArrayList();
                    String[] dictTexts = dictText.split(",");
                    String[] dictTexts1 = dictTexts;
                    int length = dictTexts.length;

                    for (int i = 0; i < length; ++i) {
                        String dictText1 = dictTexts1[i];
                        if (!isExistField(dictText1, onlCgformFieldList)) {
                            arrayList.add(dictText1);
                            JSONObject jsonObject1 = new JSONObject();
                            jsonObject1.put("title", dictText1);
                            jsonObject1.put("key", dictText1);
                            jsonObject1.put("type", "hidden");
                            jsonArray.add(jsonObject1);
                        }
                    }
                }
            }

            jsonObject.put("defaultValue", onlCgformField.getDbDefaultVal());
            jsonObject.put("fieldDefaultValue", onlCgformField.getFieldDefaultValue());
            jsonObject.put("placeholder", "请输入" + onlCgformField.getDbFieldTxt());
            jsonObject.put("validateRules", getValidateRules(onlCgformField));
            if ("list".equals(onlCgformField.getFieldShowType()) || "radio".equals(onlCgformField.getFieldShowType()) || "checkbox_meta".equals(onlCgformField.getFieldShowType()) || "list_multi".equals(onlCgformField.getFieldShowType()) || "sel_search".equals(onlCgformField.getFieldShowType())) {
                Object arrayList = new ArrayList();
                if (oConvertUtils.isNotEmpty(onlCgformField.getDictTable())) {
                    arrayList = sysBaseApi.queryTableDictItemsByCode(onlCgformField.getDictTable(), onlCgformField.getDictText(), onlCgformField.getDictField());
                } else if (oConvertUtils.isNotEmpty(onlCgformField.getDictField())) {
                    arrayList = sysBaseApi.queryDictItemsByCode(onlCgformField.getDictField());
                }

                jsonObject.put("options", arrayList);
                if ("list_multi".equals(onlCgformField.getFieldShowType())) {
                    jsonObject.put("width", "230px");
                }
            }

            jsonArray.add(jsonObject);
        }
    }

    private static JSONArray getValidateRules(OnlCgformField onlCgformField) {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        if (onlCgformField.getDbIsNull() == 0 || "1".equals(onlCgformField.getFieldMustInput())) {
            jsonObject = new JSONObject();
            jsonObject.put("required", true);
            jsonObject.put("message", onlCgformField.getDbFieldTxt() + "不能为空!");
            jsonArray.add(jsonObject);
        }

        if (oConvertUtils.isNotEmpty(onlCgformField.getFieldValidType())) {
            jsonObject = new JSONObject();
            if ("only".equals(onlCgformField.getFieldValidType())) {
                jsonObject.put("pattern", (Object) null);
            } else {
                jsonObject.put("pattern", onlCgformField.getFieldValidType());
            }

            jsonObject.put("message", onlCgformField.getDbFieldTxt() + "格式不正确");
            jsonArray.add(jsonObject);
        }

        return jsonArray;
    }

    public static Map<String, Object> getValueType(Map<String, Object> map) {
        HashMap hashMap = new HashMap();
        if (map != null && !map.isEmpty()) {
            Set set = map.keySet();
            Iterator iterator = set.iterator();

            while (iterator.hasNext()) {
                String s = (String) iterator.next();
                Object object = map.get(s);
                if (object instanceof Clob) {
                    object = clobToString((Clob) object);
                } else if (object instanceof byte[]) {
                    object = new String((byte[]) ((byte[]) object));
                } else if (object instanceof Blob) {
                    try {
                        if (object != null) {
                            Blob blob = (Blob) object;
                            object = new String(blob.getBytes(1L, (int) blob.length()), "UTF-8");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                String lowerCase = s.toLowerCase();
                hashMap.put(lowerCase, object);
            }

            return hashMap;
        } else {
            return hashMap;
        }
    }


    public static List<Map<String, Object>> getMapList(List<Map<String, Object>> mapList) {
        ArrayList arrayList = new ArrayList();
        Iterator iterator = mapList.iterator();

        while (iterator.hasNext()) {
            Map map = (Map) iterator.next();
            HashMap hashMap = new HashMap();
            Set set = map.keySet();
            Iterator iterator1 = set.iterator();

            while (iterator1.hasNext()) {
                String s = (String) iterator1.next();
                Object object = map.get(s);
                if (object instanceof Clob) {
                    object = clobToString((Clob) object);
                } else if (object instanceof byte[]) {
                    object = new String((byte[]) ((byte[]) object));
                } else if (object instanceof Blob) {
                    try {
                        if (object != null) {
                            Blob blob = (Blob) object;
                            object = new String(blob.getBytes(1L, (int) blob.length()), "UTF-8");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                String lowerCase = s.toLowerCase();
                hashMap.put(lowerCase, object);
            }

            arrayList.add(hashMap);
        }

        return arrayList;
    }

    public static String clobToString(Clob clob) {
        String s = "";

        try {
            Reader reader = clob.getCharacterStream();
            char[] lengths = new char[(int) clob.length()];
            reader.read(lengths);
            s = new String(lengths);
            reader.close();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }

        return s;
    }

    public static Map<String, Object> getFMakingInsertSql(String s, List<OnlCgformField> onlCgformFields, JSONObject jsonObject) {

        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilder1 = new StringBuilder();
        String s1 = "";

        try {
            s1 = TableUtil.getDatabaseType();
        } catch (SQLException | DBException e) {
            e.printStackTrace();
        }

        HashMap hashMap = new HashMap();
        boolean b = false;
        String s2 = null;
        SysUser loginUser = LoginUserUtil.getLoginUser();
        if (loginUser == null) {
            throw new OnlineBootException("online保存表单数据异常:系统未找到当前登陆用户信息");
        } else {
            Iterator iterator = onlCgformFields.iterator();

            while (true) {
                while (iterator.hasNext()) {
                    OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                    String dbFieldName = onlCgformField.getDbFieldName();
                    if (null == dbFieldName) {
                        log.info("--------online保存表单数据遇见空名称的字段------->>" + onlCgformField.getId());
                    } else if (jsonObject.get(dbFieldName) != null || "CREATE_BY".equalsIgnoreCase(dbFieldName) || "CREATE_TIME".equalsIgnoreCase(dbFieldName) || "SYS_ORG_CODE".equalsIgnoreCase(dbFieldName)) {
                        appendUserData(onlCgformField, loginUser, jsonObject, "CREATE_BY", "CREATE_TIME", "SYS_ORG_CODE");
                        String s3;
                        if ("".equals(jsonObject.get(dbFieldName))) {
                            s3 = onlCgformField.getDbType();
                            if (DataTypeUtil.isNumberType(s3) || DataTypeUtil.isDateType(s3)) {
                                continue;
                            }
                        }

                        if ("id".equals(dbFieldName.toLowerCase())) {
                            b = true;
                            s2 = jsonObject.getString(dbFieldName);
                        } else {
                            stringBuilder.append("," + dbFieldName);
                            s3 = DataTypeUtil.getSql(s1, onlCgformField, jsonObject, hashMap);
                            stringBuilder1.append("," + s3);
                        }
                    }
                }

                if (!b || oConvertUtils.isEmpty(s2)) {
                    s2 = getIdWorkerId();
                }

                String sql = "insert into " + getSubstring(s) + "(" + "id" + stringBuilder.toString() + ") values(" + "'" + s2 + "'" + stringBuilder1.toString() + ")";
                hashMap.put("execute_sql_string", sql);
                log.info("--表单设计器表单保存sql-->" + sql);
                return hashMap;
            }
        }
    }

    public static Map<String, Object> formDesignerEditingSql(String s, List<OnlCgformField> onlCgformFields, JSONObject jsonObject) {
        StringBuffer stringBuffer = new StringBuffer();
        HashMap hashMap = new HashMap();
        String s1 = "";

        try {
            s1 = TableUtil.getDatabaseType();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (DBException e) {
            e.printStackTrace();
        }
        SysUser sysUser = LoginUserUtil.getLoginUser();
        if (sysUser == null) {
            throw new OnlineBootException("online保存表单数据异常:系统未找到当前登陆用户信息");
        } else {
            Iterator iterator = onlCgformFields.iterator();

            while (true) {
                while (iterator.hasNext()) {
                    OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                    String dbFieldName = onlCgformField.getDbFieldName();
                    if (null == dbFieldName) {
                        log.info("--------online修改表单数据遇见空名称的字段------->>" + onlCgformField.getId());
                    } else if (!"id".equals(dbFieldName) && (jsonObject.get(dbFieldName) != null || "UPDATE_BY".equalsIgnoreCase(dbFieldName) ||
                            "UPDATE_TIME".equalsIgnoreCase(dbFieldName) || "SYS_ORG_CODE".equalsIgnoreCase(dbFieldName))) {
                        appendUserData(onlCgformField, sysUser, jsonObject, "UPDATE_BY", "UPDATE_TIME", "SYS_ORG_CODE");
                        String s2;
                        if ("".equals(jsonObject.get(dbFieldName))) {
                            s2 = onlCgformField.getDbType();
                            if (DataTypeUtil.isNumberType(s2) || DataTypeUtil.isDateType(s2)) {
                                continue;
                            }
                        }

                        s2 = DataTypeUtil.getSql(s1, onlCgformField, jsonObject, hashMap);
                        stringBuffer.append(dbFieldName).append("=").append(s2).append(",");
                    }
                }

                String s2 = stringBuffer.toString();
                if (s2.endsWith(",")) {
                    s2 = s2.substring(0, s2.length() - 1);
                }

                String sql = "update " + getSubstring(s) + " set " + s2 + " where 1=1  " + " AND " + "id" + "=" + "'" + jsonObject.getString("id") + "'";
                log.info("--表单设计器表单编辑sql-->" + sql);
                hashMap.put("execute_sql_string", sql);
                return hashMap;
            }
        }
    }

    public static Map<String, Object> getUpdateSql(String tableName, String filed, String id) {
        HashMap hashMap = new HashMap();
        String sql = "update " + getSubstring(tableName) + " set " + filed + "=" + "'" + 0 + "'" + " where 1=1  " + " AND " + "id" + "=" + "'" + id + "'";
        log.info("--修改树节点状态：为无子节点sql-->" + sql);
        hashMap.put("execute_sql_string", sql);
        return hashMap;
    }

    public static String getCODELike(String dictField) {
        return dictField != null && !"".equals(dictField) && !"0".equals(dictField) ? "CODE like '" + dictField + "%" + "'" : "";
    }

    public static String getSubstring(String s) {
        return Pattern.matches("^[a-zA-z].*\\$\\d+$", s) ? s.substring(0, s.lastIndexOf("$")) : s;
    }

    public static void setColumns(LinkDownProperty linkDownProperty, List<OnlCgformField> onlCgformFieldList, List<String> stringList) {
        String dictTable = linkDownProperty.getDictTable();
        JSONObject jsonObject = JSONObject.parseObject(dictTable);
        String linkField = jsonObject.getString("linkField");
        ArrayList arrayList = new ArrayList();
        if (oConvertUtils.isNotEmpty(linkField)) {
            String[] linkFields = linkField.split(",");
            Iterator iterator = onlCgformFieldList.iterator();

            label26:
            while (true) {
                while (true) {
                    if (!iterator.hasNext()) {
                        break label26;
                    }

                    OnlCgformField onlCgformField = (OnlCgformField) iterator.next();
                    String dbFieldName = onlCgformField.getDbFieldName();

                    for (String linkField1 : linkFields) {
                        if (linkField1.equals(dbFieldName)) {
                            stringList.add(dbFieldName);
                            arrayList.add(new BaseColumn(onlCgformField.getDbFieldTxt(), dbFieldName));
                            break;
                        }
                    }
                }
            }
        }

        linkDownProperty.setOtherColumns(arrayList);
    }

    public static String uploadOnlineImage(byte[] data, String basePath, String bizPath, String uploadType) {
        return CommonUtils.uploadOnlineImage(data, basePath, bizPath, uploadType);
    }

    public static List<String> getImageType(List<OnlCgformField> onlCgformFields) {
        ArrayList arrayList = new ArrayList();

        for (OnlCgformField onlCgformField : onlCgformFields) {
            if ("image".equals(onlCgformField.getFieldShowType())) {
                arrayList.add(onlCgformField.getDbFieldTxt());
            }
        }

        return arrayList;
    }

    public static List<String> getImageTypeText(List<OnlCgformField> onlCgformFields, String text) {
        ArrayList arrayList = new ArrayList();

        for (OnlCgformField onlCgformField : onlCgformFields) {
            if ("image".equals(onlCgformField.getFieldShowType())) {
                arrayList.add(text + "_" + onlCgformField.getDbFieldTxt());
            }
        }

        return arrayList;
    }

    public static String getIdWorkerId() {
        long id = IdWorker.getId();
        return String.valueOf(id);
    }

    public static String getMessage(Exception e) {
        String msg = e.getCause() != null ? e.getCause().getMessage() : e.getMessage();
        if (msg.contains("ORA-01452")) {
            msg = "ORA-01452: 无法 CREATE UNIQUE INDEX; 找到重复的关键字";
        } else if (msg.contains("duplicate key")) {
            msg = "无法 CREATE UNIQUE INDEX; 找到重复的关键字";
        }

        return msg;
    }

    private static String getType(OnlCgformField onlCgformField) {
        if ("checkbox".equals(onlCgformField.getFieldShowType())) {
            return "checkbox";
        } else if ("list".equals(onlCgformField.getFieldShowType())) {
            return "select";
        } else if ("switch".equals(onlCgformField.getFieldShowType())) {
            return "switch";
        } else if (!"image".equals(onlCgformField.getFieldShowType()) && !"file".equals(onlCgformField.getFieldShowType()) && !"radio".equals(onlCgformField.getFieldShowType()) && !"popup".equals(onlCgformField.getFieldShowType()) && !"list_multi".equals(onlCgformField.getFieldShowType()) && !"sel_search".equals(onlCgformField.getFieldShowType())) {
            if ("datetime".equals(onlCgformField.getFieldShowType())) {
                return "datetime";
            } else if ("date".equals(onlCgformField.getFieldShowType())) {
                return "date";
            } else if ("int".equals(onlCgformField.getDbType())) {
                return "inputNumber";
            } else {
                return !"double".equals(onlCgformField.getDbType()) && !"BigDecimal".equals(onlCgformField.getDbType()) ? "input" : "inputNumber";
            }
        } else {
            return onlCgformField.getFieldShowType();
        }
    }

    private static String getDatabseType() {
        if (oConvertUtils.isNotEmpty(au)) {
            return au;
        } else {
            try {
                SysBaseApi sysBaseAPI = (SysBaseApi) ApplicationContextUtil.getContext().getBean(SysBaseApi.class);
                au = sysBaseAPI.getDatabaseType();
                return au;
            } catch (Exception e) {
                e.printStackTrace();
                return au;
            }
        }
    }
}
