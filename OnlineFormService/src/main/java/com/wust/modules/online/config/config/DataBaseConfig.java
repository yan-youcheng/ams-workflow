package com.wust.modules.online.config.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author wanheng
 */
@Component("dataBaseConfig")
@ConfigurationProperties(
        prefix = "spring.datasource.dynamic.datasource.master"
)
@Data
public class DataBaseConfig {

    private String url;
    private String username;
    private String password;
    private String driverClassName;

}
