package com.wust.modules.online.config.config;

import com.wust.modules.online.cgform.entity.OnlCgformField;
import com.wust.modules.online.cgform.entity.OnlCgformIndex;
import lombok.Data;

import java.util.List;

/**
 * @author wanheng
 */
@Data
public class TableConfig {

    private String tableName;

    private String isDbSynch;

    private String content;

    private String jFormVersion;

    private Integer jFormType;

    private String jFormPkType;

    private String jFormPkSequence;

    private Integer relationType;

    private String subTableStr;

    private Integer tabOrder;

    private List<OnlCgformField> columns;

    private List<OnlCgformIndex> indexes;

    private String treeParentIdFieldName;

    private String treeIdFieldName;

    private String treeFieldName;

    private DataBaseConfig dataBaseConfig;

}
