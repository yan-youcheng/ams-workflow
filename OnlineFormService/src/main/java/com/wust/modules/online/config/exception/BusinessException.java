package com.wust.modules.online.config.exception;

/**
 * @author wanheng
 */
public class BusinessException extends Exception {

    public BusinessException(String msg) {
        super(msg);
    }
}
