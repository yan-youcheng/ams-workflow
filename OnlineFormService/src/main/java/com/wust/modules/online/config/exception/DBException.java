package com.wust.modules.online.config.exception;

/**
 * @author wanheng
 */
public class DBException extends Exception {

    public DBException(String msg) {
        super(msg);
    }
}
