package com.wust.modules.online.config.service;

import com.wust.modules.online.config.exception.DBException;
import com.wust.modules.online.config.util.ColumnMeta;

/**
 * @author wanheng
 */
public interface DbTableHandle {

  /**
   * 获取增加字段sql
   * @param columnMeta
   * @return
   */
  String getAddColumnSql(ColumnMeta columnMeta);

  /**
   * 获取重命名字段sql
   * @param columnMeta
   * @return
   */
  String getReNameFieldName(ColumnMeta columnMeta);

  /**
   * 获取更新字段sql
   * @param columnMeta1
   * @param columnMeta2
   * @return
   * @throws DBException
   */
  String getUpdateColumnSql(ColumnMeta columnMeta1, ColumnMeta columnMeta2) throws DBException;

  /**
   * 通过数据类型获取匹配规则
   * @param dataType
   * @param digits
   * @return
   */
  String getMatchClassTypeByDataType(String dataType, int digits);

  /**
   * 获取删表语句
   * @param tableName
   * @return
   */
  String dropTableSql(String tableName);

  /**
   * 获取删除字段语句
   * @param fieldName
   * @return
   */
  String getDropColumnSql(String fieldName);

  /**
   * 获取评论sql
   * @param columnMeta
   * @return
   */
  String getCommentSql(ColumnMeta columnMeta);

  /**
   * 特别处理
   * @param columnMeta1
   * @param columnMeta2
   * @return
   */
  String getSpecialHandle(ColumnMeta columnMeta1, ColumnMeta columnMeta2);

  /**
   * 获取删除索引sql
   * @param indexName
   * @param tableName
   * @return
   */
  String dropIndexs(String indexName, String tableName);

  /**
   * 获取统计所有数量
   * @param indexName
   * @param tableName
   * @return
   */
  String countIndex(String indexName, String tableName);
}
