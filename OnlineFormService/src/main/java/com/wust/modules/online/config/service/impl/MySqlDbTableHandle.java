package com.wust.modules.online.config.service.impl;

import com.wust.modules.online.config.service.DbTableHandle;
import com.wust.modules.online.config.util.ColumnMeta;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;

/**
 * @author wanheng
 */
@NoArgsConstructor
public class MySqlDbTableHandle implements DbTableHandle {


    @Override
    public String getAddColumnSql(ColumnMeta columnMeta) {
        return " ADD COLUMN " + this.getAdColumnSql(columnMeta) + ";";
    }

    @Override
    public String getReNameFieldName(ColumnMeta columnMeta) {
        return "CHANGE COLUMN " + columnMeta.getOldColumnName() + " " + this.getChangeColumn(columnMeta) + " ;";
    }

    @Override
    public String getUpdateColumnSql(ColumnMeta cgformcolumnMeta, ColumnMeta datacolumnMeta) {
        return " MODIFY COLUMN " + this.getModifyColumn(cgformcolumnMeta, datacolumnMeta) + ";";
    }

    @Override
    public String getMatchClassTypeByDataType(String dataType, int digits) {
        String sql = "";
        if ("varchar".equalsIgnoreCase(dataType)) {
            sql = "string";
        } else if ("double".equalsIgnoreCase(dataType)) {
            sql = "double";
        } else if ("int".equalsIgnoreCase(dataType)) {
            sql = "int";
        } else if ("Date".equalsIgnoreCase(dataType)) {
            sql = "date";
        } else if ("Datetime".equalsIgnoreCase(dataType)) {
            sql = "date";
        } else if ("decimal".equalsIgnoreCase(dataType)) {
            sql = "bigdecimal";
        } else if ("text".equalsIgnoreCase(dataType)) {
            sql = "text";
        } else if ("blob".equalsIgnoreCase(dataType)) {
            sql = "blob";
        }

        return sql;
    }

    @Override
    public String dropTableSql(String tableName) {
        return " DROP TABLE IF EXISTS " + tableName + " ;";
    }

    @Override
    public String getDropColumnSql(String fieldName) {
        return " DROP COLUMN " + fieldName + ";";
    }

    private String getCreateSql(ColumnMeta columnMeta1, ColumnMeta columnMeta2) {
        String sql = "";
        if ("string".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " varchar(" + columnMeta1.getColumnSize() + ") " + ("Y".equals(columnMeta1.getIsNullable()) ? "NULL" : "NOT NULL");
        } else if ("date".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " datetime " + ("Y".equals(columnMeta1.getIsNullable()) ? "NULL" : "NOT NULL");
        } else if ("int".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " int(" + columnMeta1.getColumnSize() + ") " + ("Y".equals(columnMeta1.getIsNullable()) ? "NULL" : "NOT NULL");
        } else if ("double".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " double(" + columnMeta1.getColumnSize() + "," + columnMeta1.getDecimalDigits() + ") " + ("Y".equals(columnMeta1.getIsNullable()) ? "NULL" : "NOT NULL");
        } else if ("bigdecimal".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " decimal(" + columnMeta1.getColumnSize() + "," + columnMeta1.getDecimalDigits() + ") " + ("Y".equals(columnMeta1.getIsNullable()) ? "NULL" : "NOT NULL");
        } else if ("text".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " text " + ("Y".equals(columnMeta1.getIsNullable()) ? "NULL" : "NOT NULL");
        } else if ("blob".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " blob " + ("Y".equals(columnMeta1.getIsNullable()) ? "NULL" : "NOT NULL");
        }

        sql = sql + (StringUtils.isNotEmpty(columnMeta1.getComment()) ? " COMMENT '" + columnMeta1.getComment() + "'" : " ");
        sql = sql + (StringUtils.isNotEmpty(columnMeta1.getFieldDefault()) ? " DEFAULT " + columnMeta1.getFieldDefault() : " ");
        String pkType = columnMeta1.getPkType();
        if ("id".equalsIgnoreCase(columnMeta1.getColumnName()) && pkType != null &&
                ("SEQUENCE".equalsIgnoreCase(pkType) || "NATIVE".equalsIgnoreCase(pkType))) {
            sql = sql + " AUTO_INCREMENT ";
        }

        return sql;
    }

    private String getModifyColumn(ColumnMeta columnMeta1, ColumnMeta columnMeta2) {
        String sql = this.getCreateSql(columnMeta1, columnMeta2);
        return sql;
    }

    private String getAdColumnSql(ColumnMeta columnMeta) {
        String sql = this.getCreateSql(columnMeta, (ColumnMeta)null);
        return sql;
    }

    private String getChangeColumn(ColumnMeta columnMeta) {
        String sql = this.getCreateSql(columnMeta, (ColumnMeta)null);
        return sql;
}

    @Override
    public String getCommentSql(ColumnMeta columnMeta) {
        return "";
    }

    @Override
    public String getSpecialHandle(ColumnMeta cgformcolumnMeta, ColumnMeta datacolumnMeta) {
        return null;
    }

    @Override
    public String dropIndexs(String indexName, String tableName) {
        return "DROP INDEX " + indexName + " ON " + tableName;
    }

    @Override
    public String countIndex(String indexName, String tableName) {
        return "select COUNT(*) from information_schema.statistics where table_name = '" + tableName + "'  AND index_name = '" + indexName + "'";
    }
}
