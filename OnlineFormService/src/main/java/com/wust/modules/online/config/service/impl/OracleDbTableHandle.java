package com.wust.modules.online.config.service.impl;

import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;
import com.wust.modules.online.config.service.DbTableHandle;
import com.wust.modules.online.config.util.ColumnMeta;

/**
 * @author wanheng
 */
@NoArgsConstructor
public class OracleDbTableHandle implements DbTableHandle {

    @Override
    public String getAddColumnSql(ColumnMeta columnMeta) {
        return " ADD  " + this.getCreateSql(columnMeta) + "";
    }

    @Override
    public String getReNameFieldName(ColumnMeta columnMeta) {
        return "RENAME COLUMN  " + columnMeta.getOldColumnName() + " TO " + columnMeta.getColumnName() + "";
    }

    @Override
    public String getUpdateColumnSql(ColumnMeta cgformcolumnMeta, ColumnMeta datacolumnMeta) {
        return " MODIFY   " + this.getCreateSqla(cgformcolumnMeta, datacolumnMeta) + "";
    }

    @Override
    public String getMatchClassTypeByDataType(String dataType, int digits) {
        String sql = "";
        if ("varchar2".equalsIgnoreCase(dataType)) {
            sql = "string";
        }

        if ("nvarchar2".equalsIgnoreCase(dataType)) {
            sql = "string";
        } else if ("double".equalsIgnoreCase(dataType)) {
            sql = "double";
        } else if ("number".equalsIgnoreCase(dataType) && digits == 0) {
            sql = "int";
        } else if ("number".equalsIgnoreCase(dataType) && digits != 0) {
            sql = "double";
        } else if ("int".equalsIgnoreCase(dataType)) {
            sql = "int";
        } else if ("Date".equalsIgnoreCase(dataType)) {
            sql = "date";
        } else if ("Datetime".equalsIgnoreCase(dataType)) {
            sql = "date";
        } else if ("blob".equalsIgnoreCase(dataType)) {
            sql = "blob";
        } else if ("clob".equalsIgnoreCase(dataType)) {
            sql = "text";
        }

        return sql;
    }

    @Override
    public String dropTableSql(String tableName) {
        return " DROP TABLE  " + tableName.toLowerCase() + " ";
    }

    @Override
    public String getDropColumnSql(String fieldName) {
        return " DROP COLUMN " + fieldName.toUpperCase() + "";
    }

    private String getCreateSql(ColumnMeta columnMeta) {
        String sql = "";
        if ("string".equalsIgnoreCase(columnMeta.getColunmType())) {
            sql = columnMeta.getColumnName() + " varchar2(" + columnMeta.getColumnSize() + ")";
        } else if ("date".equalsIgnoreCase(columnMeta.getColunmType())) {
            sql = columnMeta.getColumnName() + " date";
        } else if ("int".equalsIgnoreCase(columnMeta.getColunmType())) {
            sql = columnMeta.getColumnName() + " NUMBER(" + columnMeta.getColumnSize() + ")";
        } else if ("double".equalsIgnoreCase(columnMeta.getColunmType())) {
            sql = columnMeta.getColumnName() + " NUMBER(" + columnMeta.getColumnSize() + "," + columnMeta.getDecimalDigits() + ")";
        } else if ("bigdecimal".equalsIgnoreCase(columnMeta.getColunmType())) {
            sql = columnMeta.getColumnName() + " NUMBER(" + columnMeta.getColumnSize() + "," + columnMeta.getDecimalDigits() + ")";
        } else if ("text".equalsIgnoreCase(columnMeta.getColunmType())) {
            sql = columnMeta.getColumnName() + " CLOB ";
        } else if ("blob".equalsIgnoreCase(columnMeta.getColunmType())) {
            sql = columnMeta.getColumnName() + " BLOB ";
        }

        sql = sql + (StringUtils.isNotEmpty(columnMeta.getFieldDefault()) ? " DEFAULT " + columnMeta.getFieldDefault() : " ");
        sql = sql + ("Y".equals(columnMeta.getIsNullable()) ? " NULL" : " NOT NULL");
        return sql;
    }

    private String getCreateSqla(ColumnMeta columnMeta1, ColumnMeta columnMeta2) {
        String sql = "";
        String s1 = "";
        if (!columnMeta2.getIsNullable().equals(columnMeta1.getIsNullable())) {
            s1 = "Y".equals(columnMeta1.getIsNullable()) ? "NULL" : "NOT NULL";
        }

        if ("string".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " varchar2(" + columnMeta1.getColumnSize() + ")" + s1;
        } else if ("date".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " date " + s1;
        } else if ("int".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " NUMBER(" + columnMeta1.getColumnSize() + ") " + s1;
        } else if ("double".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " NUMBER(" + columnMeta1.getColumnSize() + "," + columnMeta1.getDecimalDigits() + ") " + s1;
        } else if ("bigdecimal".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " NUMBER(" + columnMeta1.getColumnSize() + "," + columnMeta1.getDecimalDigits() + ") " + s1;
        } else if ("blob".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " BLOB " + s1;
        } else if ("text".equalsIgnoreCase(columnMeta1.getColunmType())) {
            sql = columnMeta1.getColumnName() + " CLOB " + s1;
        }

        sql = sql + (StringUtils.isNotEmpty(columnMeta1.getFieldDefault()) ? " DEFAULT " + columnMeta1.getFieldDefault() : " ");
        sql = sql + s1;
        return sql;
    }

    @Override
    public String getCommentSql(ColumnMeta columnMeta) {
        return "COMMENT ON COLUMN " + columnMeta.getTableName() + "." + columnMeta.getColumnName() + " IS '" + columnMeta.getComment() + "'";
    }

    @Override
    public String getSpecialHandle(ColumnMeta cgformcolumnMeta, ColumnMeta datacolumnMeta) {
        return null;
    }

    @Override
    public String dropIndexs(String indexName, String tableName) {
        return "DROP INDEX " + indexName;
    }

    @Override
    public String countIndex(String indexName, String tableName) {
        return "select count(*) from user_ind_columns where index_name=upper('" + indexName + "')";
    }
}
