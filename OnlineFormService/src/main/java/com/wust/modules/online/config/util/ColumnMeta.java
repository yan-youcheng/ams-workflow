package com.wust.modules.online.config.util;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;

/**
 * @author wanheng
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class ColumnMeta {

    private String tableName;
    private String columnId;
    private String columnName;
    private int columnSize;
    private String colunmType;
    private String comment;
    private String fieldDefault;
    private int decimalDigits;
    private String isNullable;
    private String pkType;
    private String oldColumnName;




    public boolean isEqual(Object object, String s) {
        if (object == this) {
            return true;
        } else if (!(object instanceof ColumnMeta)) {
            return false;
        } else {
            ColumnMeta columnMeta = (ColumnMeta) object;
            if ("SQLSERVER".equals(s)) {
                if (!this.colunmType.contains("date") && !this.colunmType.contains("blob") && !this.colunmType.contains("text")) {
                    return this.colunmType.equals(columnMeta.getColunmType()) && this.isNullable.equals(columnMeta.isNullable) && this.columnSize == columnMeta.getColumnSize();
                } else {
                    return this.columnName.equals(columnMeta.getColumnName()) && this.isNullable.equals(columnMeta.isNullable);
                }
            } else if ("POSTGRESQL".equals(s)) {
                if (!this.colunmType.contains("date") && !this.colunmType.contains("blob") && !this.colunmType.contains("text")) {
                    return this.colunmType.equals(columnMeta.getColunmType()) && this.isNullable.equals(columnMeta.isNullable) && this.columnSize == columnMeta.getColumnSize();
                } else {
                    return this.columnName.equals(columnMeta.getColumnName()) && this.isNullable.equals(columnMeta.isNullable);
                }
            } else if ("ORACLE".equals(s)) {
                if (!this.colunmType.contains("date") && !this.colunmType.contains("blob") && !this.colunmType.contains("text")) {
                    return this.colunmType.equals(columnMeta.getColunmType()) && this.isNullable.equals(columnMeta.isNullable) && this.columnSize == columnMeta.getColumnSize();
                } else {
                    return this.columnName.equals(columnMeta.getColumnName()) && this.isNullable.equals(columnMeta.isNullable);
                }
            } else if (!this.colunmType.contains("date") && !this.colunmType.contains("blob") && !this.colunmType.contains("text")) {
                return this.colunmType.equals(columnMeta.getColunmType()) && this.isNullable.equals(columnMeta.isNullable) && this.columnSize == columnMeta.getColumnSize() && this.isEqual(this.comment, columnMeta.getComment()) && this.isEqual(this.fieldDefault, columnMeta.getFieldDefault());
            } else {
                return this.colunmType.equals(columnMeta.getColunmType()) && this.columnName.equals(columnMeta.getColumnName()) && this.isNullable.equals(columnMeta.isNullable) && this.isEqual(this.comment, columnMeta.getComment()) && this.isEqual(this.fieldDefault, columnMeta.getFieldDefault());
            }
        }
    }

    public boolean isEqual(ColumnMeta columnMeta) {
        return columnMeta != this && !this.isEqual(this.comment, columnMeta.getComment());
    }

    private boolean isEqual(String s1, String s2) {
        boolean b1 = StringUtils.isNotEmpty(s1);
        boolean b2 = StringUtils.isNotEmpty(s2);
        if (b1 != b2) {
            return false;
        } else {
            return !b1 || s1.equals(s2);
        }
    }
}
