package com.wust.modules.online.config.util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.StringWriter;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

/**
 * @author wanheng
 */
@Slf4j
public class FreemarkerHelper {

    private static final Configuration CONFIGURATION;

    public FreemarkerHelper() {
    }

    public static String setEcode(String s, String s1, Map<String, Object> map) {
        try {
            StringWriter stringWriter = new StringWriter();
            Template template;
            template = CONFIGURATION.getTemplate(s, s1);
            template.process(map, stringWriter);
            return stringWriter.toString();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return e.toString();
        }
    }

    public static String setUtf8(String s, Map<String, Object> map) {
        return setEcode(s, "utf-8", map);
    }


    static {
        CONFIGURATION = new Configuration(Configuration.VERSION_2_3_28);
        CONFIGURATION.setNumberFormat("0.#####################");
        CONFIGURATION.setClassForTemplateLoading(FreemarkerHelper.class, "/");
    }
}
