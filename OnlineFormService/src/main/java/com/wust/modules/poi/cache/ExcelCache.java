package com.wust.modules.poi.cache;

import com.wust.modules.poi.cache.manager.PoiCacheManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * @author wanheng
 */
@Slf4j
public final class ExcelCache {

    public static Workbook getWorkbook(String url, Integer[] sheetNums, boolean needAll) {
        InputStream is = null;
        List sheetList = Arrays.asList(sheetNums);

        try {
            is = PoiCacheManager.getFile(url);
            assert is != null;
            Workbook wb = WorkbookFactory.create(is);
            if (!needAll) {
                for(int i = wb.getNumberOfSheets() - 1; i >= 0; --i) {
                    if (!sheetList.contains(i)) {
                        wb.removeSheetAt(i);
                    }
                }
            }

            return wb;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                assert is != null;
                is.close();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }

        }

        return null;
    }
}
