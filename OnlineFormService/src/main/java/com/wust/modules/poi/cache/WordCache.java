package com.wust.modules.poi.cache;

import java.io.InputStream;

import com.wust.modules.poi.cache.manager.PoiCacheManager;
import com.wust.modules.poi.word.entity.MyXWPFDocument;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wanheng
 */
@Slf4j
public class WordCache {
    
    public static MyXWPFDocument getXWPFDocumen(String url) {
        InputStream is = null;

        try {
            is = PoiCacheManager.getFile(url);
            return new MyXWPFDocument(is);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                is.close();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }

        }

        return null;
    }
}
