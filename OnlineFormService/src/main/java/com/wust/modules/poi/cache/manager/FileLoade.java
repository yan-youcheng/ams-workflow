package com.wust.modules.poi.cache.manager;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.wust.modules.poi.util.PoiPublicUtil;
import lombok.extern.slf4j.Slf4j;


/**
 * @author wanheng
 */
@Slf4j
class FileLoade {
    
    public byte[] getFile(String url) {
        FileInputStream fileInputStream = null;
        ByteArrayOutputStream byteArrayOutputStream = null;

        try {
            try {
                fileInputStream = new FileInputStream(url);
            } catch (FileNotFoundException e) {
                String path = PoiPublicUtil.getWebRootPath(url);
                fileInputStream = new FileInputStream(path);
            }

            byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];

            int len;
            while((len = fileInputStream.read(buffer)) > -1) {
                byteArrayOutputStream.write(buffer, 0, len);
            }

            byteArrayOutputStream.flush();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }

                if (fileInputStream != null) {
                    assert byteArrayOutputStream != null;
                    byteArrayOutputStream.close();
                }
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }

        }

        log.error(fileInputStream + "这个路径文件没有找到,请查询");
        return null;
    }
}
