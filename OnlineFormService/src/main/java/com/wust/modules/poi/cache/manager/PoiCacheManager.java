package com.wust.modules.poi.cache.manager;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;

/**
 * @author wanheng
 */
@Slf4j
public final class PoiCacheManager {

    private static final LoadingCache<String, byte[]> LOADING_CACHE;

    public static InputStream getFile(String id) {
        try {
            byte[] result = Arrays.copyOf(LOADING_CACHE.get(id), LOADING_CACHE.get(id).length);
            return new ByteArrayInputStream(result);
        } catch (ExecutionException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    static {
        LOADING_CACHE = CacheBuilder.newBuilder().expireAfterWrite(7L, TimeUnit.DAYS).maximumSize(50L).build(new CacheLoader<String, byte[]>() {
            @Override
            public byte[] load(String url) {
                return (new FileLoade()).getFile(url);
            }
        });
    }
}
