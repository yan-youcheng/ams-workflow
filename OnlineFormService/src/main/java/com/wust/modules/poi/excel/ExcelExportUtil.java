package com.wust.modules.poi.excel;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.wust.modules.poi.excel.entity.ExportParams;
import com.wust.modules.poi.excel.entity.TemplateExportParams;
import com.wust.modules.poi.excel.entity.enmus.ExcelType;
import com.wust.modules.poi.excel.entity.params.ExcelExportEntity;
import com.wust.modules.poi.excel.export.ExcelExportServer;
import com.wust.modules.poi.excel.export.template.ExcelExportOfTemplateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @author wanheng
 */
public final class ExcelExportUtil {

    public static Workbook exportExcel(ExportParams entity, Class<?> pojoClass, Collection<?> dataSet, String[] exportFields) {
        Workbook workbook;
        if (ExcelType.HSSF.equals(entity.getType())) {
            workbook = new HSSFWorkbook();
        } else if (dataSet.size() < 1000) {
            workbook = new XSSFWorkbook();
        } else {
            workbook = new SXSSFWorkbook();
        }

        (new ExcelExportServer()).createSheet(workbook, entity, pojoClass, dataSet, exportFields);
        return workbook;
    }


    public static Workbook exportExcel(ExportParams entity, List<ExcelExportEntity> entityList, Collection<? extends Map<?, ?>> dataSet) {
        Object workbook;
        if (ExcelType.HSSF.equals(entity.getType())) {
            workbook = new HSSFWorkbook();
        } else if (dataSet.size() < 1000) {
            workbook = new XSSFWorkbook();
        } else {
            workbook = new SXSSFWorkbook();
        }

        (new ExcelExportServer()).createSheetForMap((Workbook)workbook, entity, entityList, dataSet);
        return (Workbook)workbook;
    }


    public static Workbook exportExcel(TemplateExportParams params, Class<?> pojoClass, Collection<?> dataSet, Map<String, Object> map) {
        return (new ExcelExportOfTemplateUtil()).createExcleByTemplate(params, pojoClass, dataSet, map);
    }

}
