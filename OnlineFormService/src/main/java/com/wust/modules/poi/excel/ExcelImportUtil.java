package com.wust.modules.poi.excel;

import com.wust.modules.poi.excel.entity.ImportParams;
import com.wust.modules.poi.excel.entity.result.ExcelImportResult;
import com.wust.modules.poi.excel.imports.ExcelImportServer;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author wanheng
 */
@Slf4j
public final class ExcelImportUtil {

    private ExcelImportUtil() {
    }

    public static <T> List<T> importExcel(File file, Class<?> pojoClass, ImportParams params) {
        FileInputStream in = null;
        List result = null;

        try {
            in = new FileInputStream(file);
            result = (new ExcelImportServer()).importExcelByIs(in, pojoClass, params).getList();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                assert in != null;
                in.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }

        return result;
    }

    public static <T> List<T> importExcel(InputStream inputstream, Class<?> pojoClass, ImportParams params) throws Exception {
        return (new ExcelImportServer()).importExcelByIs(inputstream, pojoClass, params).getList();
    }
}
