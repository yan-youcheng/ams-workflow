package com.wust.modules.poi.excel.entity;

import com.wust.modules.poi.handler.inter.IExcelDataHandler;
import lombok.Data;

/**
 * @author wanheng
 */
@Data
public class ExcelBaseParams {

    private IExcelDataHandler dataHanlder;

}
