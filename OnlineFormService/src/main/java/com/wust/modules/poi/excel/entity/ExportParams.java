package com.wust.modules.poi.excel.entity;


import com.wust.modules.poi.excel.entity.enmus.ExcelType;
import com.wust.modules.poi.excel.export.styler.ExcelExportStylerDefaultImpl;
import lombok.Data;

/**
 * @author wanheng
 */
@Data
public class ExportParams extends ExcelBaseParams {

    private String title;
    private short titleHeight = 10;
    private String secondTitle;
    private short secondTitleHeight = 8;
    private String sheetName;
    private String[] exclusions;
    private boolean addIndex;
    private String indexName = "序号";
    private int freezeCol;
    private short color = 9;
    private short headerColor = 40;
    private ExcelType type;
    private Class<?> style;
    private boolean isCreateHeadRows;
    private String imageBasePath;

    public ExportParams() {
        this.type = ExcelType.HSSF;
        this.style = ExcelExportStylerDefaultImpl.class;
        this.isCreateHeadRows = true;
    }

    public ExportParams(String title, String sheetName) {
        this.type = ExcelType.HSSF;
        this.style = ExcelExportStylerDefaultImpl.class;
        this.isCreateHeadRows = true;
        this.title = title;
        this.sheetName = sheetName;
    }

    public ExportParams(String title, String sheetName, ExcelType type) {
        this.type = ExcelType.HSSF;
        this.style = ExcelExportStylerDefaultImpl.class;
        this.isCreateHeadRows = true;
        this.title = title;
        this.sheetName = sheetName;
        this.type = type;
    }

    public ExportParams(String title, String secondTitle, String sheetName) {
        this.type = ExcelType.HSSF;
        this.style = ExcelExportStylerDefaultImpl.class;
        this.isCreateHeadRows = true;
        this.title = title;
        this.secondTitle = secondTitle;
        this.sheetName = sheetName;
    }

}
