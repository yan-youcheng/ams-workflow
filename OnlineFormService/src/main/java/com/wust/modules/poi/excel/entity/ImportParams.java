package com.wust.modules.poi.excel.entity;

import com.wust.modules.poi.handler.inter.IExcelVerifyHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author wanheng
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ImportParams extends ExcelBaseParams {
    private int titleRows = 0;
    private int headRows = 1;
    private int startRows = 0;
    private int keyIndex = 0;
    private int sheetNum = 1;
    private boolean needSave = false;
    private String saveUrl = "upload/excelUpload";
    private IExcelVerifyHandler verifyHanlder;
    private int lastOfInvalidRow = 0;
    private List<String> ignoreHeaderList;
    private List<String> imageList;

    public boolean isIgnoreHeader(String text) {
        return this.ignoreHeaderList != null && this.ignoreHeaderList.contains(text);
    }
}
