package com.wust.modules.poi.excel.entity;

import com.wust.modules.poi.excel.export.styler.ExcelExportStylerDefaultImpl;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wanheng
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TemplateExportParams extends ExcelBaseParams {
    private boolean scanAllsheet = false;
    private String templateUrl;
    private Integer[] sheetNum = new Integer[]{0};
    private String[] sheetName;
    private int headingRows = 1;
    private int headingStartRow = 1;
    private int dataSheetNum = 0;
    private Class<?> style = ExcelExportStylerDefaultImpl.class;
    private String tempParams = "t";

}
