package com.wust.modules.poi.excel.entity.enmus;

/**
 * @author wanheng
 */

public enum CellValueType {

    /**
     * 数据类型
     */
    String,
    Number,
    Boolean,
    Date,
    TElement,
    Null,
    None;

}
