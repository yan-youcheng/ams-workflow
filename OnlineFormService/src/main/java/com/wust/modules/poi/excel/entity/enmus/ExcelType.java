package com.wust.modules.poi.excel.entity.enmus;


/**
 * @author wanheng
 */
public enum ExcelType {

    /**
     * Excel类型
     */
    HSSF,
    XSSF;

}
