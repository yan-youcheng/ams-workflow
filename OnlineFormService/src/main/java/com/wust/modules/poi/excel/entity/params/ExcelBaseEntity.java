package com.wust.modules.poi.excel.entity.params;

import lombok.Data;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author wanheng
 */
@Data
public class ExcelBaseEntity {

    protected String name;
    private int type = 1;
    private String databaseFormat;
    private String format;
    private String numFormat;
    private String[] replace;
    private boolean multiReplace;
    private String groupName;
    private Method method;
    private List<Method> methods;

}
