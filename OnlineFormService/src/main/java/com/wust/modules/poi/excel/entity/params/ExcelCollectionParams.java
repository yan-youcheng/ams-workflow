package com.wust.modules.poi.excel.entity.params;


import lombok.Data;

import java.util.Map;

/**
 * @author wanheng
 */
@Data
public class ExcelCollectionParams {

    private String name;
    private String excelName;
    private Class<?> type;
    private Map<String, ExcelImportEntity> excelParams;

}
