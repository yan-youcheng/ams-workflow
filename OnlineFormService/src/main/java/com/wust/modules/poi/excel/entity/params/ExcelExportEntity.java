package com.wust.modules.poi.excel.entity.params;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author wanheng
 */
@Data
@NoArgsConstructor
public class ExcelExportEntity extends ExcelBaseEntity implements Comparable<ExcelExportEntity> {
    private Object key;
    private double width = 10.0D;
    private double height = 10.0D;
    private int exportImageType = 3;
    private String imageBasePath;
    private int orderNum = 0;
    private boolean isWrap;
    private boolean needMerge;
    private boolean mergeVertical;
    private int[] mergeRely;
    private String suffix;
    private boolean isStatistics;
    private boolean colspan;
    private List<String> subColumnList;
    private String groupName;
    private List<ExcelExportEntity> list;

    public ExcelExportEntity(String name) {
        super.name = name;
    }

    public ExcelExportEntity(String name, Object key) {
        super.name = name;
        this.key = key;
    }

    public boolean isSubColumn() {
        return this.colspan && (this.subColumnList == null || this.subColumnList.size() == 0);
    }

    public boolean isMergeColumn() {
        return this.colspan && this.subColumnList != null && this.subColumnList.size() > 0;
    }

    public List<ExcelExportEntity> initSubExportEntity(List<ExcelExportEntity> all) {
        List<ExcelExportEntity> sub = new ArrayList();
        Iterator iterator = all.iterator();

        while(iterator.hasNext()) {
            ExcelExportEntity temp = (ExcelExportEntity) iterator.next();
            if (this.subColumnList.contains(temp.getKey())) {
                sub.add(temp);
            }
        }

        this.setList(sub);
        return sub;
    }

    @Override
    public int compareTo(ExcelExportEntity prev) {
        return this.getOrderNum() - prev.getOrderNum();
    }
}
