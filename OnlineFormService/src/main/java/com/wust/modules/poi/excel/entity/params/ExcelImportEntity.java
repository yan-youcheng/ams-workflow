package com.wust.modules.poi.excel.entity.params;

import lombok.Data;

import java.util.List;

/**
 * @author wanheng
 */
@Data
public class ExcelImportEntity extends ExcelBaseEntity {

    private String collectionName;
    private String saveUrl;
    private int saveType;
    private String classType;
    private ExcelVerifyEntity verify;
    private String suffix;
    private List<ExcelImportEntity> list;

}
