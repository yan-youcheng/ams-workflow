package com.wust.modules.poi.excel.entity.params;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.poi.ss.usermodel.CellStyle;

/**
 * @author wanheng
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ExcelTemplateParams implements Serializable {

    private static final long serialVersionUID = 1L;
    private String name;
    private CellStyle cellStyle;
    private short height;

}
