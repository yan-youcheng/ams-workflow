package com.wust.modules.poi.excel.entity.params;

import lombok.Data;

/**
 * @author wanheng
 */
@Data
public class ExcelVerifyEntity {

    private boolean interHandler;
    private boolean notNull;
    private boolean isMobile;
    private boolean isTel;
    private boolean isEmail;
    private int minLength;
    private int maxLength;
    private String regex;
    private String regexTip;
}
