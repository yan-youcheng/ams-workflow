package com.wust.modules.poi.excel.entity.params;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author wanheng
 */
@Data
@AllArgsConstructor
public class MergeEntity {

    private int startRow;
    private int endRow;
    private String text;
    private List<String> relyList;

    public MergeEntity(String text, int startRow, int endRow) {
        this.text = text;
        this.endRow = endRow;
        this.startRow = startRow;
    }


}
