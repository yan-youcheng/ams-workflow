package com.wust.modules.poi.excel.entity.result;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * @author wanheng
 */
@Data
@AllArgsConstructor
public class ExcelImportResult<T> {

    private List<T> list;
    private boolean verfiyFail;
    private Workbook workbook;
}
