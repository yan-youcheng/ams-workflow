package com.wust.modules.poi.excel.entity.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wanheng
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExcelVerifyHandlerResult {

    private boolean success;
    private String msg;

    public ExcelVerifyHandlerResult(boolean success) {
        this.success = success;
    }


}
