package com.wust.modules.poi.excel.entity.sax;

import com.wust.modules.poi.excel.entity.enmus.CellValueType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * @author wanheng
 */
@Data
@AllArgsConstructor
@ToString
public class SaxReadCellEntity {

    private CellValueType cellType;
    private Object value;

}
