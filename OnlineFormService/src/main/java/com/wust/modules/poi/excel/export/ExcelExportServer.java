package com.wust.modules.poi.excel.export;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.wust.modules.poi.excel.annotation.ExcelTarget;
import com.wust.modules.poi.excel.entity.ExportParams;
import com.wust.modules.poi.excel.entity.enmus.ExcelType;
import com.wust.modules.poi.excel.entity.params.ExcelExportEntity;
import com.wust.modules.poi.excel.export.base.AbstractExcelExportBase;
import com.wust.modules.poi.excel.export.styler.IExcelExportStyler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import com.wust.modules.poi.exception.excel.ExcelExportException;
import com.wust.modules.poi.exception.excel.enums.ExcelExportEnum;
import com.wust.modules.poi.util.PoiPublicUtil;

@Slf4j
public class ExcelExportServer extends AbstractExcelExportBase {
    
    private int maxNum = 60000;

    private int createHeaderAndTitle(ExportParams entity, Sheet sheet, Workbook workbook, List<ExcelExportEntity> excelParams) {
        int rows = 0;
        int feildWidth = this.getFieldWidth(excelParams);
        if (entity.getTitle() != null) {
            rows += this.createHeaderRow(entity, sheet, workbook, feildWidth);
        }

        rows += this.createTitleRow(entity, sheet, workbook, rows, excelParams);
        sheet.createFreezePane(0, rows, 0, rows);
        return rows;
    }

    public int createHeaderRow(ExportParams entity, Sheet sheet, Workbook workbook, int feildWidth) {
        Row row = sheet.createRow(0);
        row.setHeight(entity.getTitleHeight());
        this.createStringCell(row, 0, entity.getTitle(), this.getExcelExportStyler().getHeaderStyle(entity.getHeaderColor()), null);

        for(int i = 1; i <= feildWidth; ++i) {
            this.createStringCell(row, i, "", this.getExcelExportStyler().getHeaderStyle(entity.getHeaderColor()), null);
        }

        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, feildWidth));
        if (entity.getSecondTitle() == null) {
            return 1;
        } else {
            row = sheet.createRow(1);
            row.setHeight(entity.getSecondTitleHeight());
            CellStyle style = workbook.createCellStyle();
            style.setAlignment((short)3);
            this.createStringCell(row, 0, entity.getSecondTitle(), style, (ExcelExportEntity)null);

            for(int i = 1; i <= feildWidth; ++i) {
                this.createStringCell(row, i, "", this.getExcelExportStyler().getHeaderStyle(entity.getHeaderColor()), (ExcelExportEntity)null);
            }

            sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, feildWidth));
            return 2;
        }
    }

    public void createSheet(Workbook workbook, ExportParams entity, Class<?> pojoClass, Collection<?> dataSet, String[] exportFields) {
        if (log.isDebugEnabled()) {
            log.debug("Excel export start ,class is {}", pojoClass);
            log.debug("Excel version is {}", entity.getType().equals(ExcelType.HSSF) ? "03" : "07");
        }

        if (workbook != null && entity != null && pojoClass != null && dataSet != null) {
            super.type = entity.getType();
            if (this.type.equals(ExcelType.XSSF)) {
                this.maxNum = 1000000;
            }

            Sheet sheet = null;

            try {
                sheet = workbook.createSheet(entity.getSheetName());
            } catch (Exception e) {
                sheet = workbook.createSheet();
            }

            try {
                this.dataHanlder = entity.getDataHanlder();
                if (this.dataHanlder != null) {
                    this.needHanlderList = Arrays.asList(this.dataHanlder.getNeedHandlerFields());
                }

                this.setExcelExportStyler((IExcelExportStyler)entity.getStyle().getConstructor(Workbook.class).newInstance(workbook));
                Drawing patriarch = sheet.createDrawingPatriarch();
                List<ExcelExportEntity> excelParams = new ArrayList();
                if (entity.isAddIndex()) {
                    excelParams.add(this.indexExcelEntity(entity));
                }

                Field[] fileds = PoiPublicUtil.getClassFields(pojoClass);
                if (exportFields != null) {
                    List<Field> list = new ArrayList(Arrays.asList(fileds));

                    for(int i = 0; i < list.size(); ++i) {
                        if (!Arrays.asList(exportFields).contains(((Field)list.get(i)).getName())) {
                            list.remove(i);
                            --i;
                        }
                    }

                    if (list != null && list.size() > 0) {
                        fileds = (Field[])list.toArray(new Field[0]);
                    } else {
                        fileds = null;
                    }
                }

                ExcelTarget etarget = (ExcelTarget)pojoClass.getAnnotation(ExcelTarget.class);
                String targetId = etarget == null ? null : etarget.value();
                this.getAllExcelField(entity.getExclusions(), targetId, fileds, excelParams, pojoClass, (List)null);
                this.reConfigExcelExportParams(excelParams, entity);
                int index = entity.isCreateHeadRows() ? this.createHeaderAndTitle(entity, sheet, workbook, excelParams) : 0;
                this.setCellWith(excelParams, sheet);
                short rowHeight = this.getRowHeight(excelParams);
                this.setCurrentIndex(1);
                Iterator<?> its = dataSet.iterator();
                ArrayList tempList = new ArrayList();

                while(its.hasNext()) {
                    Object t = its.next();
                    index += this.createCells(patriarch, index, t, excelParams, sheet, workbook, rowHeight);
                    tempList.add(t);
                    if (index >= this.maxNum) {
                        break;
                    }
                }

                this.mergeCells(sheet, excelParams, index);
                if (entity.getFreezeCol() != 0) {
                    sheet.createFreezePane(entity.getFreezeCol(), 0, entity.getFreezeCol(), 0);
                }

                its = dataSet.iterator();
                int i = 0;

                for(int le = tempList.size(); i < le; ++i) {
                    its.next();
                    its.remove();
                }

                this.addStatisticsRow(this.getExcelExportStyler().getStyles(true, (ExcelExportEntity)null), sheet);
                if (dataSet.size() > 0) {
                    this.createSheet(workbook, entity, pojoClass, dataSet, exportFields);
                }

            } catch (Exception e) {
                log.error(e.getMessage(), e);
                throw new ExcelExportException(ExcelExportEnum.EXPORT_ERROR, e.getCause());
            }
        } else {
            throw new ExcelExportException(ExcelExportEnum.PARAMETER_ERROR);
        }
    }

    public void createSheetForMap(Workbook workbook, ExportParams entity, List<ExcelExportEntity> entityList, Collection<? extends Map<?, ?>> dataSet) {
        if (log.isDebugEnabled()) {
            log.debug("Excel version is {}", entity.getType().equals(ExcelType.HSSF) ? "03" : "07");
        }

        if (workbook != null && entity != null && entityList != null && dataSet != null) {
            super.type = entity.getType();
            if (this.type.equals(ExcelType.XSSF)) {
                this.maxNum = 1000000;
            }

            Sheet sheet = null;

            try {
                sheet = workbook.createSheet(entity.getSheetName());
            } catch (Exception e) {
                sheet = workbook.createSheet();
            }

            try {
                this.dataHanlder = entity.getDataHanlder();
                if (this.dataHanlder != null) {
                    this.needHanlderList = Arrays.asList(this.dataHanlder.getNeedHandlerFields());
                }

                this.setExcelExportStyler((IExcelExportStyler)entity.getStyle().getConstructor(Workbook.class).newInstance(workbook));
                Drawing patriarch = sheet.createDrawingPatriarch();
                List<ExcelExportEntity> excelParams = new ArrayList();
                if (entity.isAddIndex()) {
                    excelParams.add(this.indexExcelEntity(entity));
                }

                excelParams.addAll(entityList);
                this.sortAllParams(excelParams);
                int index = entity.isCreateHeadRows() ? this.createHeaderAndTitle(entity, sheet, workbook, excelParams) : 0;
                this.setCellWith(excelParams, sheet);
                short rowHeight = this.getRowHeight(excelParams);
                this.setCurrentIndex(1);
                Iterator<?> its = dataSet.iterator();
                ArrayList tempList = new ArrayList();

                while(its.hasNext()) {
                    Object t = its.next();
                    index += this.createCells(patriarch, index, t, excelParams, sheet, workbook, rowHeight);
                    tempList.add(t);
                    if (index >= this.maxNum) {
                        break;
                    }
                }

                if (entity.getFreezeCol() != 0) {
                    sheet.createFreezePane(entity.getFreezeCol(), 0, entity.getFreezeCol(), 0);
                }

                this.mergeCells(sheet, excelParams, index);
                its = dataSet.iterator();
                int i = 0;

                for(int le = tempList.size(); i < le; ++i) {
                    its.next();
                    its.remove();
                }

                if (dataSet.size() > 0) {
                    this.createSheetForMap(workbook, entity, entityList, dataSet);
                }

            } catch (Exception e) {
                log.error(e.getMessage(), e);
                throw new ExcelExportException(ExcelExportEnum.EXPORT_ERROR, e.getCause());
            }
        } else {
            throw new ExcelExportException(ExcelExportEnum.PARAMETER_ERROR);
        }
    }

    private int createTitleRow(ExportParams title, Sheet sheet, Workbook workbook, int index, List<ExcelExportEntity> excelParams) {
        Row row = sheet.createRow(index);
        int rows = this.getRowNums(excelParams);
        row.setHeight((short)450);
        Row listRow = null;
        if (rows == 2) {
            listRow = sheet.createRow(index + 1);
            listRow.setHeight((short)450);
        }

        int cellIndex = 0;
        CellStyle titleStyle = this.getExcelExportStyler().getTitleStyle(title.getColor());
        int i = 0;

        for(int exportFieldTitleSize = excelParams.size(); i < exportFieldTitleSize; ++i) {
            ExcelExportEntity entity = (ExcelExportEntity)excelParams.get(i);
            List sTitel;
            if (entity.isColspan()) {
                sTitel = entity.getSubColumnList();
                if (sTitel == null || sTitel.size() == 0) {
                    continue;
                }

                entity.initSubExportEntity(excelParams);
            }

            if (StringUtils.isNotBlank(entity.getName())) {
                this.createStringCell(row, cellIndex, entity.getName(), titleStyle, entity);
            }

            if (entity.getList() != null) {
                sTitel = entity.getList();
                if (StringUtils.isNotBlank(entity.getName())) {
                    sheet.addMergedRegion(new CellRangeAddress(index, index, cellIndex, cellIndex + sTitel.size() - 1));
                }

                int j = 0;

                for(int size = sTitel.size(); j < size; ++j) {
                    this.createStringCell(rows == 2 ? listRow : row, cellIndex, ((ExcelExportEntity)sTitel.get(j)).getName(), titleStyle, entity);
                    ++cellIndex;
                }

                --cellIndex;
            } else if (rows == 2) {
                this.createStringCell(listRow, cellIndex, "", titleStyle, entity);
                sheet.addMergedRegion(new CellRangeAddress(index, index + 1, cellIndex, cellIndex));
            }

            ++cellIndex;
        }

        return rows;
    }

    private int getRowNums(List<ExcelExportEntity> excelParams) {
        for(int i = 0; i < excelParams.size(); ++i) {
            ExcelExportEntity temp = (ExcelExportEntity)excelParams.get(i);
            if ((temp.getList() != null || temp.isColspan()) && StringUtils.isNotBlank(temp.getName())) {
                return 2;
            }
        }

        return 1;
    }

    private ExcelExportEntity indexExcelEntity(ExportParams entity) {
        ExcelExportEntity exportEntity = new ExcelExportEntity();
        exportEntity.setOrderNum(0);
        exportEntity.setName(entity.getIndexName());
        exportEntity.setWidth(10.0D);
        exportEntity.setFormat("isAddIndex");
        return exportEntity;
    }
}
