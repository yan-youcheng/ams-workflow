package com.wust.modules.poi.excel.export.styler;

import com.wust.modules.poi.excel.entity.params.ExcelExportEntity;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * @author wanheng
 */
public abstract class AbstractExcelExportStyler implements IExcelExportStyler {

    protected CellStyle stringNoneStyle;
    protected CellStyle stringNoneWrapStyle;
    protected CellStyle stringSeptailStyle;
    protected CellStyle stringSeptailWrapStyle;
    protected Workbook workbook;
    protected static final short STRING_FORMAT = (short)BuiltinFormats.getBuiltinFormat("TEXT");

    protected void createStyles(Workbook workbook) {
        this.stringNoneStyle = this.stringNoneStyle(workbook, false);
        this.stringNoneWrapStyle = this.stringNoneStyle(workbook, true);
        this.stringSeptailStyle = this.stringSeptailStyle(workbook, false);
        this.stringSeptailWrapStyle = this.stringSeptailStyle(workbook, true);
        this.workbook = workbook;
    }

    @Override
    public CellStyle getStyles(boolean noneStyler, ExcelExportEntity entity) {
        if (!noneStyler || entity != null && !entity.isWrap()) {
            if (noneStyler) {
                return this.stringNoneStyle;
            } else {
                return noneStyler || entity != null && !entity.isWrap() ? this.stringSeptailStyle : this.stringSeptailWrapStyle;
            }
        } else {
            return this.stringNoneWrapStyle;
        }
    }

    public CellStyle stringNoneStyle(Workbook workbook, boolean isWarp) {
        return null;
    }

    public CellStyle stringSeptailStyle(Workbook workbook, boolean isWarp) {
        return null;
    }
}
