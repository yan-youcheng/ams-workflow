package com.wust.modules.poi.excel.export.styler;

import com.wust.modules.poi.excel.entity.params.ExcelExportEntity;
import org.apache.poi.ss.usermodel.CellStyle;

/**
 * @author wanheng
 */
public interface IExcelExportStyler {

    /**
     * 获取头部样式
     * @param color
     * @return
     */
    CellStyle getHeaderStyle(short color);

    /**
     * 获取标题样式
     * @param color
     * @return
     */
    CellStyle getTitleStyle(short color);

    /**
     * 获取样式
     * @param noneStyler
     * @param entity
     * @return
     */
    CellStyle getStyles(boolean noneStyler, ExcelExportEntity entity);
}
