package com.wust.modules.poi.excel.imports;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.wust.modules.core.util.ApplicationContextUtil;
import com.wust.modules.poi.excel.annotation.ExcelTarget;
import com.wust.modules.poi.excel.entity.ImportParams;
import com.wust.modules.poi.excel.entity.params.ExcelCollectionParams;
import com.wust.modules.poi.excel.entity.params.ExcelImportEntity;
import com.wust.modules.poi.excel.entity.result.ExcelImportResult;
import com.wust.modules.poi.excel.entity.result.ExcelVerifyHandlerResult;
import com.wust.modules.poi.excel.imports.base.ImportBaseService;
import com.wust.modules.poi.excel.imports.base.ImportFileServiceI;
import com.wust.modules.poi.excel.imports.verify.VerifyHandlerServer;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.wust.modules.poi.exception.excel.ExcelImportException;
import com.wust.modules.poi.exception.excel.enums.ExcelImportEnum;
import com.wust.modules.poi.util.ExcelUtil;
import com.wust.modules.poi.util.PoiPublicUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author wanheng
 */
public class ExcelImportServer extends ImportBaseService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelImportServer.class);
    private final CellValueServer cellValueServer = new CellValueServer();
    private final VerifyHandlerServer verifyHandlerServer = new VerifyHandlerServer();
    private boolean verfiyFail = false;
    private CellStyle errorCellStyle;

    public ExcelImportServer() {
    }

    private void addListContinue(Object object, ExcelCollectionParams param, Row row, Map<Integer, String> titlemap, String targetId, Map<String, PictureData> pictures, ImportParams params) throws Exception {
        Collection collection = (Collection) PoiPublicUtil.getMethod(param.getName(), object.getClass()).invoke(object);
        Object entity = PoiPublicUtil.createObject(param.getType(), targetId);
        boolean isUsed = false;

        for(int i = row.getFirstCellNum(); i < row.getLastCellNum(); ++i) {
            Cell cell = row.getCell(i);
            String titleString = (String)titlemap.get(i);
            if (param.getExcelParams().containsKey(titleString)) {
                if (((ExcelImportEntity)param.getExcelParams().get(titleString)).getType() == 2) {
                    String picId = row.getRowNum() + "_" + i;
                    this.saveImage(object, picId, param.getExcelParams(), titleString, pictures, params);
                } else {
                    this.saveFieldValue(params, entity, cell, param.getExcelParams(), titleString, row);
                }

                isUsed = true;
            }
        }

        if (isUsed) {
            collection.add(entity);
        }

    }

    private String getKeyValue(Cell cell) {
        if (cell == null) {
            return null;
        } else {
            Object obj = null;
            switch(cell.getCellType()) {
                case 0:
                    obj = cell.getNumericCellValue();
                    break;
                case 1:
                    obj = cell.getStringCellValue();
                    break;
                case 2:
                    obj = cell.getCellFormula();
                    break;
                case 3:
                    break;
                case 4:
                    obj = cell.getBooleanCellValue();
                    break;
                default:
            }

            return obj == null ? null : obj.toString().trim();
        }
    }

    private String getSaveUrl(ExcelImportEntity excelImportEntity, Object object) throws Exception {
        String url;
        if ("upload".equals(excelImportEntity.getSaveUrl())) {
            if (excelImportEntity.getMethods() != null && excelImportEntity.getMethods().size() > 0) {
                object = this.getFieldBySomeMethod(excelImportEntity.getMethods(), object);
            }

            url = object.getClass().getName().split("\\.")[object.getClass().getName().split("\\.").length - 1];
            return excelImportEntity.getSaveUrl() + "/" + url.substring(0, url.lastIndexOf("Entity"));
        } else {
            return excelImportEntity.getSaveUrl();
        }
    }

    private <T> List<T> importExcel(Sheet sheet, Class<?> pojoClass, ImportParams params, Map<String, PictureData> pictures) throws Exception {
        List collection = new ArrayList();
        Map<String, ExcelImportEntity> excelParams = new HashMap();
        List<ExcelCollectionParams> excelCollection = new ArrayList();
        String targetId = null;
        if (!Map.class.equals(pojoClass)) {
            Field[] fileds = PoiPublicUtil.getClassFields(pojoClass);
            ExcelTarget etarget = pojoClass.getAnnotation(ExcelTarget.class);
            if (etarget != null) {
                targetId = etarget.value();
            }

            this.getAllExcelField(targetId, fileds, excelParams, excelCollection, pojoClass, (List)null);
        }

        this.ignoreHeaderHandler(excelParams, params);
        Iterator<Row> rows = sheet.rowIterator();
        Map<Integer, String> titlemap = this.getTitleMap(sheet, params);
        Set<Integer> columnIndexSet = titlemap.keySet();
        Integer maxColumnIndex = Collections.max(columnIndexSet);
        Integer minColumnIndex = Collections.min(columnIndexSet);
        Row row = null;

        for(int j = 0; j < params.getTitleRows() + params.getHeadRows(); ++j) {
            row = rows.next();
        }

        Object object = null;

        while(rows.hasNext() && (row == null || sheet.getLastRowNum() - row.getRowNum() > params.getLastOfInvalidRow())) {
            row = rows.next();
            Cell keyIndexCell = row.getCell(params.getKeyIndex());
            if (excelCollection.size() > 0 && StringUtils.isEmpty(this.getKeyValue(keyIndexCell)) && object != null && !Map.class.equals(pojoClass)) {

                for (ExcelCollectionParams param : excelCollection) {
                    this.addListContinue(object, param, row, titlemap, targetId, pictures, params);
                }
            } else {
                object = PoiPublicUtil.createObject(pojoClass, targetId);

                try {
                    int firstCellNum = row.getFirstCellNum();
                    if (firstCellNum > minColumnIndex) {
                        firstCellNum = minColumnIndex;
                    }

                    int lastCellNum = row.getLastCellNum();
                    if (lastCellNum < maxColumnIndex + 1) {
                        lastCellNum = maxColumnIndex + 1;
                    }

                    int i = firstCellNum;

                    for(int le = lastCellNum; i < le; ++i) {
                        Cell cell = row.getCell(i);
                        String titleString = titlemap.get(i);
                        if (excelParams.containsKey(titleString) || Map.class.equals(pojoClass)) {
                            String picId;
                            if (excelParams.get(titleString) != null && excelParams.get(titleString).getType() == 2) {
                                picId = row.getRowNum() + "_" + i;
                                this.saveImage(object, picId, excelParams, titleString, pictures, params);
                            } else if (params.getImageList() != null && params.getImageList().contains(titleString)) {
                                if (pictures != null) {
                                    picId = row.getRowNum() + "_" + i;
                                    PictureData image = pictures.get(picId);
                                    if (image != null) {
                                        byte[] data = image.getData();
                                        params.getDataHanlder().setMapValue((Map)object, titleString, data);
                                    }
                                }
                            } else {
                                this.saveFieldValue(params, object, cell, excelParams, titleString, row);
                            }
                        }
                    }

                    for (ExcelCollectionParams param : excelCollection) {
                        this.addListContinue(object, param, row, titlemap, targetId, pictures, params);
                    }

                    collection.add(object);
                } catch (ExcelImportException e) {
                    if (!e.getType().equals(ExcelImportEnum.VERIFY_ERROR)) {
                        throw new ExcelImportException(e.getType(), e);
                    }
                }
            }
        }

        return collection;
    }

    private void ignoreHeaderHandler(Map<String, ExcelImportEntity> excelParams, ImportParams params) {
        List<String> ignoreList = new ArrayList();

        for (String key : excelParams.keySet()) {
            String temp = excelParams.get(key).getGroupName();
            if (temp != null && temp.length() > 0) {
                ignoreList.add(temp);
            }
        }

        params.setIgnoreHeaderList(ignoreList);
    }

    private Map<Integer, String> getTitleMap(Sheet sheet, ImportParams params) throws Exception {
        Map<Integer, String> titlemap = new HashMap();
        Iterator<Cell> cellTitle;
        String collectionName;
        Row headRow = null;
        int headBegin = params.getTitleRows();

        for(int allRowNum = sheet.getPhysicalNumberOfRows(); headRow == null && headBegin < allRowNum; headRow = sheet.getRow(headBegin++)) {
        }

        if (headRow == null) {
            throw new Exception("不识别该文件");
        } else {
            if (ExcelUtil.isMergedRegion(sheet, headRow.getRowNum(), 0)) {
                params.setHeadRows(2);
            } else {
                params.setHeadRows(1);
            }

            cellTitle = headRow.cellIterator();

            while(cellTitle.hasNext()) {
                Cell cell = cellTitle.next();
                String value = this.getKeyValue(cell);
                if (StringUtils.isNotEmpty(value)) {
                    titlemap.put(cell.getColumnIndex(), value);
                }
            }

            for(int j = headBegin; j < headBegin + params.getHeadRows() - 1; ++j) {
                headRow = sheet.getRow(j);
                cellTitle = headRow.cellIterator();

                while(cellTitle.hasNext()) {
                    Cell cell = cellTitle.next();
                    String value = this.getKeyValue(cell);
                    if (StringUtils.isNotEmpty(value)) {
                        int columnIndex = cell.getColumnIndex();
                        if (ExcelUtil.isMergedRegion(sheet, cell.getRowIndex() - 1, columnIndex)) {
                            collectionName = ExcelUtil.getMergedRegionValue(sheet, cell.getRowIndex() - 1, columnIndex);
                            if (params.isIgnoreHeader(collectionName)) {
                                titlemap.put(cell.getColumnIndex(), value);
                            } else {
                                titlemap.put(cell.getColumnIndex(), collectionName + "_" + value);
                            }
                        } else {
                            titlemap.put(cell.getColumnIndex(), value);
                        }
                    }
                }
            }

            return titlemap;
        }
    }


    public ExcelImportResult importExcelByIs(InputStream inputstream, Class<?> pojoClass, ImportParams params) throws Exception {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Excel import start ,class is {}", pojoClass);
        }

        List<T> result = new ArrayList();
        Workbook book = null;
        boolean isXSSFWorkbook = true;
        if (!inputstream.markSupported()) {
            inputstream = new PushbackInputStream(inputstream, 8);
        }

        if (POIFSFileSystem.hasPOIFSHeader(inputstream)) {
            book = new HSSFWorkbook(inputstream);
            isXSSFWorkbook = false;
        } else if (POIXMLDocument.hasOOXMLHeader(inputstream)) {
            book = new XSSFWorkbook(OPCPackage.open(inputstream));
        }

        this.createErrorCellStyle(book);

        for(int i = 0; i < params.getSheetNum(); ++i) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(" start to read excel by is ,startTime is {}", (new Date()).getTime());
            }

            Map pictures;
            if (isXSSFWorkbook) {
                pictures = PoiPublicUtil.getSheetPictrues07((XSSFSheet)((Workbook)book).getSheetAt(i), (XSSFWorkbook)book);
            } else {
                pictures = PoiPublicUtil.getSheetPictrues03((HSSFSheet)((Workbook)book).getSheetAt(i), (HSSFWorkbook)book);
            }

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(" end to read excel by is ,endTime is {}", (new Date()).getTime());
            }

            result.addAll(this.importExcel(((Workbook)book).getSheetAt(i), pojoClass, params, pictures));
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(" end to read excel list by pos ,endTime is {}", (new Date()).getTime());
            }
        }

        if (params.isNeedSave()) {
            this.saveThisExcel(params, pojoClass, isXSSFWorkbook, (Workbook)book);
        }

        return new ExcelImportResult(result, this.verfiyFail, (Workbook)book);
    }

    private void saveFieldValue(ImportParams params, Object object, Cell cell, Map<String, ExcelImportEntity> excelParams, String titleString, Row row) throws Exception {
        Object value = this.cellValueServer.getValue(params.getDataHanlder(), object, cell, excelParams, titleString);
        if (object instanceof Map) {
            if (params.getDataHanlder() != null) {
                params.getDataHanlder().setMapValue((Map)object, titleString, value);
            } else {
                ((Map)object).put(titleString, value);
            }
        } else {
            ExcelVerifyHandlerResult verifyResult = this.verifyHandlerServer.verifyData(object, value, titleString, ((ExcelImportEntity)excelParams.get(titleString)).getVerify(), params.getVerifyHanlder());
            if (!verifyResult.isSuccess()) {
                Cell errorCell = row.createCell(row.getLastCellNum());
                errorCell.setCellValue(verifyResult.getMsg());
                errorCell.setCellStyle(this.errorCellStyle);
                this.verfiyFail = true;
                throw new ExcelImportException(ExcelImportEnum.VERIFY_ERROR);
            }

            this.setValues((ExcelImportEntity)excelParams.get(titleString), object, value);
        }

    }

    private void saveImage(Object object, String picId, Map<String, ExcelImportEntity> excelParams, String titleString, Map<String, PictureData> pictures, ImportParams params) throws Exception {
        if (pictures != null && pictures.get(picId) != null) {
            PictureData image = (PictureData)pictures.get(picId);
            byte[] data = image.getData();
            String fileName = "pic" + Math.round(Math.random() * 1.0E11D);
            fileName = fileName + "." + PoiPublicUtil.getFileExtendName(data);
            int saveType = ((ExcelImportEntity)excelParams.get(titleString)).getSaveType();
            if (saveType == 1) {
                String path = PoiPublicUtil.getWebRootPath(this.getSaveUrl((ExcelImportEntity)excelParams.get(titleString), object));
                File savefile = new File(path);
                if (!savefile.exists()) {
                    savefile.mkdirs();
                }

                savefile = new File(path + "/" + fileName);
                FileOutputStream fos = new FileOutputStream(savefile);
                fos.write(data);
                fos.close();
                this.setValues((ExcelImportEntity)excelParams.get(titleString), object, this.getSaveUrl((ExcelImportEntity)excelParams.get(titleString), object) + "/" + fileName);
            } else if (saveType == 2) {
                this.setValues((ExcelImportEntity)excelParams.get(titleString), object, data);
            } else {
                ImportFileServiceI importFileService = null;

                try {
                    importFileService = (ImportFileServiceI) ApplicationContextUtil.getContext().getBean(ImportFileServiceI.class);
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                }

                if (importFileService != null) {
                    String dbPath = importFileService.doUpload(data);
                    this.setValues((ExcelImportEntity)excelParams.get(titleString), object, dbPath);
                }
            }

        }
    }

    private void createErrorCellStyle(Workbook workbook) {
        this.errorCellStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setColor((short)10);
        this.errorCellStyle.setFont(font);
    }
}
