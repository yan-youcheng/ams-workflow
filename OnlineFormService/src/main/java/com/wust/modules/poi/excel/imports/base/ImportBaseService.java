package com.wust.modules.poi.excel.imports.base;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.wust.modules.core.util.ApplicationContextUtil;
import com.wust.modules.poi.excel.annotation.Excel;
import com.wust.modules.poi.excel.annotation.ExcelCollection;
import com.wust.modules.poi.excel.annotation.ExcelVerify;
import com.wust.modules.poi.excel.entity.ImportParams;
import com.wust.modules.poi.excel.entity.params.ExcelCollectionParams;
import com.wust.modules.poi.excel.entity.params.ExcelImportEntity;
import com.wust.modules.poi.excel.entity.params.ExcelVerifyEntity;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import com.wust.modules.dict.service.AutoPoiDictServiceI;
import com.wust.modules.poi.util.PoiPublicUtil;

/**
 * @author wanheng
 */
public class ImportBaseService {

    public void addEntityToMap(String targetId, Field field, Class<?> pojoClass, List<Method> getMethods, Map<String, ExcelImportEntity> temp) throws Exception {
        Excel excel = field.getAnnotation(Excel.class);
        ExcelImportEntity excelEntity = new ExcelImportEntity();
        excelEntity.setType(excel.type());
        excelEntity.setSaveUrl(excel.savePath());
        excelEntity.setSaveType(excel.imageType());
        excelEntity.setReplace(excel.replace());
        excelEntity.setDatabaseFormat(excel.databaseFormat());
        excelEntity.setVerify(this.getImportVerify(field));
        excelEntity.setSuffix(excel.suffix());
        excelEntity.setNumFormat(excel.numFormat());
        excelEntity.setGroupName(excel.groupName());
        excelEntity.setMultiReplace(excel.multiReplace());
        if (StringUtils.isNotEmpty(excel.dicCode())) {
            AutoPoiDictServiceI dictService = null;

            try {
                dictService = (AutoPoiDictServiceI) ApplicationContextUtil.getContext().getBean(AutoPoiDictServiceI.class);
            } catch (Exception e) {
            }

            if (dictService != null) {
                String[] dictReplace = dictService.queryDict(excel.dictTable(), excel.dicCode(), excel.dicText());
                if (excelEntity.getReplace() != null && dictReplace != null && dictReplace.length != 0) {
                    excelEntity.setReplace(dictReplace);
                }
            }
        }

        this.getExcelField(targetId, field, excelEntity, excel, pojoClass);
        if (getMethods != null) {
            List<Method> newMethods = new ArrayList();
            newMethods.addAll(getMethods);
            newMethods.add(excelEntity.getMethod());
            excelEntity.setMethods(newMethods);
        }

        temp.put(excelEntity.getName(), excelEntity);
    }

    public ExcelVerifyEntity getImportVerify(Field field) {
        ExcelVerify verify = (ExcelVerify)field.getAnnotation(ExcelVerify.class);
        if (verify != null) {
            ExcelVerifyEntity entity = new ExcelVerifyEntity();
            entity.setEmail(verify.isEmail());
            entity.setInterHandler(verify.interHandler());
            entity.setMaxLength(verify.maxLength());
            entity.setMinLength(verify.minLength());
            entity.setMobile(verify.isMobile());
            entity.setNotNull(verify.notNull());
            entity.setRegex(verify.regex());
            entity.setRegexTip(verify.regexTip());
            entity.setTel(verify.isTel());
            return entity;
        } else {
            return null;
        }
    }

    public void getAllExcelField(String targetId, Field[] fields, Map<String, ExcelImportEntity> excelParams, List<ExcelCollectionParams> excelCollection, Class<?> pojoClass, List<Method> getMethods) throws Exception {
        ExcelImportEntity excelEntity = null;

        for(int i = 0; i < fields.length; ++i) {
            Field field = fields[i];
            if (!PoiPublicUtil.isNotUserExcelUserThis((List)null, field, targetId)) {
                if (PoiPublicUtil.isCollection(field.getType())) {
                    ExcelCollectionParams collection = new ExcelCollectionParams();
                    collection.setName(field.getName());
                    Map<String, ExcelImportEntity> temp = new HashMap();
                    ParameterizedType pt = (ParameterizedType)field.getGenericType();
                    Class<?> clz = (Class)pt.getActualTypeArguments()[0];
                    collection.setType(clz);
                    this.getExcelFieldList(targetId, PoiPublicUtil.getClassFields(clz), clz, temp, (List)null);
                    collection.setExcelParams(temp);
                    collection.setExcelName(((ExcelCollection)field.getAnnotation(ExcelCollection.class)).name());
                    this.additionalCollectionName(collection);
                    excelCollection.add(collection);
                } else if (PoiPublicUtil.isJavaClass(field)) {
                    this.addEntityToMap(targetId, field, pojoClass, getMethods, excelParams);
                } else {
                    List<Method> newMethods = new ArrayList();
                    if (getMethods != null) {
                        newMethods.addAll(getMethods);
                    }

                    newMethods.add(PoiPublicUtil.getMethod(field.getName(), pojoClass));
                    this.getAllExcelField(targetId, PoiPublicUtil.getClassFields(field.getType()), excelParams, excelCollection, field.getType(), newMethods);
                }
            }
        }

    }

    private void additionalCollectionName(ExcelCollectionParams collection) {
        Set<String> keys = new HashSet();
        keys.addAll(collection.getExcelParams().keySet());
        Iterator iterator = keys.iterator();

        while(iterator.hasNext()) {
            String key = (String) iterator.next();
            collection.getExcelParams().put(collection.getExcelName() + "_" + key, collection.getExcelParams().get(key));
            collection.getExcelParams().remove(key);
        }

    }

    public void getExcelField(String targetId, Field field, ExcelImportEntity excelEntity, Excel excel, Class<?> pojoClass) throws Exception {
        excelEntity.setName(this.getExcelName(excel.name(), targetId));
        String fieldname = field.getName();
        excelEntity.setMethod(PoiPublicUtil.getMethod(fieldname, pojoClass, field.getType(), excel.importConvert()));
        if (StringUtils.isNotEmpty(excel.importFormat())) {
            excelEntity.setFormat(excel.importFormat());
        } else {
            excelEntity.setFormat(excel.format());
        }

    }

    public void getExcelFieldList(String targetId, Field[] fields, Class<?> pojoClass, Map<String, ExcelImportEntity> temp, List<Method> getMethods) throws Exception {

        for(int i = 0; i < fields.length; ++i) {
            Field field = fields[i];
            if (!PoiPublicUtil.isNotUserExcelUserThis((List)null, field, targetId)) {
                if (PoiPublicUtil.isJavaClass(field)) {
                    this.addEntityToMap(targetId, field, pojoClass, getMethods, temp);
                } else {
                    List<Method> newMethods = new ArrayList();
                    if (getMethods != null) {
                        newMethods.addAll(getMethods);
                    }

                    newMethods.add(PoiPublicUtil.getMethod(field.getName(), pojoClass, field.getType()));
                    this.getExcelFieldList(targetId, PoiPublicUtil.getClassFields(field.getType()), field.getType(), temp, newMethods);
                }
            }
        }

    }

    public String getExcelName(String exportName, String targetId) {
        if (exportName.indexOf("_") < 0) {
            return exportName;
        } else {
            String[] arr = exportName.split(",");
            String[] strings = arr;
            int length = arr.length;

            for(int i = 0; i < length; ++i) {
                String str = strings[i];
                if (str.indexOf(targetId) != -1) {
                    return str.split("_")[0];
                }
            }

            return null;
        }
    }

    public Object getFieldBySomeMethod(List<Method> list, Object t) throws Exception {
        for(int i = 0; i < list.size() - 1; ++i) {
            Method m = (Method)list.get(i);
            t = m.invoke(t);
        }

        return t;
    }

    public void saveThisExcel(ImportParams params, Class<?> pojoClass, boolean isXSSFWorkbook, Workbook book) throws Exception {
        String path = PoiPublicUtil.getWebRootPath(this.getSaveExcelUrl(params, pojoClass));
        File savefile = new File(path);
        if (!savefile.exists()) {
            savefile.mkdirs();
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyMMddHHmmss");
        FileOutputStream fos = new FileOutputStream(path + "/" + format.format(new Date()) + "_" + Math.round(Math.random() * 100000.0D) + (isXSSFWorkbook ? ".xlsx" : ".xls"));
        book.write(fos);
        fos.close();
    }

    public String getSaveExcelUrl(ImportParams params, Class<?> pojoClass) throws Exception {
        String url = "";
        if ("upload/excelUpload".equals(params.getSaveUrl())) {
            url = pojoClass.getName().split("\\.")[pojoClass.getName().split("\\.").length - 1];
            return params.getSaveUrl() + "/" + url;
        } else {
            return params.getSaveUrl();
        }
    }

    public void setFieldBySomeMethod(List<Method> setMethods, Object object, Object value) throws Exception {
        Object t = this.getFieldBySomeMethod(setMethods, object);
        ((Method)setMethods.get(setMethods.size() - 1)).invoke(t, value);
    }

    public void setValues(ExcelImportEntity entity, Object object, Object value) throws Exception {
        if (entity.getMethods() != null) {
            this.setFieldBySomeMethod(entity.getMethods(), object, value);
        } else {
            entity.getMethod().invoke(object, value);
        }

    }
}
