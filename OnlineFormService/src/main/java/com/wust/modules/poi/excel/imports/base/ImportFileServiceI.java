package com.wust.modules.poi.excel.imports.base;

/**
 * @author wanheng
 */
public interface ImportFileServiceI {

    String doUpload(byte[] data);
}
