package com.wust.modules.poi.excel.imports.verify;

import com.wust.modules.poi.excel.entity.result.ExcelVerifyHandlerResult;

import java.util.regex.Pattern;

/**
 * @author wanheng
 */
public class BaseVerifyHandler {

    private static final String NOT_NULL = "不允许为空";
    private static final String IS_MOBILE = "不是手机号";
    private static final String IS_TEL = "不是电话号码";
    private static final String IS_EMAIL = "不是邮箱地址";
    private static final String MIN_LENGHT = "小于规定长度";
    private static final String MAX_LENGHT = "超过规定长度";
    private static final Pattern MOBILE_PATTERN = Pattern.compile("^[1][3,4,5,8,7][0-9]{9}$");
    private static final Pattern TEL_PATTERN = Pattern.compile("^([0][1-9]{2,3}-)?[0-9]{5,10}$");
    private static final Pattern EMAIL_PATTERN = Pattern.compile("^([a-zA-Z0-9]+[_|\\_|\\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\\_|\\.]?)*[a-zA-Z0-9]+\\.[a-zA-Z]{2,3}$");

    public static ExcelVerifyHandlerResult isEmail(String name, Object val) {
        return !EMAIL_PATTERN.matcher(String.valueOf(val)).matches() ? new ExcelVerifyHandlerResult(false, name + IS_EMAIL) : new ExcelVerifyHandlerResult(true);
    }

    public static ExcelVerifyHandlerResult isMobile(String name, Object val) {
        return !MOBILE_PATTERN.matcher(String.valueOf(val)).matches() ? new ExcelVerifyHandlerResult(false, name + IS_MOBILE) : new ExcelVerifyHandlerResult(true);
    }

    public static ExcelVerifyHandlerResult isTel(String name, Object val) {
        return !TEL_PATTERN.matcher(String.valueOf(val)).matches() ? new ExcelVerifyHandlerResult(false, name + IS_TEL) : new ExcelVerifyHandlerResult(true);
    }

    public static ExcelVerifyHandlerResult maxLength(String name, Object val, int maxLength) {
        return notNull(name, val).isSuccess() && String.valueOf(val).length() > maxLength ? new ExcelVerifyHandlerResult(false, name + MAX_LENGHT) : new ExcelVerifyHandlerResult(true);
    }

    public static ExcelVerifyHandlerResult minLength(String name, Object val, int minLength) {
        return notNull(name, val).isSuccess() && String.valueOf(val).length() < minLength ? new ExcelVerifyHandlerResult(false, name + MIN_LENGHT) : new ExcelVerifyHandlerResult(true);
    }

    public static ExcelVerifyHandlerResult notNull(String name, Object val) {
        return val != null && !"".equals(val.toString()) ? new ExcelVerifyHandlerResult(true) : new ExcelVerifyHandlerResult(false, name + NOT_NULL);
    }

    public static ExcelVerifyHandlerResult regex(String name, Object val, String regex, String regexTip) {
        Pattern pattern = Pattern.compile(regex);
        return !pattern.matcher(String.valueOf(val)).matches() ? new ExcelVerifyHandlerResult(false, name + regexTip) : new ExcelVerifyHandlerResult(true);
    }
}
