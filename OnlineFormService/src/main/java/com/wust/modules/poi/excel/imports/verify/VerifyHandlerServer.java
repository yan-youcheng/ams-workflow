package com.wust.modules.poi.excel.imports.verify;

import com.wust.modules.poi.excel.entity.params.ExcelVerifyEntity;
import com.wust.modules.poi.excel.entity.result.ExcelVerifyHandlerResult;
import org.apache.commons.lang3.StringUtils;
import com.wust.modules.poi.handler.inter.IExcelVerifyHandler;

/**
 * @author wanheng
 */
public class VerifyHandlerServer {

    private static final ExcelVerifyHandlerResult DEFAULT_RESULT = new ExcelVerifyHandlerResult(true);

    private void addVerifyResult(ExcelVerifyHandlerResult handlerResult, ExcelVerifyHandlerResult result) {
        if (!handlerResult.isSuccess()) {
            result.setSuccess(false);
            result.setMsg((StringUtils.isEmpty(result.getMsg()) ? "" : result.getMsg() + " , ") + handlerResult.getMsg());
        }

    }

    public ExcelVerifyHandlerResult verifyData(Object object, Object value, String name, ExcelVerifyEntity verify, IExcelVerifyHandler excelVerifyHandler) {
        if (verify == null) {
            return DEFAULT_RESULT;
        } else {
            ExcelVerifyHandlerResult result = new ExcelVerifyHandlerResult(true, "");
            if (verify.isNotNull()) {
                this.addVerifyResult(BaseVerifyHandler.notNull(name, value), result);
            }

            if (verify.isEmail()) {
                this.addVerifyResult(BaseVerifyHandler.isEmail(name, value), result);
            }

            if (verify.isMobile()) {
                this.addVerifyResult(BaseVerifyHandler.isMobile(name, value), result);
            }

            if (verify.isTel()) {
                this.addVerifyResult(BaseVerifyHandler.isTel(name, value), result);
            }

            if (verify.getMaxLength() != -1) {
                this.addVerifyResult(BaseVerifyHandler.maxLength(name, value, verify.getMaxLength()), result);
            }

            if (verify.getMinLength() != -1) {
                this.addVerifyResult(BaseVerifyHandler.minLength(name, value, verify.getMinLength()), result);
            }

            if (StringUtils.isNotEmpty(verify.getRegex())) {
                this.addVerifyResult(BaseVerifyHandler.regex(name, value, verify.getRegex(), verify.getRegexTip()), result);
            }

            if (verify.isInterHandler()) {
                this.addVerifyResult(excelVerifyHandler.verifyHandler(object, name, value), result);
            }

            return result;
        }
    }
}
