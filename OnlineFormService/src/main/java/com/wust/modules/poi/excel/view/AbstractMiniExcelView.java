package com.wust.modules.poi.excel.view;

import javax.servlet.http.HttpServletRequest;
import org.springframework.web.servlet.view.AbstractView;

/**
 * @author wanheng
 */
public abstract class AbstractMiniExcelView extends AbstractView {

    private static final String CONTENT_TYPE = "application/vnd.ms-excel";
    protected static final String HSSF = ".xls";
    protected static final String XSSF = ".xlsx";

    public AbstractMiniExcelView() {
        this.setContentType("application/vnd.ms-excel");
    }

    protected boolean isIE(HttpServletRequest request) {
        return request.getHeader("USER-AGENT").toLowerCase().indexOf("msie") > 0 || request.getHeader("USER-AGENT").toLowerCase().indexOf("rv:11.0") > 0;
    }
}
