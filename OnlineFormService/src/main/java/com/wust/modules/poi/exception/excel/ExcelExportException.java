package com.wust.modules.poi.exception.excel;

import com.wust.modules.poi.exception.excel.enums.ExcelExportEnum;
import lombok.Data;

/**
 * @author wanheng
 */
@Data
public class ExcelExportException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private ExcelExportEnum type;

    public ExcelExportException(ExcelExportEnum type) {
        super(type.getMsg());
        this.type = type;
    }

    public ExcelExportException(ExcelExportEnum type, Throwable cause) {
        super(type.getMsg(), cause);
    }

    public ExcelExportException(String message) {
        super(message);
    }

}
