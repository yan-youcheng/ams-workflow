package com.wust.modules.poi.exception.excel;

import com.wust.modules.poi.exception.excel.enums.ExcelImportEnum;
import lombok.Data;

/**
 * @author wanheng
 */
@Data
public class ExcelImportException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private ExcelImportEnum type;


    public ExcelImportException(ExcelImportEnum type) {
        super(type.getMsg());
        this.type = type;
    }

    public ExcelImportException(ExcelImportEnum type, Throwable cause) {
        super(type.getMsg(), cause);
    }

    public ExcelImportException(String message) {
        super(message);
    }

}
