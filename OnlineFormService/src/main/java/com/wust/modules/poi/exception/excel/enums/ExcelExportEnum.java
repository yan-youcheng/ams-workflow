package com.wust.modules.poi.exception.excel.enums;

/**
 * @author wanheng
 */

public enum ExcelExportEnum {

    /**
     * ExcelError
     */
    PARAMETER_ERROR("Excel 导出   参数错误"),
    EXPORT_ERROR("Excel导出错误");

    private String msg;

    ExcelExportEnum(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
