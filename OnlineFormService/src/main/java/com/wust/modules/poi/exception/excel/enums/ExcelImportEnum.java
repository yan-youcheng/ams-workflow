package com.wust.modules.poi.exception.excel.enums;

/**
 * @author wanheng
 */

public enum ExcelImportEnum {

    /**
     * ERROR
     */
    GET_VALUE_ERROR("Excel 值获取失败"),
    VERIFY_ERROR("值校验失败");

    private String msg;

    ExcelImportEnum(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
