package com.wust.modules.poi.exception.word;

import com.wust.modules.poi.exception.word.enmus.WordExportEnum;

/**
 * @author wanheng
 */
public class WordExportException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public WordExportException(WordExportEnum exception) {
        super(exception.getMsg());
    }
}
