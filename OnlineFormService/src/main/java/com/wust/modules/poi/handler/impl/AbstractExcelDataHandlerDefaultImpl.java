package com.wust.modules.poi.handler.impl;

import com.wust.modules.poi.handler.inter.IExcelDataHandler;

import java.util.Map;

/**
 * @author wanheng
 */
public abstract class AbstractExcelDataHandlerDefaultImpl implements IExcelDataHandler {

    private String[] needHandlerFields;

    @Override
    public Object exportHandler(Object obj, String name, Object value) {
        return value;
    }

    @Override
    public String[] getNeedHandlerFields() {
        return this.needHandlerFields;
    }

    @Override
    public Object importHandler(Object obj, String name, Object value) {
        return value;
    }

    @Override
    public void setMapValue(Map<String, Object> map, String originKey, Object value) {
        map.put(originKey, value);
    }
}
