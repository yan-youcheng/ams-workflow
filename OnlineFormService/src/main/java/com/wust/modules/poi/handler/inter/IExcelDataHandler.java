package com.wust.modules.poi.handler.inter;

import java.util.Map;

/**
 * @author wanheng
 */
public interface IExcelDataHandler {

    /**
     * exportHandler
     * @param obj
     * @param name
     * @param value
     * @return
     */
    Object exportHandler(Object obj, String name, Object value);

    /**
     * getNeedHandlerFields
     * @return
     */
    String[] getNeedHandlerFields();

    /**
     * importHandler
     * @param obj
     * @param name
     * @param value
     * @return
     */
    Object importHandler(Object obj, String name, Object value);

    /**
     * setMapValue
     * @param map
     * @param originKey
     * @param value
     */
    void setMapValue(Map<String, Object> map, String originKey, Object value);
}
