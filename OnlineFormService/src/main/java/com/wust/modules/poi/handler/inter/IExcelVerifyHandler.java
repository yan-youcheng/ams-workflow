package com.wust.modules.poi.handler.inter;

import com.wust.modules.poi.excel.entity.result.ExcelVerifyHandlerResult;

/**
 * @author wanheng
 */
public interface IExcelVerifyHandler {

    /**
     * verifyHandler
     * @param object
     * @param name
     * @param value
     * @return
     */
    ExcelVerifyHandlerResult verifyHandler(Object object, String name, Object value);

}
