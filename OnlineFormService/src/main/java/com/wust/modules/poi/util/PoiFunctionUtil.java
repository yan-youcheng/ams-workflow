package com.wust.modules.poi.util;

import com.wust.modules.poi.exception.excel.ExcelExportException;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Map;

/**
 * @author wanheng
 */
public final class PoiFunctionUtil {

    private static final String TWO_DECIMAL_STR = "###.00";
    private static final String THREE_DECIMAL_STR = "###.000";
    private static final DecimalFormat TWO_DECIMAL = new DecimalFormat("###.00");
    private static final DecimalFormat THREE_DECIMAL = new DecimalFormat("###.000");
    private static final String DAY_STR = "yyyy-MM-dd";
    private static final String TIME_STR = "yyyy-MM-dd HH:mm:ss";
    private static final String TIME__NO_S_STR = "yyyy-MM-dd HH:mm";
    private static final SimpleDateFormat DAY_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat TIME__NO_S_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public static int length(Object obj) {
        if (obj == null) {
            return 0;
        } else if (obj instanceof Map) {
            return ((Map)obj).size();
        } else if (obj instanceof Collection) {
            return ((Collection)obj).size();
        } else {
            return obj.getClass().isArray() ? Array.getLength(obj) : String.valueOf(obj).length();
        }
    }

    public static String formatNumber(Object obj, String format) {
        if (obj != null && obj.toString() != "") {
            double number = Double.parseDouble(obj.toString());
            DecimalFormat decimalFormat;
            if (TWO_DECIMAL.equals(format)) {
                decimalFormat = TWO_DECIMAL;
            } else if ("###.000".equals(format)) {
                decimalFormat = THREE_DECIMAL;
            } else {
                decimalFormat = new DecimalFormat(format);
            }

            return decimalFormat.format(number);
        } else {
            return "";
        }
    }

    public static String formatDate(Object obj, String format) {
        if (obj != null && obj.toString() != "") {
            SimpleDateFormat dateFormat;
            if ("yyyy-MM-dd".equals(format)) {
                dateFormat = DAY_FORMAT;
            } else if ("yyyy-MM-dd HH:mm:ss".equals(format)) {
                dateFormat = TIME_FORMAT;
            } else if ("yyyy-MM-dd HH:mm".equals(format)) {
                dateFormat = TIME__NO_S_FORMAT;
            } else {
                dateFormat = new SimpleDateFormat(format);
            }

            return dateFormat.format(obj);
        } else {
            return "";
        }
    }

    public static boolean isTrue(Object first, String operator, Object second) {
        if (">".endsWith(operator)) {
            return isGt(first, second);
        } else if ("<".endsWith(operator)) {
            return isGt(second, first);
        } else if ("==".endsWith(operator)) {
            if (first != null && second != null) {
                return first.equals(second);
            } else {
                return first == second;
            }
        } else if ("!=".endsWith(operator)) {
            if (first != null && second != null) {
                return !first.equals(second);
            } else {
                return first != second;
            }
        } else {
            throw new ExcelExportException("占不支持改操作符");
        }
    }

    private static boolean isGt(Object first, Object second) {
        if (first != null && first.toString() != "") {
            if (second != null && second.toString() != "") {
                double one = Double.parseDouble(first.toString());
                double two = Double.parseDouble(second.toString());
                return one > two;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
