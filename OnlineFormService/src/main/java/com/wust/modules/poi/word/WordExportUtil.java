package com.wust.modules.poi.word;

import java.util.Map;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import com.wust.modules.poi.word.parse.ParseWord07;

/**
 * @author wanheng
 */
public final class WordExportUtil {

    public static XWPFDocument exportWord07(String url, Map<String, Object> map) throws Exception {
        return (new ParseWord07()).parseWord(url, map);
    }

}
