package com.wust.modules.poi.word.entity;

import lombok.Data;

/**
 * @author wanheng
 */
@Data
public class WordImageEntity {

    public static String URL = "url";
    public static String Data = "data";
    private String type;
    private int width;
    private int height;
    private String url;
    private byte[] data;

}
