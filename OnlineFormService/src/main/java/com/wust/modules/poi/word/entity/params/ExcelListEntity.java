package com.wust.modules.poi.word.entity.params;

import com.wust.modules.poi.excel.entity.ExcelBaseParams;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author wanheng
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ExcelListEntity extends ExcelBaseParams {

    private List<?> list;
    private Class<?> clazz;
    private int headRows = 1;

}
