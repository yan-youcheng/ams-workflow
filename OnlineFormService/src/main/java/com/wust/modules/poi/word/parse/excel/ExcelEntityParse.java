package com.wust.modules.poi.word.parse.excel;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.wust.modules.poi.excel.annotation.ExcelTarget;
import com.wust.modules.poi.excel.entity.params.ExcelExportEntity;
import com.wust.modules.poi.excel.export.base.ExportBase;
import com.wust.modules.poi.exception.word.WordExportException;
import com.wust.modules.poi.exception.word.enmus.WordExportEnum;
import com.wust.modules.poi.util.PoiPublicUtil;
import com.wust.modules.poi.word.entity.params.ExcelListEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

/**
 * @author wanheng
 */
@Slf4j
public class ExcelEntityParse extends ExportBase {

    private static void checkExcelParams(ExcelListEntity entity) {
        if (entity.getList() == null || entity.getClazz() == null) {
            throw new WordExportException(WordExportEnum.EXCEL_PARAMS_ERROR);
        }
    }

    private int createCells(int index, Object t, List<ExcelExportEntity> excelParams, XWPFTable table, short rowHeight) throws Exception {
        XWPFTableRow row = table.createRow();
        row.setHeight(rowHeight);
        int maxHeight = 1;
        int cellNum = 0;
        int k = 0;

        ExcelExportEntity entity;
        int paramSize;
        for(paramSize = excelParams.size(); k < paramSize; ++k) {
            entity = excelParams.get(k);
            if (entity.getList() == null) {
                Object value = this.getCellValue(entity, t);
                if (entity.getType() == 1) {
                    this.setCellValue(row, value, cellNum++);
                }
            } else {
                Collection<?> list = (Collection)entity.getMethod().invoke(t);
                int listC = 0;

                for(Iterator iterator = list.iterator(); iterator.hasNext(); ++listC) {
                    Object obj = iterator.next();
                    this.createListCells(index + listC, cellNum, obj, entity.getList(), table);
                }

                cellNum += entity.getList().size();
                if (list.size() > maxHeight) {
                    maxHeight = list.size();
                }
            }
        }

        cellNum = 0;
        k = 0;

        for(paramSize = excelParams.size(); k < paramSize; ++k) {
            entity = excelParams.get(k);
            if (entity.getList() != null) {
                cellNum += entity.getList().size();
            } else if (entity.isNeedMerge()) {
                table.setCellMargins(index, index + maxHeight - 1, cellNum, cellNum);
                ++cellNum;
            }
        }

        return maxHeight;
    }

    public void createListCells(int index, int cellNum, Object obj, List<ExcelExportEntity> excelParams, XWPFTable table) throws Exception {
        XWPFTableRow row;
        if (table.getRow(index) == null) {
            row = table.createRow();
            row.setHeight(this.getRowHeight(excelParams));
        } else {
            row = table.getRow(index);
        }

        int k = 0;

        for(int paramSize = excelParams.size(); k < paramSize; ++k) {
            ExcelExportEntity entity = excelParams.get(k);
            Object value = this.getCellValue(entity, obj);
            if (entity.getType() == 1) {
                this.setCellValue(row, value, cellNum++);
            }
        }

    }

    private Map<String, Integer> getTitleMap(XWPFTable table, int index, int headRows) {
        if (index < headRows) {
            throw new WordExportException(WordExportEnum.EXCEL_NO_HEAD);
        } else {
            Map<String, Integer> map = new HashMap(16);

            for(int j = 0; j < headRows; ++j) {
                List<XWPFTableCell> cells = table.getRow(index - j - 1).getTableCells();

                for(int i = 0; i < cells.size(); ++i) {
                    String text = cells.get(i).getText();
                    if (StringUtils.isEmpty(text)) {
                        throw new WordExportException(WordExportEnum.EXCEL_HEAD_HAVA_NULL);
                    }

                    map.put(text, i);
                }
            }

            return map;
        }
    }

    public void parseNextRowAndAddRow(XWPFTable table, int index, ExcelListEntity entity) {
        checkExcelParams(entity);
        Map titleMap = this.getTitleMap(table, index, entity.getHeadRows());

        try {
            Field[] fileds = PoiPublicUtil.getClassFields(entity.getClazz());
            ExcelTarget etarget = entity.getClazz().getAnnotation(ExcelTarget.class);
            String targetId = null;
            if (etarget != null) {
                targetId = etarget.value();
            }

            List<ExcelExportEntity> excelParams = new ArrayList();
            this.getAllExcelField(null, targetId, fileds, excelParams, entity.getClazz(), null);
            this.sortAndFilterExportField(excelParams, titleMap);
            short rowHeight = this.getRowHeight(excelParams);

            Object t;
            for(Iterator its = entity.getList().iterator(); its.hasNext(); index += this.createCells(index, t, excelParams, table, rowHeight)) {
                t = its.next();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    private void setCellValue(XWPFTableRow row, Object value, int cellNum) {
        if (row.getCell(cellNum++) != null) {
            row.getCell(cellNum - 1).setText(value == null ? "" : value.toString());
        } else {
            row.createCell().setText(value == null ? "" : value.toString());
        }

    }

    private void sortAndFilterExportField(List<ExcelExportEntity> excelParams, Map<String, Integer> titlemap) {
        for(int i = excelParams.size() - 1; i >= 0; --i) {
            if (excelParams.get(i).getList() != null && excelParams.get(i).getList().size() > 0) {
                this.sortAndFilterExportField(excelParams.get(i).getList(), titlemap);
                if (excelParams.get(i).getList().size() == 0) {
                    excelParams.remove(i);
                } else {
                    excelParams.get(i).setOrderNum(i);
                }
            } else if (titlemap.containsKey(excelParams.get(i).getName())) {
                excelParams.get(i).setOrderNum(i);
            } else {
                excelParams.remove(i);
            }
        }

        this.sortAllParams(excelParams);
    }
}
