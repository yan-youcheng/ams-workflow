package com.wust.modules.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.wust.common.system.vo.DictModel;
import com.wust.modules.system.entity.SysDict;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @Author zhangweijian
 * @since 2018-12-28
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

    /**
     * queryDictItemsByCode
     * @param code
     * @return
     */
    List<DictModel> queryDictItemsByCode(@Param("code") String code);

    /**
     * queryTableDictItemsByCode
     * @param table
     * @param text
     * @param code
     * @return
     */
    @Deprecated
    List<DictModel> queryTableDictItemsByCode(@Param("table") String table, @Param("text") String text, @Param("code") String code);

    /**
     * queryTableDictItemsByCodeAndFilter
     * @param table
     * @param text
     * @param code
     * @param filterSql
     * @return
     */
    @Deprecated
    List<DictModel> queryTableDictItemsByCodeAndFilter(@Param("table") String table, @Param("text") String text, @Param("code") String code, @Param("filterSql") String filterSql);

}
