package com.wust.modules.system.service;

import com.wust.modules.system.entity.SysDict;
import com.wust.common.system.vo.DictModel;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @Author zhangweijian
 * @since 2018-12-28
 */
public interface ISysDictService extends IService<SysDict> {

    /**
     * 通过查询指定code 获取字典
     * @param code
     * @return
     */
    List<DictModel> queryDictItemsByCode(String code);

    /**
     * 通过查询指定table的 text code 获取字典
     * dictTableCache采用redis缓存有效期10分钟
     * @param table
     * @param text
     * @param code
     * @return
     */
    @Deprecated
    List<DictModel> queryTableDictItemsByCode(String table, String text, String code);

    /**
     * 通过查询指定table的 text code filter获取字典
     * @param table
     * @param text
     * @param code
     * @param filterSql
     * @return
     */
    @Deprecated
    List<DictModel> queryTableDictItemsByCodeAndFilter(String table, String text, String code, String filterSql);

}
