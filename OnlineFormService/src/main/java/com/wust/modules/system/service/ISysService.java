package com.wust.modules.system.service;

import com.wust.modules.system.entity.SysUser;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author wanheng
 */
@Service
public interface ISysService {

    /**
     * 根据id 获取用户信息
     * @param id
     * @return
     */
    SysUser getSysUserById(String id);

    /**
     * 获取隐藏字段
     * @param userId
     * @param onlineTbname
     * @return
     */
    List<String> selectOnlineHideColumns(String userId, String onlineTbname);

    /**
     * 根据token获取登录用户
     * @return
     */
    SysUser getLoginUser();

}
