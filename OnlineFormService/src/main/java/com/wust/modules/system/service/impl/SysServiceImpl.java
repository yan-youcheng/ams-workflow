package com.wust.modules.system.service.impl;

import com.wust.common.api.vo.Result;
import com.wust.common.util.SpringContextUtils;
import com.wust.modules.feign.service.UserFeignService;
import com.wust.modules.system.entity.SysUser;
import com.wust.modules.system.service.ISysService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author wanheng
 */
@Service("sysService")
public class SysServiceImpl implements ISysService{

    @Resource
    private UserFeignService userFeignService;

    @Override
    public SysUser getSysUserById(String id) {
        userFeignService = SpringContextUtils.getBean(UserFeignService.class);
        Result<SysUser> result = userFeignService.queryById(id);
        SysUser user = result.getResult();
        return user;
    }

    @Override
    public List<String> selectOnlineHideColumns(String userId, String onlineTbname) {
        return null;
    }

    @Override
    public SysUser getLoginUser() {
        HttpServletRequest request = SpringContextUtils.getHttpServletRequest();
        String userId = request.getHeader("userId");
        SysUser sysUser = getSysUserById(userId);
        return sysUser;
    }

}