package com.wust.modules.system.util;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wanheng
 */
@Slf4j
@Data
public class TenantContext {

    private static ThreadLocal<String> currentTenant;

    public static void setTenant(String tenant) {
        log.debug(" setting tenant to " + tenant);
        currentTenant.set(tenant);
    }

    public static String getTenant() {
        return currentTenant.get();
    }

    public static void clear(){
        currentTenant.remove();
    }
}