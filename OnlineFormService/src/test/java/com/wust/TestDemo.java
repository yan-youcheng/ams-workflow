package com.wust;

import com.wust.common.util.LoginUserUtil;
import com.wust.common.util.SpringContextUtils;
import com.wust.modules.codegenerate.database.DbReadTableUtil;
import com.wust.modules.feign.service.UserFeignService;
import com.wust.modules.system.service.ISysService;
import com.wust.modules.system.service.impl.SysServiceImpl;
import org.junit.Test;

import javax.annotation.Resource;

public class TestDemo {

    @Resource
    private UserFeignService userFeignService;

    @Test
    public void test01() {
        String table_name = DbReadTableUtil.getUpCamelCase("table_name");
        System.out.println(table_name);
    }

    @Test
    public void test02(){
        ISysService sysService = SpringContextUtils.getBean(SysServiceImpl.class);
        System.out.println(sysService);
    }

}
