package org.jeecg.modules.system.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Description: TODO
 * @author: 周天翼
 * @date: 2020年07月22日 20:29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysClient implements Serializable {

    private static final long serialVersionUID = 1L;

    private String clientId;

    private String resourceIds;

    private String clientSecret;

    private String scope;

    private String authorizedGrantTypes;

    private String webServerRedirectUri;

    private String authorities;

    private long accessTokenValidity;

    private long refreshTokenValidity;

    private String additionalInformation;

    private Timestamp createTime;

    private Integer archived;

    private Integer trusted;

    private String autoApprove;

}
