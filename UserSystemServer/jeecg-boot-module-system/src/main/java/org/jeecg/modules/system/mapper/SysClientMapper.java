package org.jeecg.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.system.entity.SysClient;
import org.jeecg.modules.system.entity.SysUser;

public interface SysClientMapper extends BaseMapper<SysClient> {
    SysClient getClientById(@Param("clientId") String clientId);
}
