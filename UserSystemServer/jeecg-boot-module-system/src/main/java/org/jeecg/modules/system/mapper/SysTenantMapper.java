package org.jeecg.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.entity.SysTenant;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SysTenantMapper extends BaseMapper<SysTenant> {



}
