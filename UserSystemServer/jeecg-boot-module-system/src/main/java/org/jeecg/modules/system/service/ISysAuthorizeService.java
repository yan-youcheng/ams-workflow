package org.jeecg.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.system.entity.SysClient;

public interface ISysAuthorizeService extends IService<SysClient> {

    SysClient getClientByClientId(String ClientId);

}
