package org.jeecg.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.system.entity.SysClient;
import org.jeecg.modules.system.mapper.SysClientMapper;
import org.jeecg.modules.system.service.ISysAuthorizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description: TODO
 * @author: 周天翼
 * @date: 2020年07月23日 19:29
 */
@Service
public class SysAuthorizedServiceImpl extends ServiceImpl<SysClientMapper, SysClient>  implements ISysAuthorizeService {

    @Autowired(required = false)
    SysClientMapper sysClientMapper;

    @Override
    public SysClient getClientByClientId(String ClientId) {
        return sysClientMapper.getClientById(ClientId);
    }
}
