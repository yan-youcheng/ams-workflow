package org.jeecg.modules.system.util;

import lombok.Data;

/**
 * Created by zhangpan on 2017/9/30.
 */
@Data
public class CommonResponse {
    public int code;
    public String msg;

    public Object data;

    public CommonResponse(){

    }
    public CommonResponse(int code, String msg){
        this.code = code;
        this.msg = msg;
    }
    public CommonResponse(int code, String msg, Object data){
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
