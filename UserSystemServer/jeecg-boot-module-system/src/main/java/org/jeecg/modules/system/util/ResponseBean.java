package org.jeecg.modules.system.util;

import lombok.Data;

/**
 * Created by zhangpan on 2017/9/30.
 */
@Data
public class ResponseBean extends CommonResponse {
    private int page;//当前页面数
    private long records;//总共记录数
    private  int total;//总页数
    public int pageSize;//每页记录数（每页大小都一致）
    //2020-6-19 新增查询顺序id
    private int queryId;
    public ResponseBean() {
    }

    public ResponseBean(int code, String msg, Object data) {
        super(code, msg, data);
    }

    public ResponseBean(int code, String msg) {
        super(code,msg);
    }
    public ResponseBean(int code, String msg, int page, long records, int total , int pageSize, Object data) {
        super(code,msg,data);
        this.pageSize = pageSize;
        this.page = page;
        this.records = records;
        this.total = total;
    }
    public ResponseBean(int code, String msg, int page, long records, int total , int pageSize, Object data, int queryId) {
        super(code,msg,data);
        this.pageSize = pageSize;
        this.page = page;
        this.records = records;
        this.total = total;
        this.queryId = queryId;
    }
    public int getQueryId(){
        return queryId;
    }
}
