package org.jeecg.modules.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "获取用户部门等信息请求")
public class GetUserListRequest {
    private String keyWord;
    private Integer pageNum;
    private Integer pageSize;
}
