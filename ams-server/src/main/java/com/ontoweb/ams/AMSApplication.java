package com.ontoweb.ams;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class AMSApplication {

    public static void main(String[] args) {
        SpringApplication.run(AMSApplication.class, args);
    }

}
