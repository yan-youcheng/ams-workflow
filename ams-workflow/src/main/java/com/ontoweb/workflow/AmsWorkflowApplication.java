package com.ontoweb.workflow;

import com.bstek.uflo.console.UfloServlet;
import com.ontoweb.workflow.filter.UfloFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableAutoConfiguration
//开启事物管理
@EnableTransactionManagement
@ImportResource("classpath:context.xml")
@ComponentScan("com/ontoweb/workflow")
@EnableDiscoveryClient
@EnableFeignClients
public class AmsWorkflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmsWorkflowApplication.class, args);
	}

	@Bean
	public ServletRegistrationBean servletRegistration() {
		//为servlet配置路径
		return new ServletRegistrationBean(new UfloServlet(), "/uflo/*");
	}

	//注册filter到springboot中
	@Bean
	public FilterRegistrationBean buildTestFilter(){
		FilterRegistrationBean bean=new FilterRegistrationBean();
		bean.setFilter(new UfloFilter());
		bean.addUrlPatterns("/*");
		return bean;
	}

}
