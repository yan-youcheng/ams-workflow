//package com.ontoweb.workflow.controller;
//
//import com.bstek.uflo.model.variable.Variable;
//import com.bstek.uflo.service.ProcessService;
//import com.bstek.uflo.service.StartProcessInfo;
//import com.bstek.uflo.service.TaskOpinion;
//import com.bstek.uflo.service.TaskService;
//import com.bstek.uflo.utils.EnvironmentUtils;
//import com.ontoweb.workflow.entity.Demo;
//import org.hibernate.Query;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.UUID;
//
//@Controller
//@RequestMapping("/bp")
//public class BussinessProcessController {
//    //处理审批的service
//    @Autowired
//    private TaskService taskService;
//    @Autowired
//    private ProcessService processService;
//    @Autowired
//    private SessionFactory sessionFactory;
//    @RequestMapping("/startProcess")
//    @ResponseBody
//    @Transactional
//    public Object startProcess(String name,String desc){
//        String id= UUID.randomUUID().toString();
//        Session session=sessionFactory.openSession();
//        try{
//            Demo demo=new Demo();
//            demo.setId(id);
//            demo.setName(name);
//            demo.setDesc(desc);
//            //写入业务数据到数据库
//            session.save(demo);
//            //获取当前登陆用户
//            StartProcessInfo startProcessInfo=new StartProcessInfo(EnvironmentUtils.getEnvironment().getLoginUser());
//            //设置是否要把开始节点的任务一起完成,默认true
////            startProcessInfo.setCompleteStartTask(false);
//            startProcessInfo.setBusinessId(id);//使用这个id来吧业务跟流程关联起来
//            //设置开始节点的流程变量 ${approveUser}
//            Map<String,Object> variables=new HashMap<String,Object>();
//            variables.put("user","yyc");
//            startProcessInfo.setVariables(variables);
//            //通过进程名称“demo2”来开启流程实例（模拟）
//            processService.startProcessByName("物料采购流程",startProcessInfo);
//        }finally {
//            session.flush();
//            session.close();
//        }
//        return null;
//    }
//
//    //处理"初审"节点发送过来的请求
//    @RequestMapping("/loadBussinessData")
//    @ResponseBody
//    public  Object loadBussinessData(String businessId){
//        Session session=sessionFactory.openSession();
//        try{
//            String hql="from "+Demo.class.getName()+" where id=:bid";
//            Query query=session.createQuery(hql);
//            query.setString("bid",businessId);
//            Demo demo=(Demo)query.uniqueResult();
//            return demo;
//        }finally {
//            session.close();
//        }
//    }
//
//    //完成审批
//    @RequestMapping("/doApprove")
//    @ResponseBody
//    @Transactional
//    public  Object doApprove(String taskId,String opion){
//        Long id=Long.valueOf(taskId);
//        //先让任务处理运行状态
//       taskService.start(id);
//       //再完成任务
//        taskService.complete(id,new TaskOpinion(opion));
//        return null;
//    }
//
//}
