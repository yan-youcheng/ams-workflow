package com.ontoweb.workflow.controller;

import com.ontoweb.workflow.service.UfloProcessService;
import com.ontoweb.workflow.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author 16177
 * @Date 2020/11/12 21:25
 * @Description ProcessController
 */
@RestController
@RequestMapping("/process")
public class ProcessController {

    @Autowired
    private UfloProcessService ufloProcessService;

    /**
     * @param map fileName
     * @return 部署流程
     */
    @PostMapping("/deploy")
    public Result deploy(@RequestBody Map<String, String> map) {
        if (!map.containsKey("fileName"))
            return Result.result400();
        ufloProcessService.deployProcessByFileName(map.get("fileName"));
        return Result.ok();
    }

    /**
     * @param map processId
     * @return 删除流程
     */
    @PostMapping("/deleteProcess")
    public Result deleteProcess(@RequestBody Map<String, Long> map) {
        if (!map.containsKey("processId"))
            return Result.result400();
        ufloProcessService.deleteProcess(map.get("processId"));
        return Result.ok();
    }

    /**
     * @param map processId
     * @return 开启流程
     */
    @PostMapping("/startProcess")
    public Result startProcess(@RequestBody Map<String, Long> map) {
        if (!map.containsKey("processId"))
            return Result.result400();
        ufloProcessService.startProcess(map.get("processId"));
        return Result.ok();
    }

}
