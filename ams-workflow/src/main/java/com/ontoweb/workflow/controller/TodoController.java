package com.ontoweb.workflow.controller;

import com.bstek.uflo.utils.EnvironmentUtils;
import com.ontoweb.workflow.dto.TodoDTO;
import com.ontoweb.workflow.service.TodoService;
import com.ontoweb.workflow.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author 16177
 * @Date 2020/11/13 11:38
 * @Description TodoController
 */
@RestController
@RequestMapping("/todo")
public class TodoController {

    @Autowired
    private TodoService todoService;

    /**
     * @param todoDTO processId, taskName
     * @return 流程节点表单信息
     */
    @PostMapping("/loadFormInfo")
    public Result loadTodo(@RequestBody TodoDTO todoDTO) {
        Map<String, String> result = todoService.loadFormInfo(todoDTO);
        if (result == null)
            return Result.resultFail("实例或任务不存在");
        return Result.ok(result);
    }

    /**
     * @param map taskId,表单数据
     * @return 完成节点任务
     */
    @PostMapping("/completeTask")
    public Result completeTask(@RequestBody Map<String, Object> map) {
        if (!map.containsKey("taskId"))
            return Result.result400();
        todoService.completeTask(map);
        return Result.ok();
    }

    @PostMapping("/test")
    public Result test() {
        return Result.ok(EnvironmentUtils.getEnvironment().getLoginUser());
    }

}
