package com.ontoweb.workflow.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author 16177
 * @Date 2020/11/13 11:41
 * @Description TodoDTO
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TodoDTO {

    @Builder.Default
    private Integer pageIndex = 1;

    @Builder.Default
    private Integer pageSize = 10;

    private Long taskId;

    private String taskName;

    private Long processId;

}
