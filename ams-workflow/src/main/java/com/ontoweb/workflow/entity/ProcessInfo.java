package com.ontoweb.workflow.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @Author 16177
 * @Date 2020/11/13 9:49
 * @Description ProcessInfo
 */
@Data
@Entity
@Table(name = "process_info")
@NoArgsConstructor
@AllArgsConstructor
public class ProcessInfo {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "process_id")
    private long processId;

    @Column(name = "task_name")
    private String taskName;

    @Column(name = "form_designer_id")
    private String formDesignerId;

}
