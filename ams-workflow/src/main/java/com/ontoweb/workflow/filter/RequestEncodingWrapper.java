package com.ontoweb.workflow.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.net.URLDecoder;

public class RequestEncodingWrapper extends HttpServletRequestWrapper {

    public RequestEncodingWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getParameter(String name) {
        String parameter = getRequest().getParameter(name);
        if (parameter == null)
            return null;
        return URLDecoder.decode(parameter);
    }

}
