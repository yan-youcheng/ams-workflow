package com.ontoweb.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ontoweb.workflow.entity.ProcessInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author 16177
 * @Date 2020/11/13 10:38
 * @Description ProcessInfoMapper
 */
@Mapper
public interface ProcessInfoMapper extends BaseMapper<ProcessInfo> {
}
