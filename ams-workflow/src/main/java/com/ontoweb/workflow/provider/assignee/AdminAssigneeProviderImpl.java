package com.ontoweb.workflow.provider.assignee;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.assign.AssigneeProvider;
import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import com.ontoweb.workflow.entity.SysRole;
import com.ontoweb.workflow.service.feign.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @Author ethan yan
 * @Date 2020/11/10
 * @Desc 根据角色为管理员来作为审核人
 */
@Component
@Slf4j
public class AdminAssigneeProviderImpl implements AssigneeProvider {

    //访问用户体系的微服务
    @Autowired
    private UserService userService;

    @Override
    public boolean isTree() {
        return false;
    }

    @Override
    public String getName() {
        return "管理员";
    }

    @Override
    public void queryEntities(PageQuery<Entity> pageQuery, String keyword) {
        /*log.info("getAdminByRole");
        List<SysRole> sysRoleList = userService.queryUserByRoleId("f6817f48af4fb3af11b9e8bf182f618b").getResult(); //查询角色

        int pageIndex = pageIndexgeQuery.getPageIndex();
        int pageSize = pageQuery.getPageSize();

        List<Entity> entities = new ArrayList<>();

        for(int i = (pageIndex - 1) * pageSize; i < pageIndex * pageSize; i++) {
            SysRole sysRole = sysRoleList.get(i);
            Entity entity = new Entity(sysRole.getId(), sysRole.getRoleName());
            entities.add(entity);
        }

        pageQuery.setResult(entities);
        pageQuery.setRecordCount(entities.size());*/

    }

    //根据用户角色ID获取
    @Override
    public Collection<String> getUsers(String entityId, Context context, ProcessInstance processInstance) {
        log.info("getAdmin");

        List<String> users=new ArrayList<String>();
        users.add(entityId);
        return users;
    }

    @Override
    public boolean disable() {
        return false;
    }
}
