package com.ontoweb.workflow.provider.assignee;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.assign.AssigneeProvider;
import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import com.ontoweb.workflow.entity.SysRole;
import com.ontoweb.workflow.service.feign.UserService;
import com.ontoweb.workflow.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @Author ethan yan
 * @Date 2020/11/10
 * @Desc 根据角色为管理员来作为审核人
 */
@Component
@Slf4j
public class RoleAssigneeProvider implements AssigneeProvider {

    @Autowired
    private UserService userService;

    @Override
    public boolean isTree() {
        return false;
    }

    @Override
    public String getName() {
        return "指定角色";
    }

    @Override
    public void queryEntities(PageQuery<Entity> pageQuery, String s) {
        log.info("getAllRoles");
        List<SysRole> allRoles = userService.queryall().getResult(); //通过调用用户体系微服务查询所有角色
        int pageIndex = pageQuery.getPageIndex();
        int pageSize = pageQuery.getPageSize();

        List<Entity> entities = new ArrayList<>(); //返回角色实体列表

        for(int i = (pageIndex - 1); i < pageIndex * pageSize; i++) {
            SysRole sysRole = allRoles.get(i);
            Entity entity = new Entity(sysRole.getId(), sysRole.getRoleName());
            entities.add(entity);
        }

        pageQuery.setRecordCount(allRoles.size());
        pageQuery.setResult(entities);
    }

    @Override
    public Collection<String> getUsers(String roleId, Context context, ProcessInstance processInstance) {
        //根据角色ID获取特定的指定参与者
        log.info(roleId);

        List<String> users = userService.queryUserByRoleId(roleId).getResult();

        return users;
    }

    @Override
    public boolean disable() {
        return false;
    }
}
