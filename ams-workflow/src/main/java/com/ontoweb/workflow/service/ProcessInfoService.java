package com.ontoweb.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ontoweb.workflow.entity.ProcessInfo;

/**
 * @Author 16177
 * @Date 2020/11/13 10:49
 * @Description process_info表操作
 */
public interface ProcessInfoService extends IService<ProcessInfo> {
}
