package com.ontoweb.workflow.service;

import com.bstek.uflo.console.handler.impl.PageData;
import com.ontoweb.workflow.dto.TodoDTO;

import java.util.Map;

/**
 * @Author 16177
 * @Date 2020/11/13 11:44
 * @Description TodoService
 */
public interface TodoService {

    /**
     * @param todoDTO processId,taskName
     * @return 获取流程节点表单数据
     */
    Map<String, String> loadFormInfo(TodoDTO todoDTO);

    /**
     * @param map taskId,表单数据
     */
    void completeTask(Map<String, Object> map);

}
