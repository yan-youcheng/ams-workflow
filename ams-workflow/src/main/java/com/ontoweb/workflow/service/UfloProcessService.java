package com.ontoweb.workflow.service;

/**
 * @Author 16177
 * @Date 2020/11/12 21:27
 * @Description 流程实例控制
 */
public interface UfloProcessService {

    /**
     * 部署流程模板
     * @param fileName 流程模板名称
     */
    void deployProcessByFileName(String fileName);

    /**
     * 产出一个流程实例
     * @param processId 流程实例ID
     */
    void deleteProcess(Long processId);

    /**
     * 开启一个流程
     * @param processId 流程实例ID
     */
    void startProcess(Long processId);

}
