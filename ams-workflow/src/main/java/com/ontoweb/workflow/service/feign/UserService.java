package com.ontoweb.workflow.service.feign;

import com.ontoweb.workflow.entity.SysRole;
import com.ontoweb.workflow.utils.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author ethan yan
 * @Date 2020/11/10
 * @Desc 调用用户体系的微服务
 */
@Service
@FeignClient("USER-SYSTEM")
public interface UserService {
    @RequestMapping(value = "/sys/role/queryall", method = RequestMethod.GET)
    Result<List<SysRole>> queryall();

    @GetMapping("/sys/user/queryUserByRoleId")
    Result<List<String>> queryUserByRoleId(@RequestParam(name = "roleId") String roleId);
}
