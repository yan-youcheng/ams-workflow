package com.ontoweb.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ontoweb.workflow.entity.ProcessInfo;
import com.ontoweb.workflow.mapper.ProcessInfoMapper;
import com.ontoweb.workflow.service.ProcessInfoService;
import org.springframework.stereotype.Service;

/**
 * @Author 16177
 * @Date 2020/11/13 10:53
 * @Description ProcessInfoServiceImpl
 */
@Service
public class ProcessInfoServiceImpl extends ServiceImpl<ProcessInfoMapper, ProcessInfo>
        implements ProcessInfoService {
}
