package com.ontoweb.workflow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bstek.uflo.diagram.TaskInfo;
import com.bstek.uflo.model.task.Task;
import com.bstek.uflo.service.TaskService;
import com.ontoweb.workflow.dto.TodoDTO;
import com.ontoweb.workflow.entity.ProcessInfo;
import com.ontoweb.workflow.service.ProcessInfoService;
import com.ontoweb.workflow.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author 16177
 * @Date 2020/11/13 11:45
 * @Description TodoServiceImpl
 */
@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    private ProcessInfoService processInfoService;

    @Autowired
    private TaskService taskService;

    @Override
    public Map<String, String> loadFormInfo(TodoDTO todoDTO) {
        QueryWrapper<ProcessInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("process_id", todoDTO.getProcessId())
                .eq("task_name", todoDTO.getTaskName());
        ProcessInfo processInfo = processInfoService.getOne(wrapper);
        if (processInfo == null)
            return null;
        Map<String, String> result = new LinkedHashMap<>();
        result.put("formDesignerId", processInfo.getFormDesignerId());
        return result;
    }

    @Override
    public void completeTask(Map<String, Object> map) {
        long taskId = Long.parseLong(String.valueOf(map.get("taskId")));
        map.remove("taskId");
        // 提交表单数据

        if (!map.isEmpty())
            taskService.complete(taskId, map);
        else
            taskService.complete(taskId);
    }
}
