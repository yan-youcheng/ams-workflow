package com.ontoweb.workflow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bstek.uflo.console.provider.ProcessProvider;
import com.bstek.uflo.console.provider.ProcessProviderUtils;
import com.bstek.uflo.model.ProcessDefinition;
import com.bstek.uflo.service.ProcessService;
import com.bstek.uflo.service.StartProcessInfo;
import com.bstek.uflo.utils.EnvironmentUtils;
import com.ontoweb.workflow.entity.ProcessInfo;
import com.ontoweb.workflow.service.ProcessInfoService;
import com.ontoweb.workflow.service.UfloProcessService;
import com.ontoweb.workflow.utils.UUIDUtils;
import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author 16177
 * @Date 2020/11/12 21:27
 * @Description ProcessServiceImpl
 */
@Log4j2
@Service
public class UfloProcessServiceImpl implements UfloProcessService {

    @Autowired
    private ProcessService processService;

    @Autowired
    private ProcessInfoService processInfoService;

    @Override
    public void deployProcessByFileName(String fileName) {
        ProcessProvider processProvider = ProcessProviderUtils.getProcessProvider(fileName);
        InputStream inputStream = processProvider.loadProcess(fileName);
        String content = "";
        try {
            StringBuilder sb = new StringBuilder();
            String line;
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            content = sb.toString();
        } catch (Exception e) {
            log.error("inputStream转String失败");
        }
        ProcessDefinition processDefinition = processService.deployProcess(
                new ByteArrayInputStream(content.getBytes()));

        // 抽取自定义数据
        List<ProcessInfo> processInfos = new ArrayList<>();
        Document document = Jsoup.parse(content);
        Elements elements = document.getElementsByTag("task");
        for (Element element:elements) {
            Elements userDataS = element.getElementsByTag("user-data");
            for (Element userData:userDataS){
                long processId = processDefinition.getId();
                String taskName = element.attr("name");
                String formDesignerId = userData.attr("value");
                processInfos.add(new ProcessInfo(null, processId, taskName, formDesignerId));
            }
        }
        processInfoService.saveBatch(processInfos);
    }

    @Override
    public void deleteProcess(Long processId) {
        processService.deleteProcess(processId);
        QueryWrapper<ProcessInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("process_id", processId);
        processInfoService.remove(wrapper);
    }

    @Override
    public void startProcess(Long processId) {
        String loginUser = EnvironmentUtils.getEnvironment().getLoginUser();
        String businessId = UUIDUtils.generateUUID();
        StartProcessInfo startProcessInfo = new StartProcessInfo(loginUser);
        startProcessInfo.setBusinessId(businessId);
        processService.startProcessById(processId, startProcessInfo);
    }

}
