package com.ontoweb.workflow.utils;

import java.util.UUID;

/**
 * @Author 16177
 * @Date 2020/11/12 22:35
 * @Description UUIDUtils
 */
public class UUIDUtils {

    public static String generateUUID(){
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

}
